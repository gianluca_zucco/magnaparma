#!/usr/bin/env bash

REMOTE_ROOT='kosmosol:/home/magnaparma/production';
PATHS=(app/code/community app/code/local app/design/frontend skin);

for path in ${PATHS[@]};
do
    rsync -rv ${REMOTE_ROOT}/${path}/* ${path}/;
done;
