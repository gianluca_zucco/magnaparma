<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

require_once dirname(dirname(dirname(__FILE__))) . '/abstract.php';

class Kosmosol_Manufacturers_Synchronize extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        $env = Mage::getModel('core/app_emulation')->startEnvironmentEmulation(Mage_Core_Model_App::ADMIN_STORE_ID,
            Mage_Core_Model_App_Area::AREA_ADMIN);

        $values = Mage::helper('kosmosol_manufacturer')->getAttribute()->getSource()->getAllOptions();

        foreach ($values as $data) {
            try {
                $attributeOption = Mage::getModel('kosmosol_manufacturer/attribute_option')->addData($data);
                //Create static block out of category
                $block = Mage::getModel('kosmosol_manufacturer/block')->createIfNotExists($attributeOption);
                $category = Mage::getModel('kosmosol_manufacturer/category')->createIfNotExists($attributeOption,
                    $block);
            } catch (Exception $e) {
                Mage::helper('kosmosol_manufacturer')->log($e->getMessage());
                continue;
            }
        }
        Mage::helper('kosmosol_manufacturer')->log('Synchronization complete.');

        Mage::getModel('core/app_emulation')->stopEnvironmentEmulation($env);
    }

    protected function _applyPhpVariables()
    {
        parent::_applyPhpVariables();
        ini_set('memory_limit', '1G');
    }


}

$class = new Kosmosol_Manufacturers_Synchronize();
$class->run();
