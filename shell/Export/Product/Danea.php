<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

require_once dirname(dirname(dirname(__FILE__))) . '/abstract.php';

class Magnaparma_Export_Product_Danea extends Mage_Shell_Abstract
{
    /**
     * Run script
     *
     */
    public function run()
    {
        $store = Mage::app()->getStore('it');
        $info = Mage::getModel('core/app_emulation')->startEnvironmentEmulation($store->getId());
        $_target = Mage::getConfig()->getVarDir('export') . DS . 'danea_export.csv';
        $_source = Mage::getConfig()->getVarDir('export') . DS . 'import_model.csv';
        $_map = array_flip(array(
            0 => 'sku',
            1 => 'name',
            3 => 'category',
            4 => 'subcategory',
            6 => 'tax_class_id',
            7 => 'price',
            16 => 'manufacturer',
            17 => 'description',
            48 => 'image'
        ));
        $io = new Varien_File_Csv();
        $_data = $io->getData($_source);
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(array('name', 'image', 'description', 'tax_class_id', 'price', 'manufacturer'));

        $collection->walk(function(Mage_Catalog_Model_Product $product) use ($io, $_target, &$_data, $_map, $store)   {

            $mapped = array();
            $taxCalculation = Mage::getModel('tax/calculation');
            foreach ($product->getData() as $k => $v)  {
                if (array_key_exists($k, $_map)) {
                    switch ($k) {
                        case 'manufacturer':
                            $mapped[$_map[$k]] = $product->getAttributeText($k);
                            break;
                        case 'tax_class_id':
                            $request = $taxCalculation->getRateRequest(null, null, null, $store);
                            $mapped[$_map[$k]] = $taxCalculation->getRate($request->setProductClassId($product->getTaxClassId()));
                            break;
                        case 'image':
                            $mapped[$_map[$k]] = !$v ? null : Mage::getBaseUrl('media') . 'catalog/product' . $v;
                            break;
                        default:
                            $mapped[$_map[$k]] = $v;
                        break;
                    }
                }
            }

            $categoryIds = $product->getCategoryIds();

            if ($categoryIds)   {
                $categories = Mage::getResourceModel('catalog/category_collection')
                    ->addAttributeToSelect('name')->addFieldToFilter('entity_id', array('in' => $categoryIds))
                    ->addFieldToFilter('path', array('nlike' => '1/2/103%'));
                $categories->setOrder('level')->getSelect()->limit(2);
                $mapped[$_map['category']] = $categories->getFirstItem()->getName();
                $mapped[$_map['subcategory']] = $categories->getLastItem()->getId() != $categories->getFirstItem()->getId() ? $categories->getLastItem()->getName() : null;
            } else {
                $mapped[$_map['category']] = null;
                $mapped[$_map['subcategory']] = null;
            }

            $line = array_map(function() {
                return null;
            }, reset($_data));
            $_data[] = array_replace($line, $mapped);
        });

        $io->saveData($_target, $_data);
        Mage::getModel('core/app_emulation')->stopEnvironmentEmulation($info);
    }
}
$class = new Magnaparma_Export_Product_Danea();
$class->run();
