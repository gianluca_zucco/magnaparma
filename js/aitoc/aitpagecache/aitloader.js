
/**
 * Product:     Magento Booster 1.4+
 * Package:     Aitoc_Aitpagecache_3.0.2_2.1.16_390964
 * Purchase ID: n/a
 * Generated:   2012-10-09 09:21:39
 * File path:   js/aitoc/aitpagecache/aitloader.js
 * Copyright:   (c) 2012 AITOC, Inc.
 */
var aitpagecache_Loader = Class.create({
    isDataLoaded: false,    
    data: null,
    config : {
        blockClass: 'aitoc-aitpagecache-loadable-block',
        loadingEffectClass: 'aitoc-aitpagecache-loading-effect',
        disabledCacheBlocks: [],
        noCacheFlag: 'noMagentoBoosterCache'
    },
    initialize: function(config) {
        Object.extend(this.config, config);
        document.observe('dom:loaded', this.onDomLoaded.bind(this));    
    },
    onDomLoaded: function() {        
        this.process();
    },
    process: function() {
    
        if (!$$('.' + this.config.blockClass).size()) {
            return;
        }
        
        this.config.disabledCacheBlocks.each(function (blockId) {
            this.addLoadingEffect(this.getPageCacheBlockId(blockId))
        }, this);
            
        new Ajax.Request(this.getNoCacheUrl(this.config.url, this.config.noCacheFlag), {
            method: 'GET',
            onSuccess: function(transport) {
                
                var cntr = new Element('div');
                Element.extend(cntr);                
                cntr.update(transport.responseText);                                            
                
                this.config.disabledCacheBlocks.each(function (blockId) 
                {            
                    var blockArr = $$('#' + this.getPageCacheBlockId(blockId));
                    var cacheBlockId = this.getPageCacheBlockId(blockId);
                    if (blockArr)
                    {    
                        for(i = 0; i < blockArr.size(); i++)
                        {
                            blockArr[i].replace(cntr.down('#' + cacheBlockId, i).innerHTML);
                        }; 
                        this.removeLoadingEffects(cacheBlockId);                                                      
                    }                                                        
                }, this);
            }.bind(this),
            onComplete: function(transport) {
                this.config.disabledCacheBlocks.each(function (blockId) {
                this.removeLoadingEffects(this.getPageCacheBlockId(blockId));
                }, this);                
            }
        });        
    },
    addLoadingEffect: function(id)
    {
        
        var cntrArr = $$('#' + id);    
        
        if (!cntrArr)
        {            
            return;
        }
        cntrArr.each(function(cntr)
        {
            var loadingEffect = new Element('div', {
                'class': this.config.loadingEffectClass,
                'id': this.getLoadingEffectBlockId(id)
            }).setStyle({
                'width': cntr.getWidth() + 'px',
                'height': cntr.getHeight() + 'px',
                'left': this.getLeftPos(cntr) + 'px',
                'top': this.getTopPos(cntr) + 'px'
            });
            $$('body').first().appendChild(loadingEffect);            
        },this); 

    },
    removeLoadingEffects: function(id)
    {
        var loadingEffectBlocks = $$('#' + this.getLoadingEffectBlockId(id));             
        loadingEffectBlocks.each(
        function(block)
        {    
            block.remove();        
        },this);        
    },
    getLoadingEffectBlockId: function(id)
    {        
        return this.config.loadingEffectClass + '-' + id;
    },
    getPageCacheBlockId: function(id)
    {
        return this.config.blockClass + '-' + id;
    },
    getLeftPos: function(element)
    {
        var valueL = 0;
        try
        {
            do
            {
                valueL += element.offsetLeft || 0;
                element = element.offsetParent;
            } while (element);
        }
        catch( ex ) {            
    }
    return valueL;
    },    
    getTopPos: function(element)
    {
        var valueT = 0;
        try
        {
            do
            {
                valueT += element.offsetTop || 0;
                element = element.offsetParent;
            } while (element);
        }
        catch( ex ) {
    }
    return valueT;
    },
    getNoCacheUrl: function(url, flag)
    {
        var sign = '?';
        if ((url.indexOf('?') > -1) && (url.indexOf('&') > -1))
        {
            sign = '&';
        }
        
        return url + sign + flag;
    }
});