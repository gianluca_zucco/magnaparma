// JavaScript Document
    jQuery(document).ready(function($){
  		//you can now use $ as your jQuery object.
  		var body = $( 'body' );
	});

	function captionMoveIn() {
		jQuery('.nivo-caption')
		.fadeIn("fast")
		.animate({left: "0px"},800)
	;};
	
	
	function captionMoveOut() {
		jQuery('.nivo-caption')
		.animate({left: "-500px"},800)
		.fadeOut("fast")
	;};

	
	jQuery(window).load(function() {
		jQuery('#slider').nivoSlider({
	        effect:"sliceUpDownLeft",
	        pauseTime:4000,
		animSpeed:500,
		randomStart: true,
		afterLoad: function (){captionMoveIn();},
	        beforeChange: function (){captionMoveOut();captionMoveIn();}
	      
	    });
	});