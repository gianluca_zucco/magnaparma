document.observe("dom:loaded", function() {

    var getLabelByElementAndContext = function(element) {
        return Element.select(element.up(2), '[for="'+element.id+'"]').shift();
    };

    var resetValidation = function(element) {
        var advice = Validation.getAdvice('validate-electronic-invoice-code', element);
        if (advice) {
            advice.remove();
        }
        Validation.reset(element);
    };

    var requireValidation = function() {
        if ($F(this)) {
            Element.select(document, electronicInvoiceEntries).each(function(element) {
                var label = getLabelByElementAndContext(element);
                if (label) {
                    label.addClassName('required');
                }
                element.addClassName('required-entry');
            });
        } else {
            Element.select(document, electronicInvoiceEntries).each(function(element) {
                var label = getLabelByElementAndContext(element);
                if (label) {
                    label.addClassName('required');
                }
                element.removeClassName('required-entry');
                resetValidation(element);
            })
        }
    };

    Element.select(document, electronicInvoiceTriggers || '').invoke('observe', 'keyup', requireValidation);
    Element.select(document, electronicInvoiceTriggers || '').invoke('observe', 'change', requireValidation);

    Element.select(document, '[id="invoice_type"],[id="billing:invoice_type"]').invoke('observe', 'change', function() {
        try {
            var element = $((/([a-z]{1,}:)/.exec(this.id)||['']).shift()+'invoice_code');
            resetValidation(element);
            Validation.validate(element);
        } catch (e) {
            console.debug(e.message);
        }
    });
});

Validation.addAllThese([
    ['validate-electronic-invoice-code', '', function(v, element) {
        var prefix = (/([a-z]{1,}:)/.exec(element.id)||['']).shift();
        try {
            var validator = Validation.get('validate-electronic-invoice-code-' + $F(prefix+'invoice_type'));
            if (!validator.test(v)) {
                this.error = validator.error;
                return false;
            }
        } catch (e) {
            console.debug(e.message);
        }
        return true;
    }],
    ['validate-electronic-invoice-code-sdi', 'SDI must be 7 alphanumeric characters', function(v) {
        return /^[a-z0-9]{6,7}$|^$/i.test(v);
    }],
    ['validate-electronic-invoice-code-pec', 'PEC must be a a valid email', function(v) {
        return Validation.get('validate-email').test(v);
    }]
]);
