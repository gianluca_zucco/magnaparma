openssl req \
    -x509 \
    -nodes \
    -days 3650 \
    -newkey rsa:2048 \
    -keyout key.key \
    -extensions v3_req \
    -out certificate.crt \
    -config req.conf

openssl dhparam -out dhparams.pem 4096
