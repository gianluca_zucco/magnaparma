#lock '3.6.1'

set :application, 'magnaparma'
set :repo_url, 'git@bitbucket.org:gianluca_zucco/magnaparma.git'
set :keep_releases, 2

set :slack_channel, '#magento_magnaparma'
set :slack_endpoint, 'https://hooks.slack.com'
set :slack_path, '/services/T3KK3DPEV/B6JTREW13/dtOzkAg8uwvY0Snrl5LuFvGM'
after 'deploy:started', 'slack:deploy:start'
after 'deploy:finishing', 'slack:deploy:finish'
after 'deploy:finishing_rollback', 'slack:deploy:rollback'
