// Generated by CoffeeScript 1.4.0

jQuery('document').ready(function(){




jQuery('button.confirm').click(function() {

	jQuery('input#card_number').keydown();
    jQuery('input#card_number').keypress();
    jQuery('input#card_number').keyup();
    jQuery('input#card_number').blur();
    if(jQuery('input#expiry_date').hasClass('valid') && jQuery('input#name_on_card').hasClass('valid')  && jQuery('input#cvv').hasClass('valid') && jQuery('input#card_number').hasClass('valid')  ){
    	document.forms["ccform"].submit();
    	return false;
    }
    jQuery('b.required').show();

	 return false;
});

jQuery('input#expiry_date').bind('input change', function() {
	
     jQuery(this).unbind('keyup');
     var pattern= new RegExp(/(([0]*[1-9]{1})|([1]{1}[0-2]{1}))(\/{1})((19[0-9]{2})|([2-9]{1}[0-9]{3}))$/);
     if(pattern.test(jQuery(this).val())) {
     	jQuery(this).addClass('valid');
     }
     else {
     	jQuery(this).removeClass('valid');
     }
   
  });
jQuery('input#name_on_card').bind('input change', function() {
	
     jQuery(this).unbind('keyup');
     var pattern= new RegExp(/^((\b[a-zA-Z]{2,40}\b)\s*){2,}$/);
     if(pattern.test(jQuery(this).val())) {
     	jQuery(this).addClass('valid');
     }
     else {
     	jQuery(this).removeClass('valid');
     }
  });
jQuery('input#cvv').bind('input change', function() {
	
     jQuery(this).unbind('keyup');
     var pattern= new RegExp(/[0-9]{3,4}/);
     if(pattern.test(jQuery(this).val())) {
     	jQuery(this).addClass('valid');
     }
     else {
     	jQuery(this).removeClass('valid');
     }
  });

function isValid(){

}

});