namespace :docker do
    desc 'Restart FPM'
    task :fpm_restart do
        on roles(:web) do
            within deploy_to do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{deploy_to}/docker-compose-#{fetch(:stage)}.yml restart php";
            end
        end
    end

    desc 'Restart application'
    task :restart do
        on roles(:web) do
            within deploy_to do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{deploy_to}/docker-compose-#{fetch(:stage)}.yml down -v";
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{deploy_to}/docker-compose-#{fetch(:stage)}.yml up -d";
            end
        end
    end

    desc 'Up application'
    task :up do
        on roles(:web) do
            within deploy_to do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{deploy_to}/docker-compose-#{fetch(:stage)}.yml up -d";
            end
        end
    end

    desc 'Down application'
    task :down do
        on roles(:web) do
            within deploy_to do
                execute "docker-compose -p #{fetch(:application)}#{fetch(:stage)} -f #{deploy_to}/docker-compose-#{fetch(:stage)}.yml down -v";
            end
        end
    end
end
