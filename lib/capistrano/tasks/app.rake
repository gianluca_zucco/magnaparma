namespace :app do
    namespace :cache do
        desc 'Flush caches'
        task :flush do
            on roles(:web) do
                execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun cache:flush"
            end
        end
        namespace :clean do
            desc 'Clean configuration cache'
            task :config do
                on roles(:web) do
                    execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun cache:clean config"
                end
            end
            task :all do
                on roles(:web) do
                    execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun cache:clean"
                end
            end
        end
        task :enable do
            on roles(:web) do
                execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun cache:enable"
            end
        end
        task :disable do
            on roles(:web) do
                execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun cache:disable"
            end
        end
    end
    namespace :reindex do
        desc 'Reindex'
        task :all do
            on roles(:web) do
                execute "docker exec -u www-data #{fetch(:application)}#{fetch(:stage)}_php_1 magerun index:reindex:all"
            end
        end
    end
    namespace :cron do
        desc 'Deploy crontab'
        task :deploy do
            on roles(:web) do
                within deploy_to do
                    upload! "config/cron", "#{deploy_to}/config/cron"
                    execute "crontab -u magnaparma #{deploy_to}/config/cron"
                end
            end
        end
    end
end
