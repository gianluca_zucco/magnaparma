Rake::Task["deploy"].clear_actions
desc 'Deploy'
task :deploy do
    on roles(:web) do
        within deploy_to do
            invoke "slack:deploy:start"
            execute("cd #{deploy_to} && git pull")
            invoke "docker:fpm_restart"
            invoke "slack:deploy:finish"
        end
    end
end
