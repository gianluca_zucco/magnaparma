namespace :nginx do
    desc 'Reload server\'s NGINX with new configuration'
    task :reload do
        on roles(:web) do
            within deploy_to do
                execute "cat #{deploy_to}/config/frontend/#{fetch(:stage)}.conf | sed s/{backend}/$(docker inspect --format={{.NetworkSettings.Networks.#{fetch(:application)}#{fetch(:stage)}_default.IPAddress}} #{fetch(:application)}#{fetch(:stage)}_backend_1)/g >> #{deploy_to}/#{fetch(:stage)}.#{fetch(:application)}.com.conf"
                execute "sudo mv #{deploy_to}/#{fetch(:stage)}.#{fetch(:application)}.com.conf /etc/nginx/conf.d/"
                execute "sudo service nginx reload"
            end
        end
    end
end
