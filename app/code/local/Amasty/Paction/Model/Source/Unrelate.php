<?php
 /**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Paction
 */

class Amasty_Paction_Model_Source_Unrelate
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = array(
            array('value'=> 0, 'label' => Mage::helper('ampaction')->__('Remove relations between selected products only')),
            array('value'=> 1, 'label' => Mage::helper('ampaction')->__('Remove selected products from ALL relations in the catalog')),
            array('value'=> 2, 'label' => Mage::helper('ampaction')->__('Remove all relations from selected products')),
        );

        return $options;
    }
}