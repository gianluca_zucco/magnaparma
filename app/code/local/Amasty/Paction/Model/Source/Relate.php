<?php
 /**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Paction
 */

class Amasty_Paction_Model_Source_Relate
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = array(
            array('value'=> 0, 'label' => Mage::helper('ampaction')->__('Default')),
            array('value'=> 1, 'label' => Mage::helper('ampaction')->__('2 Way')),
            array('value'=> 2, 'label' => Mage::helper('ampaction')->__('Multi Way')),
        );

        return $options;
    }
}