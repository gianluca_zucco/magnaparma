<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2008-2012 Amasty (http://www.amasty.com)
* @package Amasty_Feed
*/

$this->startSetup();

$this->run("
    alter table `{$this->getTable('amacart/canceled')}`
    change column reason `reason` ENUM('elapsed','bought','link','blacklist','admin','updated');
");

$this->endSetup(); 