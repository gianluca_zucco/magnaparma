<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Acart
*/
class Amasty_Acart_Adminhtml_HistoryController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout(); 

        $this->_setActiveMenu('promo/amacart/history');
            
        $this->_addContent($this->getLayout()->createBlock('amacart/adminhtml_history')); 
            $this->renderLayout();

            }
        
    }
?>