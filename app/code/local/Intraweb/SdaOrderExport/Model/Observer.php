<?php
/**
 * @package    Intraweb_SdaOrderExport
 * @copyright  Copyright (c) 2012 Riccardo Roscilli
 */

/**
 * Observer per inserire l'opzione di esportazione ldv nel csv alle Azioni nel select box della griglia Vendite/Ordini.
 */
class Intraweb_SdaOrderExport_Model_Observer
{
    /**
     * Estende il select box delle Azioni per l'export della LDV.
     * Event: core_block_abstract_prepare_layout_before
     */
    public function addMassaction(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();

        if ($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction && $block->getRequest()->getControllerName() == 'sales_order') {
            $block->addItem('simpleorderexport', array(
                'label' => 'Esporta SDA',
                'url' => Mage::app()->getStore()->getUrl('simpleorderexport/export_order/csvexport'),
            ));
        }
    }
}
