<?php
/**
 * @package    Intraweb_SdaOrderExport
 * @copyright  Copyright (c) 2012 Riccardo Roscilli
 */

/**
 * Esporta gli ordini nel file csv.
 */
class Intraweb_SdaOrderExport_Model_Export_Csv extends Intraweb_SdaOrderExport_Model_Export_Abstract
{
    const ENCLOSURE = '"';
    const DELIMITER = ';';

    /**
     * Implementazione del metodo astratto per esportare gli ordini selezionati in file csv dentro var/export.
     *
     * @param $orders Lista ordini del tipo Mage_Sales_Model_Order oppure order ids da esportare.
     * @return String Il nome del file csv scritto in var/export
     */
    public function exportOrders($orders) 
    {
        $fileName = 'order_export_'.date("Ymd_His").'.csv';
        $fp = fopen(Mage::getBaseDir('export').'/'.$fileName, 'w');
		
       // $this->writeHeadRow($fp);
        foreach ($orders as $order) {
        	
            $order = Mage::getModel('sales/order')->load($order);
            $this->writeOrder($order, $fp);
           
        }
        fclose($fp);
        return $fileName;
    }

    /**
	 * Scrive le intestazioni delle colonne come prima riga del csv.
	 * 
	 * @param $fp File handle del csv
	 */
    protected function writeHeadRow($fp) 
    {
        fputcsv($fp, $this->getHeadRowValues(), self::DELIMITER, self::ENCLOSURE);
    }

    /**
	 * Scrive la riga per l'ordine nel csv.
	 * Aggiunge una riga per ogni ordine
	 * 
	 * @param Mage_Sales_Model_Order $order Ordine da scrivere nel csv
	 * @param $fp File handle del csv
	 */
    protected function writeOrder($order, $fp) 
    {
        $common = $this->getCommonOrderValues($order);
 		fputcsv($fp, $common, self::DELIMITER, self::ENCLOSURE);

 		// commentata la sezione relativa agli articoli
 		// con questo loop scrive un articolo per ogni riga, simile al file di fatturazione...
 		
  //      $orderItems = $order->getItemsCollection();
  //      $itemInc = 0;
 /*       foreach ($orderItems as $item)
        {
            if (!$item->isDummy()) {
                $record = array_merge($common, $this->getOrderItemValues($item, $order, ++$itemInc));
                fputcsv($fp, $record, self::DELIMITER, self::ENCLOSURE);
            }
        }
   */
    }

    /**
	 * Restituisce l'intestazione delle colonne.
	 * 
	 * @return Array Array contentente i nomi delle colonne
	 */
    protected function getHeadRowValues() 
    {
	// non implementato in questa versione    	
  	}

    /**
	 * Restituisce i valori che sono comuni per ogni ordine. Questi sono 
	 * tutti i valori non specifici agli articoli ordinati: data ordine, shipping address, billing
	 * address, metodo di pagamento e i totali ordine.
	 * In questa funziona viene anche inserito il grosso del lavoro per la selezione dei servizi, contrassegno etc. specifici di SDA
	 * 
	 * @param Mage_Sales_Model_Order $order Ordine di cui si vogliono le info
	 * @return Array Array contenente tutti i valori comuni
	 */
    protected function getCommonOrderValues($order) 
    {
        $shippingAddress = !$order->getIsVirtual() ? $order->getShippingAddress() : null;
        $billingAddress = $order->getBillingAddress();
        $payment = $order->getPayment();

        // selezione del metodo di pagamento: contrassegno / altri metodi
        if(strstr($payment->getData('method'), 'cash')) // contrassegno
        	$importo_ca = $payment->getData('amount_ordered');

        // iso country code a 3 caratteri, es. ITA
        $iso3 = Mage::getModel('directory/country')->load($shippingAddress->getCountry())->getIso3Code();             	
		
        // se il campo company è vuoto uso il Cognome Nome del cliente come destinatario
        if ($shippingAddress->getData("company") == '')
        	$destinatario =  $shippingAddress->getName();
        else 
       		$destinatario = $shippingAddress->getData("company");
       		
       	// se il peso supera 200 allora è espresso in grammi
       	if ($order->getData("weight") >= '200')	
       		$weight = $order->getData("weight") / 1000;
       	else 
       		$weight = $order->getData("weight");
       		
       	// scelta tipo di servizio 
       	/*
       	 tipi servizio 
			S12	ZERO TRE CA
			S01	ZERO TRE
			S13	ZERO QUINDICI CA
			S02	ZERO QUINDICI
			S14	ZERO TRENTA CA
			S03	ZERO TRENTA
			S08	INTERNAZIONALE
			
			P01      ZERO TRE
			P02      ZERO QUINDICI
			P03      ZERO TRENTA
			P08      INTERNAZIONALE
			P09      EXTRA LARGE
			P12      ZERO TRE CA
			P13      ZERO QUINDICI CA
			P14      ZERO TRENTA CA
			P15      EXTRA LARGE CA
 
 
			 Servizi SDA
			Codice;Descrizione
			S12;ZERO TRE CA
			S01;ZEROTRE
			S13;ZERO QUINDICI CA
			S02;ZEROQUINDICI
			S14;ZERO TRENTA CA
			S03;ZEROTRENTA
			S04;REGIONALE
			S27;P/A ECONOMY
			S21;PORTO ASSEGNATO
			S05;GOLDEN
			S06;ANDATA&RITORNO
			S08;INTERNAZIONALE
			S15;EXTRA LARGE CA
			S09;EXTRALARGE
			S16;CAPI APPESI SMALL CA
			S10;CAPI APPESI SMALL
			S17;CAPI APPESI LARGE CA
			S11;CAPI APPESI LARGE
			S25;Economy Ca
			S24;ECONOMY
			S30;RACCOMANDATA CA
			S28;RACCOMANDATA
			S31;RACCOMANDATA UNO CA
			S29;RACCOMANDATA UNO
			S35;ROAD EUROPE CA
			S34;ROAD EUROPE
			S38;HOME BOX EXPRESS CA
			S37;EXPRESS BOX
			S36;EXPORT BOX
       	*/
       		
       		
       	if ($iso3 <> 'ITA')  // se è spedizione internazionale
       		$tipo_servizio = 'S08';
		else  // KOSMOSOL è sempre spedizione extralarge
			{
			if ($importo_ca >= '0.1')
				$tipo_servizio = 'S15';
			else 
				$tipo_servizio = 'S09';   		
       		}   
		/*
       	elseif($weight <= '3') // se il peso è inferiore ai 3kg ZERO TRE
       		{
       		if ($importo_ca >= '0.1')
       			$tipo_servizio = 'S12';
       		else 
       			$tipo_servizio = 'S01';
       		}
       	elseif($weight >= '3.01' && $weight <= '15') // se il peso è compreso tra 3 e 15kg ZERO QUINDICI
       		{
			if ($importo_ca >= '0.1')
				$tipo_servizio = 'S13';
			else 
				$tipo_servizio = 'S02';   		
       		}
         elseif($weight >= '15.01' && $weight <= '30') // se il peso è compreso tra 15 e 30 kg  ZERO TRENTA
       		{
			if ($importo_ca >= '0.1')
				$tipo_servizio = 'S14';
			else 
				$tipo_servizio = 'S03';   		
       		}   
		elseif( $weight >= '30.01')  // spedizione extralarge
			{
			if ($importo_ca >= '0.1')
				$tipo_servizio = 'S15';
			else 
				$tipo_servizio = 'S09';   		
       		}   
		*/
       		
      if ($importo_ca >= '0.1')  $pagamento_contrassegno = 'VAR'; // per il momento inseriamo tutti i metodi din incassso: contanti, ass. bancario ...
       		
	 	//	print_r($order->getData());
	 	//	exit;
        // array con i campi del csv
      // nella versione sda il separatore dei decimali è la ,  
      $weight = str_replace(".", ",",$weight);
      $importo_ca = str_replace(".", ",", $importo_ca);
     	
         return array(
          //  $order->getRealOrderId(),
       		$destinatario, 
         	'', // referente, opzionale 
            $shippingAddress ? $shippingAddress->getData("street").', '. $shippingAddress->getData("civico") : '',
            $shippingAddress ? $shippingAddress->getData("postcode") : '',
            $shippingAddress ? $shippingAddress->getData("city") : '',
            $shippingAddress ? $shippingAddress->getRegionCode() : '',
            $iso3,
            $shippingAddress ? $shippingAddress->getData("telephone") : '',
            '',// fax
            '', //cell
            $order->getData("customer_email"), 
            '', // ID fiscale, obbligatorio per internazionali
            $tipo_servizio, //codice servizio obbligatorio
            '', // Codice Accessorio Base non obbligatorio
            '', // Codice Sotto Accessorio non obb.
            $order->getRealOrderId(),  // Numero Riferimento Interno, increment id dell'ordine
	   	    '1', // numero colli, per adesso valori fisso a 1
            $weight, //Peso in kg
            $importo_ca, // importo da incassare in C/A
            $pagamento_contrassegno, // metodo pagamento contrassegno al momento fisso in VAR
            'M', // Tipo Contenuto obb. solo int. M=merci, D=documenti
            '', // Descrizione contenuto obb. INT.
            '', // Tipo Imballo obb Int
            '', // altezza
            '', // larghezza
            '', // profondità
            '', // Pagamento Oneri solo INT
            '', // Valore Dichiarato solo INT
            '', // Assicurazione
            '' //Note
            );              
    }

    /**
	 * Restituisce il valore degli articoli dell'ordine .
	 * 
	 * @param Mage_Sales_Model_Order_Item $item Articolo
	 * @param Mage_Sales_Model_Order $order Ordine a cui appartiene l'articoo
	 * @return Array Array con i valori dell'articolo 
	 */
    protected function getOrderItemValues($item, $order, $itemInc=1) 
    {
        return array(
       		// non implementata 
        );
   
    	//return;
    }
}
?>
