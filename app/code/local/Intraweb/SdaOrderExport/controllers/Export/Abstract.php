<?php
/**
 * @package    Intraweb_SdaOrderExport
 * @copyright  Copyright (c) 2012 Riccardo Roscilli
 */

/**
 * Abstract class per definire l'interfaccia di esportazione e i medoti helpers per 
 * recuperare i dati dell'ordine ed i prodotti.
 */
abstract class Intraweb_SdaOrderExport_Model_Export_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Definizione del metodo astratto per esportare gli ordini in formato specifico in var/export.
     *
     * @param $orders Lista ordini del tipo Mage_Sales_Model_Order oppure order ids da esportare.
     * @return Nome del file scritto in var/export
     */
    abstract public function exportOrders($orders);

    /**
     * Restituisce il nome del website, store e store view in cui è stato fatto l'ordine.
     *
     * @param Mage_Sales_Model_Order $order L'ordine di cui si vuole il dettaglio
     * @return String Nome del website, store e store view in cui è stato fatto l'ordine
     */
    protected function getStoreName($order) 
    {
        $storeId = $order->getStoreId();
        if (is_null($storeId)) {
            return $this->getOrder()->getStoreName();
        }
        $store = Mage::app()->getStore($storeId);
        $name = array(
        $store->getWebsite()->getName(),
        $store->getGroup()->getName(),
        $store->getName()
        );
        return implode(', ', $name);
    }

    /**
     * Restituisce il metodo di pagamento.
     *
     * @param Mage_Sales_Model_Order $order L'ordine di cui si vuole l'info
     * @return String Nome del metodo di pagamento
     */
    protected function getPaymentMethod($order)
    {
        return $order->getPayment()->getMethod();
    }
    
    /**
     * Restituisce il metodo di spedizione per l'ordine.
     *
     * @param Mage_Sales_Model_Order $order L'ordine di cui si vuole l'info
     * @return String Nome del metodo di spedizione
     */
    protected function getShippingMethod($order)
    {
        if (!$order->getIsVirtual() && $order->getShippingMethod()) {
            return $order->getShippingMethod();
        }
        return '';
    }
    
    /**
     * Restituisce la quantità totale di articoli acquistati.
     *
     * @param Mage_Sales_Model_Order $order L'ordine di cui si vuole l'info
     * @return int Numero totale di articoli acquistati
     */
    protected function getTotalQtyItemsOrdered($order) {
        $qty = 0;
        $orderedItems = $order->getItemsCollection();
        foreach ($orderedItems as $item)
        {
            if (!$item->isDummy()) {
                $qty += (int)$item->getQtyOrdered();
            }
        }
        return $qty;
    }

    /**
     * Restituisce lo SKU dell'articolo, in funzione del tipo prodotto.
     * 
     * @param Mage_Sales_Model_Order_Item $item L'articolo di cui si vuole l'info
     * @return String SKU prodotto
     */
    protected function getItemSku($item)
    {
        if ($item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            return $item->getProductOptionByCode('simple_sku');
        }
        return $item->getSku();
    }

    /**
     * Restituisce le opzioni del prodotto separate da virgola es:
     * option1: value1, option2: value2
     *
     * @param Mage_Sales_Model_Order_Item $item L'articolo di cui si vuole l'info
     * @return String Le opzioni
     */
    protected function getItemOptions($item)
    {
        $options = '';
        if ($orderOptions = $this->getItemOrderOptions($item)) {
            foreach ($orderOptions as $_option) {
                if (strlen($options) > 0) {
                    $options .= ', ';
                }
                $options .= $_option['label'].': '.$_option['value'];
            }
        }
        return $options;
    }

    /**
     * Restituisce le opzioni del prodotto inclusi eventuali attributi aggiuntivi
     * @param Mage_Sales_Model_Order_Item $item L'articolo di cui si vuole l'info
     * @return Array Le opzioni e attributi
     */
    protected function getItemOrderOptions($item)
    {
        $result = array();
        if ($options = $item->getProductOptions()) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (!empty($options['attributes_info'])) {
                $result = array_merge($options['attributes_info'], $result);
            }
        }
        return $result;
    }

    /**
     * Calcula il grand total di un articolo incluse tasse ed esclusi gli sconti.
     *
     * @param Mage_Sales_Model_Order_Item $item L'articolo di cui si vuole l'info
     * @return Float Il grand total
     */
    protected function getItemTotal($item) 
    {
        return $item->getRowTotal() - $item->getDiscountAmount() + $item->getTaxAmount() + $item->getWeeeTaxAppliedRowAmount();
    }

    /**
     * Formatta un prezzo aggiungendo il simbolo della valuta  in accordo con il locale corrente, da usare solo in casi particolari.
     *
     * @param Float $price Formato prezzo
     * @param Mage_Sales_Model_Order $formatter Il formato prezzo dell'ordine implementando il metodo formatPriceTxt($price)
     * @return String Prezzo formattato
     */
    protected function formatPrice($price, $formatter) 
    {
        return $formatter->formatPriceTxt($price);
    }
}
?>
