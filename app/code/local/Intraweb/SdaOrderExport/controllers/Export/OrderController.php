<?php
/**
 * @package    Intraweb_SdaOrderExport
 * @copyright  Copyright (c) 2012 Riccardo Roscilli
 */

/**
 * Controller che gestisce le richieste di export.
 */
class Intraweb_SdaOrderExport_Export_OrderController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Esporta gli ordini definiti nel Post "order_ids" in formato csv e lo invia direttamente al browser per il
     * download
     *
     */
    public function csvExportAction()
    {
        $orders = $this->getRequest()->getPost('order_ids', array());
        $file = Mage::getModel('Intraweb_SdaOrderExport/export_csv')->exportOrders($orders);
        $this->_prepareDownloadResponse($file, file_get_contents(Mage::getBaseDir('export') . '/' . $file));
    }

    protected function _isAllowed()
    {
        return true;
    }

}

?>
