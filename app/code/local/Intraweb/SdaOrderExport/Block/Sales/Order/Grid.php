<?php
/**
 * @package    Intraweb_SdaOrderExport
 * @copyright  Copyright (c) 2012 Riccardo Roscilli
 */

/**
 * Override Mage_Adminhtml_Block_Sales_Order_Grid per inserire l'opzione Esporta LDV
 * nel menu mass action.
 */
class Intraweb_SdaOrderExport_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    /**
     * Aggiunge l'opzione Esporta LDV.
     */
    protected function _prepareMassaction()
    {
        // prepara il menu a tendina
        parent::_prepareMassaction();

        // Aggiunge l'opzione di export nel menu
        $this->getMassactionBlock()->addItem(
            'simpleorderexport',
            array(
                'label' => $this->__('Esporta SDA'),
                'url' => $this->getUrl('simpleorderexport/export_order/csvexport')
            )
        );
    }
}

?>
