<?php

class Kosmosol_Alternate_Model_Observer
{

    public function alternateLinks()
    {
        $headBlock = Mage::app()->getLayout()->getBlock('head');

        $stores = Mage::app()->getStores();
        $product = Mage::registry('current_product');
        $category = Mage::registry('current_category');
				$url = '';

        if ($headBlock) {
            foreach ($stores as $store) {
                if ($product) {
                    $category ? $categoryId = $category->getId() : $categoryId = null;
                    $url = $store->getBaseUrl() . Mage::helper('kosmosol_alternate')->rewrittenProductUrl($product->getId(), $categoryId, $store->getId());
                } else {
                    if ($category != null) {
											//$base_url = Mage::app()->getStore($store->getId())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK)
											$url = $store->getBaseUrl() . Mage::getModel('catalog/category')->setStoreId($store->getId())->load($category->getId())->getUrlPath();
											//Mage::getModel('catalog/category')->setStoreId($store->getId())->load($category->getId())->getUrl();
										}	elseif(Mage::getSingleton('cms/page')->getIdentifier() == 'home') {
											// get the page id
											//$page_id = Mage::getBlockSingleton('cms/page')->setStoreId($storeId)->getPage()->getId();
											//$url = Mage::getBlockSingleton('cms/page')->setStoreId($storeId)->getPageUrl($page_id);
											// TODO: fix output for cms pages
											$url = $store->getBaseUrl();
										}
                } 
                $storeCode = substr(Mage::getStoreConfig('general/locale/code', $store->getId()), 0, 2);
                $headBlock->addLinkRel('alternate"' . ' hreflang="' . $storeCode, $url);
            }
        }
        return $this;
    }

}
