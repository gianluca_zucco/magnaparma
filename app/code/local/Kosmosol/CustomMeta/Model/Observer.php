<?php

class Kosmosol_CustomMeta_Model_Observer
{
    /**
     * Change product meta title on product view
     *
     * @pram Varien_Event_Observer $observer
     * @return KS_CustomMeta_Model_Observer
     */
    public function catalog_controller_product_view(Varien_Event_Observer $observer)
    {
        if ($_product = $observer->getEvent()->getProduct()) {
					  //ITALIANO
						if ($observer->getEvent()->getProduct()->getStoreId() == '1') {
							$title = $_product->getData('name') . ' - ' . $_product->getAttributeText('manufacturer');
							$description = 'Cerchi ' . $_product->getData('name') . ' da acquistare? Su MagnaParma le spedizioni sono gratis. Scopri prezzi e offerte online!';
            	$_product->setMetaTitle(str_replace('"','',$title));
							$_product->setMetaDescription(str_replace('"','',$description));
						}
						//INGLESE
						elseif ($observer->getEvent()->getProduct()->getStoreId() == '2') {
							$title = 'Buy Online ' . $_product->getData('name') . ' - MagnaParma';
							$description = 'Buy authentic ' . $_product->getData('name') . ' at the best prices. MagnaParma sells high quality Italian Food.';
            	$_product->setMetaTitle(str_replace('"','',$title));
							$_product->setMetaDescription(str_replace('"','',$description));
						}
						//FRANCESE
						elseif ($observer->getEvent()->getProduct()->getStoreId() == '3') {
							$title = $_product->getData('name') . ' - ' . $_product->getAttributeText('manufacturer');
							$description = 'Vous recherchez ' . $_product->getData('name') . '? MagnaParma livre dans le monde entier. Decouvrez offres et prix en ligne!';
            	$_product->setMetaTitle(str_replace('"','',$title));
							$_product->setMetaDescription(str_replace('"','',$description));
						}
        }
        return $this;
    }
}
