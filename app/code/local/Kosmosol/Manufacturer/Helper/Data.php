<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Kosmosol_Manufacturer_Helper_Data extends Mage_Core_Helper_Abstract
{
    const MANUFACTURERS_ROOT_CATEGORY_ID = 'kosmosol/manufacturers/root';
    const MANUFACTURER_ATTRIBUTE_CODE = 'kosmosol/manufacturers/attribute_code';
    protected $_manufacturerUrls = array();

    /**
     * @return int
     */
    public function getRootCategoryId()
    {
        return Mage::getStoreConfig(self::MANUFACTURERS_ROOT_CATEGORY_ID);
    }

    /**
     * @return Mage_Catalog_Model_Category
     */
    public function getRootCategory()
    {
        return Mage::getModel('catalog/category')->load($this->getRootCategoryId());
    }

    /**
     * @return string
     */
    public function getAttributeCode()
    {
        return Mage::getStoreConfig(self::MANUFACTURER_ATTRIBUTE_CODE);
    }

    /**
     * @return Mage_Eav_Model_Entity_Attribute_Abstract
     */
    public function getAttribute()
    {
        return Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, $this->getAttributeCode());
    }

    public function log($data)
    {
        if (php_sapi_name() == 'cli') {
            fwrite(STDOUT, $data . PHP_EOL);
        }
        Mage::log($data, Zend_Log::DEBUG, sprintf('%s.log', strtolower($this->_getModuleName())), true);
    }

    public function getManufacturerUrlFromProduct(Mage_Catalog_Model_Product $product)
    {
        $manufacturerId = $product->getManufacturer();
        if (!$manufacturerId) return false;
        if (!array_key_exists($manufacturerId, $this->_manufacturerUrls)) {

            /** @var Mage_Catalog_Model_Category $category */
            $category = Mage::getModel('catalog/category')
                ->loadByAttribute('manufacturer', $manufacturerId, array('name', 'url_key', 'url_path', 'is_active'));
            if ($category === false)    {
                return false;
            }
            if (!$category->getIsActive())  {
                return false;
            }
            $this->_manufacturerUrls[$manufacturerId] = $category->getUrl();
        }
        return $this->_manufacturerUrls[$manufacturerId];
    }
}
