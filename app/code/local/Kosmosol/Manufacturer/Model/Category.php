<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Kosmosol_Manufacturer_Model_Category extends Mage_Core_Model_Abstract
{

    /**
     * @param Kosmosol_Manufacturer_Model_Attribute_Option $attributeOption
     * @param Mage_Cms_Model_Block $block
     * @return Mage_Catalog_Model_Category
     */
    public function createIfNotExists(Kosmosol_Manufacturer_Model_Attribute_Option $attributeOption, Mage_Cms_Model_Block $block)
    {
        $category = Mage::getModel('catalog/category')->loadByAttribute('manufacturer', $attributeOption->getValue());

        if ($category !== false) {
            Mage::helper('kosmosol_manufacturer')->log(sprintf('Category for %s exists.', $attributeOption->getLabel()));
            return $category;
        }

        return $this->_create($attributeOption, $block);
    }

    /**
     * @param Kosmosol_Manufacturer_Model_Attribute_Option $attributeOption
     * @param Mage_Cms_Model_Block $block
     * @return Mage_Catalog_Model_Category
     */
    protected function _create(Kosmosol_Manufacturer_Model_Attribute_Option $attributeOption, Mage_Cms_Model_Block $block)
    {
        if (!$attributeOption->getLabel()) {
            Mage::throwException('Attribute label is empty, skipped.');
        }
        $productIds = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('manufacturer', $attributeOption->getValue())
            ->getAllIds();

        if (count($productIds) == 0)    {
            Mage::throwException(sprintf('Found no product for %s, skipped.', $attributeOption->getLabel()));
        }

        $params = array(
            'is_active' => false,
            'include_in_menu' => false,
            'name' => $attributeOption->getLabel(),
            'display_mode' => Mage_Catalog_Model_Category::DM_MIXED,
            'is_anchor' => false,
            'url_key' => Mage::getModel('catalog/category')->formatUrlKey($attributeOption->getLabel()),
            'manufacturer' => $attributeOption->getValue(),
            'landing_page' => $block->getId()
        );
        /** @var Mage_Catalog_Model_Category $category */
        $category = Mage::getModel('catalog/category')->addData($params);
        $category->save();
        Mage::helper('kosmosol_manufacturer')->log(sprintf('Category for %s has been created.', $attributeOption->getLabel()));
        $category->setData('path', sprintf('1/2/%s/%s', Mage::helper('kosmosol_manufacturer')->getRootCategoryId(), $category->getId()));
        $category->setData('level', 3);
        $category->setData('parent_id', Mage::helper('kosmosol_manufacturer')->getRootCategoryId());
        $category->save();

        foreach ($productIds as $productId)
        {
            Mage::getModel('catalog/category_api')->assignProduct($category->getId(), $productId);
        }
        Mage::helper('kosmosol_manufacturer')->log(sprintf('%s products have been added.', count($productIds)));
        return $category;
    }
}
