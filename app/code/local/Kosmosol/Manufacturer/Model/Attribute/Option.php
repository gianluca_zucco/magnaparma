<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 *
 * @method string getLabel()
 * @method int getValue()
 */

class Kosmosol_Manufacturer_Model_Attribute_Option extends Varien_Object
{
    public function prepareIdentifier()
    {
        $name = Mage::getModel('catalog/category')->formatUrlKey($this->getLabel());
        return sprintf('manufacturer_block_%s_%s', $name, $this->getValue());
    }
}
