<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Kosmosol_Manufacturer_Model_Block extends Mage_Core_Model_Abstract
{

    /**
     * @param Kosmosol_Manufacturer_Model_Attribute_Option $attributeOption
     * @return Mage_Cms_Model_Block
     */
    public function createIfNotExists(Kosmosol_Manufacturer_Model_Attribute_Option $attributeOption)
    {
        $block = Mage::getModel('cms/block')->load($attributeOption->prepareIdentifier(), 'identifier');

        if ($block->getId())    {
            Mage::helper('kosmosol_manufacturer')->log(sprintf('Block for %s exists.', $attributeOption->getLabel()));
            return $block;
        }

        return $this->_create($attributeOption);
    }

    /**
     * @param Kosmosol_Manufacturer_Model_Attribute_Option $attributeOption
     * @return Mage_Cms_Model_Block
     */
    protected function _create(Kosmosol_Manufacturer_Model_Attribute_Option $attributeOption)
    {
        if (!$attributeOption->getLabel()) {
            Mage::throwException('Attribute label is empty. Skipped.');
        }

        $params = array(
            'title' => $attributeOption->getLabel(),
            'identifier' => $attributeOption->prepareIdentifier(),
            'stores' => array(Mage::app()->getDefaultStoreView()->getId()),
            'content' => "",
            'is_active' => false
        );
        Mage::helper('kosmosol_manufacturer')->log(sprintf('Block for %s has been created.', $attributeOption->getLabel()));
        return Mage::getModel('cms/block')->addData($params)->save();
    }
}
