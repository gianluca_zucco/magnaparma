<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

$model = Mage::getModel('catalog/category');

$params = array(
    'parent_id' => Mage::app()->getDefaultStoreView()->getRootCategoryId(),
    'position' => 999,
    'is_active' => false,
    'include_in_menu' => false,
    'name' => 'Produttori',
    'display_mode' => Mage_Catalog_Model_Category::DM_PAGE,
    'is_anchor' => false,
    'url_key' => 'produttori'
);
$model->addData($params);

/** @var Mage_Catalog_Model_Category $manufacturersRootCategory */
$manufacturersRootCategory = $model->save();
$manufacturersRootCategory->setData('level', 2)->save();
$manufacturersRootCategory->setData('level', Mage::app()->getDefaultStoreView()->getRootCategoryId())->save();
$manufacturersRootCategory->setData('path', sprintf('1/2/%s', $manufacturersRootCategory->getId()))->save();

Mage::getConfig()->saveConfig('kosmosol/manufacturers/root', $manufacturersRootCategory->getId());

$params = array(
    'title' => 'Manufacturers root block',
    'identifier' => 'manufacturers_root_block',
    'stores' => array(Mage::app()->getDefaultStoreView()->getId()),
    'content' => '',
    'is_active' => false
);

/** @var Mage_Cms_Model_Block $manufacturersRootBlock */
$manufacturersRootBlock = Mage::getModel('cms/block')->addData($params)->save();

$manufacturersRootCategory->setData('landing_page', $manufacturersRootBlock->getId())->save();
