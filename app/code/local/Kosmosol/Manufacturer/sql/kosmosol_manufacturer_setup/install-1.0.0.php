<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = new Mage_Catalog_Model_Resource_Setup('catalog_setup');

$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'manufacturer', array(
    'label' => 'Manufacturer',
    'type' => 'text',
    'unique' => true,
    'group' => 'Manufacturers',
    'comment' => 'Refers to Product manufacturer attribute value',
    'required' => false
));
$installer->endSetup();
