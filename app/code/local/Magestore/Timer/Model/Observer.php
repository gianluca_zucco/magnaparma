<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Model_Observer {

    /**
     * process controller_action_predispatch event
     *
     * @return Magestore_Timer_Model_Observer
     */
    protected $_count = 0;
    public function countdowntimer($observer) {
        if(Mage::app()->getRequest()->getControllerName() == 'category' || Mage::app()->getRequest()->getControllerName() == 'result'){
            $_block = $observer->getBlock();
            
            $_type = $_block->getType();
            if ($_type == 'catalog/product_price' || $_type == 'bundle/catalog_product_price') {
                
                $productid = $_block->getProduct()->getEntityId();
                $_child = clone $_block;
                $currentDate = Mage::getModel('core/date')->date('Y-m-d H:i:s');

                $timerproduct = Mage::getModel('timer/timerproduct')->load($productid, 'product_id');

                if (!empty($timerproduct)) {
                    $timerid = $timerproduct->getTimerId();
                    $timer = Mage::getModel('timer/timer')->load($timerproduct->getTimerId());
                    $todate = Mage::helper('timer')->toLocaleTimezone($timer->getEndTime());
                    $fromdate = Mage::helper('timer')->toLocaleTimezone($timer->getStartTime());
                    if ($timer->getEndTime() != null) {
                        if (strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate)) {
                            $_child->setType('core/template');
                            $_block->setChild('child_'.$timerid, $_child);
                            $this->_count++;
                            $_block->setCount($this->_count)->setCountdown($timerid)->setTemplate('timer/product/list.phtml');
                        }
                        if($timer->getStatus()==1 && strtotime($todate) < strtotime($currentDate)){
                            $timer->setStatus('2')->save();
                        }
                    }
                }
            }
        }
    }


}
