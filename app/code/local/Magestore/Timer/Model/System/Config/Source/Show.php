<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Model_System_Config_Source_Show
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => showall, 'label'=>Mage::helper('adminhtml')->__('Show in catalog/product pages')),
            array('value' => listpage, 'label'=>Mage::helper('adminhtml')->__('Show in catalog pages')),
            array('value' => viewpage, 'label'=>Mage::helper('adminhtml')->__('Show in product pages')),
            array('value' => hideall, 'label'=>Mage::helper('adminhtml')->__('Hide in all pages')),
           
        );
    }

}
