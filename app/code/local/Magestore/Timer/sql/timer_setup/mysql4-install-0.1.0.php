<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
$installer = $this;

$installer->startSetup();

/**
 * create timer table
 */
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('magestore_pricecountdown_timer')};

CREATE TABLE {$this->getTable('magestore_pricecountdown_timer')} (
  `timer_id` int(11) unsigned NOT NULL auto_increment,
  `timername` varchar(255) NOT NULL default '',
  `timerheading` varchar(255) NOT NULL default '',
  `type` smallint(6) NOT NULL default '1',
  `catalogpage` smallint(6) NOT NULL default '1',
  `productpage` smallint(6) NOT NULL default '1',
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` smallint(6) NOT NULL default '1',
  PRIMARY KEY (`timer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('magestore_pricecountdown_timerproduct')};

CREATE TABLE {$this->getTable('magestore_pricecountdown_timerproduct')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `product_id` int(11) NOT NULL,
  `timer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();

