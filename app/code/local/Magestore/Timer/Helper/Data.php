<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Helper_Data extends Mage_Core_Helper_Abstract {

   
	const XML_PATH_ENABLED       = 'timer/general/enabled';
	
	const XML_PATH_TIMER_HEADING = 'timer/general/timer_heading';
	
	const XML_PATH_TIMER_CAPTION = 'timer/general/timer_catption';
	
	const XML_PATH_TITLE 		 = 'timer/general/title';
	
	public function conf($code, $store = null){
		return Mage::getStoreConfig($code, $store);
	}
	
	public function isEnabled($store = null){
		return $this->conf(self::XML_PATH_ENABLED, $store);
	}
	
	public function isTimerHeading(){
		return $this->conf(self::XML_PATH_TIMER_HEADING, $store);
	}
	
	public function isTimerCaption(){
		return $this->conf(self::XML_PATH_TIMER_CAPTION, $store);
	}
	
	public function getTimerTitle(){
		return $this->conf(self::XML_PATH_TITLE, $store);
	}
	
	public function isShowTitle($currentpage = null){
		if($this->isTimerHeading() == 'showall'){
			return true;
		} else if($this->isTimerHeading() == $currentpage){
			return true;
		} else if($this->isTimerHeading() != 'hideall'){
			return false;
		} else {
			return false;
		}
	}
	
	public function isShowCaption($currentpage = null){
		if ($this->isTimerCaption() == 'showall'){
			return true;
		} else if($this->isTimerCaption() == $currentpage) {
			return true;
		} else if($this->isTimerCaption() != 'hideall'){
				return false;
		}else {
			return false;
		}
	}
        public function toLocaleTimezone($date, $format = null){
        $utcZone = new DateTimeZone('UTC');
        $date = new DateTime($date, $utcZone);
        $localeZone = new DateTimeZone(Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE));
        $date->setTimezone($localeZone);
        if($format == null){
            $format = 'Y-m-d H:i:s';
        }
        return $date->format($format);
    }
}
