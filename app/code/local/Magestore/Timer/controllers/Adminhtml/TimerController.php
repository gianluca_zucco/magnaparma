<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Adminhtml_TimerController extends Mage_Adminhtml_Controller_Action {

    /**
     * init layout and set active for current menu
     *
     * @return Magestore_Timer_Adminhtml_TimerController
     */
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('timer/timer')
                ->_addBreadcrumb(
                        Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager')
        );
        return $this;
    }

    /**
     * index action
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * view and edit item action
     */
    public function timertadAction() {
        // used for selecting customers on tab load
//        $saved_timer_id = array(); // your load logic here

        $this->loadLayout()
                ->getLayout()
                ->getBlock('timer.tab.grid')
                ->setProducts($this->getRequest()->getPost('entityid', null));

        $this->renderLayout();
    }

    public function productsGridAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('timer.tab.grid')
                ->setProducts($this->getRequest()->getPost('entityid', null));
        $this->renderLayout();
    }

    public function editAction() {
//        if (!Mage::helper('magenotification')->checkLicenseKeyAdminController($this)) {
//            return;
//        }
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('timer/timer')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            Mage::register('timer_data', $model);
            $this->loadLayout();
            $this->_setActiveMenu('timer/timer');

            $this->_addBreadcrumb(
                    Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager')
            );
            $this->_addBreadcrumb(
                    Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News')
            );

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('timer/adminhtml_timer_edit'))
                    ->_addLeft($this->getLayout()->createBlock('timer/adminhtml_timer_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('timer')->__('Item does not exist')
            );
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    /**
     * save item action
     */
    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {

            $model = Mage::getModel('timer/timer');
            $modelproduct = Mage::getModel('timer/timerproduct');
            $entityid = $this->getRequest()->getParam('entityid');
            $entityid = Mage::helper('adminhtml/js')->decodeGridSerializedInput($entityid);

            $timerid = $this->getRequest()->getParam('id');
            $data['timername'] = preg_replace('/\s\s+/', ' ', trim($data['timername']));
            $timername = $model->getCollection()->addFieldToFilter('timername', $data['timername']);

            $startdate = DateTime::createFromFormat('Y-m-d H:i:s', $data['start_time']);
            $enddate = DateTime::createFromFormat('Y-m-d H:i:s', $data['end_time']);

            try {
                if ($startdate && $enddate) {
                    if (Mage::getModel('core/date')->timestamp($data['start_time']) <= Mage::getModel('core/date')->timestamp($data['end_time'])) {

                        //add new timer
                        if (empty($timerid)) {
                            //have add product
                            if (!empty($entityid)) {
                                //check timer name
																$ks_timername = $timername->getData();
                                if (empty($ks_timername)) {
                                    $timer = array();
                                    $timer['status'] = $data['status'];
                                    $timer['timername'] = $data['timername'];
                                    $timer['timerheading'] = $data['timerheading'];
                                    $timer['type'] = $data['type'];
                                    $timer['catalogpage'] = $data['catalogpage'];
                                    $timer['productpage'] = $data['productpage'];
                                    $timer['start_time'] = $model->toUTCTimezone($data['start_time']);
                                    $timer['end_time'] = $model->toUTCTimezone($data['end_time']);
                                    $model->setData($timer);
                                    $model->save();

                                    $timer_id = Mage::getModel('timer/timer')->getCollection()->getLastItem()->getTimerId();

                                    foreach ($entityid as $key => $value) {
                                        $productid = $key;
//                                    $product = Mage::getModel('catalog/product')->load($productid);
//                                    $product->setSpecialFromDate($model->toUTCTimezone($data['special_from_date']))
//                                            ->setSpecialToDate($model->toUTCTimezone($data['special_to_date']))
//                                            ->save();
                                        $product_id = Mage::getModel('timer/timerproduct')->load($productid, 'product_id');
																				$ks_product_id = $product_id->getProductId();
                                        if (!empty($ks_product_id)) {
                                            $product_id->setTimerId($timer_id)
                                                    ->save();
                                        } else {

                                            $producttimer = array();
                                            $producttimer['product_id'] = $productid;
                                            $producttimer['timer_id'] = $timer_id;
                                            $product_id->setData($producttimer);

                                            $product_id->save();
                                        }
                                    }
                                    Mage::getSingleton('adminhtml/session')->addSuccess(
                                            Mage::helper('timer')->__('Item was successfully saved'));
                                } else {
                                    Mage::getSingleton('adminhtml/session')->addError(
                                            Mage::helper('timer')->__('Countdown timer name  to exist!'));
                                }
                            } else {
                                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('timer')->__('You have not selected any products for timer to run!'));
                            }
                        } else {

                            $timers = $model->load($timerid);
                            $countname = $timers->getTimername();

                            //edit timer have add product
                            if (!empty($entityid)) {
                                //check name timer
																$ks_timername = $timername->getData();
                                if (empty($ks_timername)) {
                                    $idtimer = Mage::getModel('timer/timerproduct')->getCollection()->addFieldToFilter('timer_id', $timerid);
                                    foreach ($idtimer as $obj) {
                                        $timerproduct = Mage::getModel('timer/timerproduct')->load($obj->getProductId(), 'product_id');
                                        $timerproduct->delete();
                                    }
                                    $timers->setStatus($data['status'])
                                            ->setTimername($data['timername'])
                                            ->setTimerheading($data['timerheading'])
                                            ->setType($data['type'])
                                            ->setCatalogpage($data['catalogpage'])
                                            ->setProductpage($data['productpage'])
                                            ->setStartTime($model->toUTCTimezone($data['start_time']))
                                            ->setEndTime($model->toUTCTimezone($data['end_time']))
                                            ->save();
                                    foreach ($entityid as $key => $value) {
                                        $productid = $key;
//                                    $product = Mage::getModel('catalog/product')->load($productid);
//                                    $product->setSpecialFromDate($model->toUTCTimezone($data['special_from_date']))
//                                            ->setSpecialToDate($model->toUTCTimezone($data['special_to_date']))
//                                            ->save();
                                        $product_id = Mage::getModel('timer/timerproduct')->load($productid, 'product_id');
																				$ks_product_id = $product_id->getProductId();
                                        if (!empty($ks_product_id)) {
                                            $product_id->setTimerId($timerid)
                                                    ->save();
                                        } else {

                                            $producttimer = array();
                                            $producttimer['product_id'] = $productid;
                                            $producttimer['timer_id'] = $timerid;
                                            $product_id->setData($producttimer);

                                            $product_id->save();
                                        }
                                    }
                                    Mage::getSingleton('adminhtml/session')->addSuccess(
                                            Mage::helper('timer')->__('Item was successfully saved'));
                                } else {

                                    if ($countname != $data['timername']) {
                                        Mage::getSingleton('adminhtml/session')->addError(
                                                Mage::helper('timer')->__('Countdown timer name  to exist!'));
                                    } else {
                                        $idtimer = Mage::getModel('timer/timerproduct')->getCollection()->addFieldToFilter('timer_id', $timerid);
                                        foreach ($idtimer as $obj) {
                                            $timerproduct = Mage::getModel('timer/timerproduct')->load($obj->getProductId(), 'product_id');
                                            $timerproduct->delete();
                                        }
                                        $timers->setStatus($data['status'])
                                                ->setTimerheading($data['timerheading'])
                                                ->setType($data['type'])
                                                ->setCatalogpage($data['catalogpage'])
                                                ->setProductpage($data['productpage'])
                                                ->setStartTime($model->toUTCTimezone($data['start_time']))
                                                ->setEndTime($model->toUTCTimezone($data['end_time']))
                                                ->save();

                                        foreach ($entityid as $key => $value) {
                                            $productid = $key;
//                                        $product = Mage::getModel('catalog/product')->load($productid);
//                                        $product->setSpecialFromDate($model->toUTCTimezone($data['special_from_date']))
//                                                ->setSpecialToDate($model->toUTCTimezone($data['special_to_date']))
//                                                ->save();
                                            $product_id = Mage::getModel('timer/timerproduct')->load($productid, 'product_id');
																						$ks_product_id = $product_id->getProductId();
                                            if (!empty($ks_product_id)) {
                                                $product_id->setTimerId($timerid)
                                                        ->save();
                                            } else {

                                                $producttimer = array();
                                                $producttimer['product_id'] = $productid;
                                                $producttimer['timer_id'] = $timerid;
                                                $product_id->setData($producttimer);

                                                $product_id->save();
                                            }
                                        }
                                        Mage::getSingleton('adminhtml/session')->addSuccess(
                                                Mage::helper('timer')->__('Item was successfully saved'));
                                    }
                                }
                            } else {
                                if (isset($data['entityid'])) {
                                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('timer')->__('Countdown timer have at least one product !'));
                                } else {
                                    //check name timer
																		$ks_timername = $timername->getData();
                                    if (empty($ks_timername)) {
                                        $timers->setStatus($data['status'])
                                                ->setTimername($data['timername'])
                                                ->setTimerheading($data['timerheading'])
                                                ->setType($data['type'])
                                                ->setCatalogpage($data['catalogpage'])
                                                ->setProductpage($data['productpage'])
                                                ->setStartTime($model->toUTCTimezone($data['start_time']))
                                                ->setEndTime($model->toUTCTimezone($data['end_time']))
                                                ->save();

                                        Mage::getSingleton('adminhtml/session')->addSuccess(
                                                Mage::helper('timer')->__('Item was successfully saved'));
                                    } else {

                                        if ($countname != $data['timername']) {
                                            Mage::getSingleton('adminhtml/session')->addError(
                                                    Mage::helper('timer')->__('Countdown timer name  to exist!'));
                                        } else {
                                            $timers->setStatus($data['status'])
                                                    ->setTimerheading($data['timerheading'])
                                                    ->setType($data['type'])
                                                    ->setCatalogpage($data['catalogpage'])
                                                    ->setProductpage($data['productpage'])
                                                    ->setStartTime($model->toUTCTimezone($data['start_time']))
                                                    ->setEndTime($model->toUTCTimezone($data['end_time']))
                                                    ->save();
                                            Mage::getSingleton('adminhtml/session')->addSuccess(
                                                    Mage::helper('timer')->__('Item was successfully saved'));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('timer')->__('Start time must be smaller than end time!'));
                    }
                } else {
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('timer')->__('Error format date !'));
                }
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
//        Mage::getSingleton('adminhtml/session')->addError(
//                Mage::helper('timer')->__('Unable to find item to save')
//        );
        $this->_redirect('*/*/');
    }

    /**
     * delete item action
     */
    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $id = $this->getRequest()->getParam('id');
                $timers = Mage::getModel('timer/timer')->load($id);
                $collection = Mage::getModel('timer/timerproduct')->getCollection();
                $collection->addFieldToFilter('timer_id', $id);
//                $specialfromdate = '';
//                $specialtodate = '';
                foreach ($collection as $obj) {
                    $timerproduct = Mage::getModel('timer/timerproduct')->load($obj->getProductId(), 'product_id');
                    $timerproduct->delete();


//                $product = Mage::getModel('catalog/product')->load($obj->getProductId());
//                $product->setSpecialFromDate($specialfromdate)
//                        ->setSpecialToDate($specialtodate)
//                        ->save();
                }

                $timers->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__('Item was successfully deleted')
                );
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * mass delete item(s) action
     */
    public function massDeleteAction() {
        $timerIds = $this->getRequest()->getParam('timer');
        if (!is_array($timerIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {

                foreach ($timerIds as $time) {
                    $timers = Mage::getModel('timer/timer')->load($time);
                    $idtimer = Mage::getModel('timer/timerproduct')->getCollection()->addFieldToFilter('timer_id', $time);
                    foreach ($idtimer as $obj) {
                        $timerproduct = Mage::getModel('timer/timerproduct')->load($obj->getProductId(), 'product_id');
                        $timerproduct->delete();
                    }

                    $timers->delete();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($timerIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass change status for item(s) action
     */
    public function massStatusAction() {
        $timerIds = $this->getRequest()->getParam('timer');
        if (!is_array($timerIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($timerIds as $timerId) {
                    $timer = Mage::getModel('timer/timer')->load($timerId);
                    $timer->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($timerIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass change show/hiden product for item(s) action
     */
    public function massProductAction() {
        $timerIds = $this->getRequest()->getParam('timer');
        if (!is_array($timerIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($timerIds as $timerId) {



                    $timers = Mage::getModel('timer/timer')->load($timerId);
                    $timers->setProductpage($this->getRequest()->getParam('product'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($timerIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massCatalogAction() {
        $timerIds = $this->getRequest()->getParam('timer');
        if (!is_array($timerIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($timerIds as $timerId) {

                    $timers = Mage::getModel('timer/timer')->load($timerId);
                    $timers->setCatalogpage($this->getRequest()->getParam('catalog'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($timerIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('timer');
    }

}
