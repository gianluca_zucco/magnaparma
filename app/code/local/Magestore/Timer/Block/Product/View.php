<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Block_Product_View extends Mage_Catalog_Block_Product_View {

    public function getProductId() {
        return $this->getProduct()->getEntityId();
    }

    public function getCountDown() {
        $currentDate = Mage::getModel('core/date')->date('Y-m-d H:i:s');
//    		$todate = $this->toLocaleTimezone($this->getProduct()->getSpecialToDate());
//    		$fromdate =$this->toLocaleTimezone($this->getProduct()->getSpecialFromDate());

        $productid = $this->getProductId();

        $timerproduct = Mage::getModel('timer/timerproduct')->load($productid, 'product_id');
        $timer = Mage::getModel('timer/timer')->load($timerproduct->getTimerId());
        $todate = Mage::helper('timer')->toLocaleTimezone($timer->getEndTime());
        $fromdate = Mage::helper('timer')->toLocaleTimezone($timer->getStartTime());


        if ($timer->getEndTime() != null) {
            if (strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate)) {
                return true;
            }
        }
    }


}
