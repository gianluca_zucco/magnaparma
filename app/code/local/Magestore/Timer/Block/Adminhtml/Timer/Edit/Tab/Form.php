<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Block_Adminhtml_Timer_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * prepare tab form's information
     *
     * @return Magestore_Timer_Block_Adminhtml_Timer_Edit_Tab_Form
     */
    public function __construct() {
        parent::__construct();
    }

    protected function _prepareForm() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('timer/timer')->load($id);
        $t=$model->getType();
        if(empty($t)){
            $t=0;
        }
        
        $form = new Varien_Data_Form();
        $this->setForm($form);


        $fieldset = $form->addFieldset('timer_edit', array('legend' => Mage::helper('timer')->__('Timer information')));

        $image_calendar = Mage::getBaseUrl('skin') . 'adminhtml/default/default/images/grid-cal.gif';
        $disabled = false;
        $disabled = ($data['status'] == 4) ? true : $disabled;


        $fieldset->addField('timername', 'text', array(
            'label' => Mage::helper('timer')->__('Countdown Timer Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'timername',
            'disabled' => $disabled,
            'note' => '',
        ));
        $fieldset->addField('timerheading', 'text', array(
            'label' => Mage::helper('timer')->__('Countdown Timer Heading'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'timerheading',
            'note' => '',
        ))->setAfterElementHtml("<img id='headding' src='".$this->getSkinUrl('images/timer/style3.png')."' style='WIDTH: 200px; height: 90px; margin-top:10px'/>
                         ");

        $time_zone = $this->__('Time Zone (PST Time): %s', Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE));

        $startTime = $fieldset->addField('start_time', 'date', array(
            'name' => 'start_time',
            'label' => Mage::helper('timer')->__('Start time'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => 'y-MM-dd HH:mm:00',
            'time' => true,
            'required' => true,
            'style' => 'width:255px;',
            'note' => $time_zone, //was converted in model _afterLoad
        ));
        $startTime = $fieldset->addField('end_time', 'date', array(
            'name' => 'end_time',
            'label' => Mage::helper('timer')->__('End time'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => 'y-MM-dd HH:mm:00',
            'time' => true,
            'required' => true,
            'style' => 'width:255px;',
            'note' => $time_zone, //was converted in model _afterLoad
        ));





       $template = $fieldset->addField('type', 'select', array(
            'label' => Mage::helper('timer')->__('Style'),
            'class' => 'required-entry',
            'required' => FALSE,
            'name' => 'type',
            'values' => array('0' => 'Simple style', '1' => 'Mini-frame style', '2' => 'Full-frame style'),
            'onchange' => 'checkSelectedItem(this.value)',
        ));
        
       $template->setAfterElementHtml("<img id='myimg' src='' style='WIDTH: 200px; height: 90px; margin-top:10px'/>
                         <script type=\"text/javascript\">
                            var image = document.getElementById('myimg');              
                             image.src='".$this->getSkinUrl('images/timer/style'.$t.'.png')."';
                            function checkSelectedItem(selectElement){ 
                               var image = document.getElementById('myimg');
                               var style = $('type')[$('type').selectedIndex].value;
           
                               if(style==0){
                               image.src='".$this->getSkinUrl('images/timer/style0.png')."';
                               }if(style==1){
                               image.src='".$this->getSkinUrl('images/timer/style1.png')."';
                               }if(style==2){
                               image.src='".$this->getSkinUrl('images/timer/style2.png')."';
                               }
                            }
                         </script>");

        $fieldset->addField('catalogpage', 'select', array(
            'label' => Mage::helper('timer')->__('Show in Catalog'),
            'class' => 'required-entry',
            'required' => FALSE,
            'name' => 'catalogpage',
            'values' => array('2' => 'Hide', '1' => 'Show'),
        ));
        $fieldset->addField('productpage', 'select', array(
            'label' => Mage::helper('timer')->__('Show in Product page'),
            'class' => 'required-entry',
            'required' => FALSE,
            'name' => 'productpage',
            'values' => array('2' => 'Hide', '1' => 'Show'),
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('timer')->__('Status'),
            'name' => 'status',
            'values' => Mage::getSingleton('timer/status')->getOptionHash(),
        ));
        if (Mage::getSingleton('adminhtml/session')->getTimerData()) {
            $data = Mage::getSingleton('adminhtml/session')->getTimerData();
            Mage::getSingleton('adminhtml/session')->setTimerData(null);
        } elseif (Mage::registry('timer_data')) {
            $data = Mage::registry('timer_data')->getData();
            if (empty($data)) {
                $data['type'] = 0;
                $data['catalogpage'] = 1;
                $data['productpage'] = 1;
            }

            $data['start_time'] = $this->toLocaleTimezone($data['start_time']);
            $data['end_time'] = $this->toLocaleTimezone($data['end_time']);
        }



        $form->setValues($data);
        return parent::_prepareForm();
    }

    public function toLocaleTimezone($date, $format = null) {
        $utcZone = new DateTimeZone('UTC');
        $date = new DateTime($date, $utcZone);
        $localeZone = new DateTimeZone(Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE));
        $date->setTimezone($localeZone);
        if ($format == null) {
            $format = 'Y-m-d H:i:s';
        }
        return $date->format($format);
    }

}
