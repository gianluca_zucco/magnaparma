<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Block_Adminhtml_Timer_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {

        parent::__construct();
        $this->setId('timerGrid');
        $this->setDefaultSort('timer_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
    }
     protected function _getStore() {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
    
    /**
     * prepare collection for block to display
     *
     * @return Magestore_Timer_Block_Adminhtml_Timer_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getModel('timer/timer')->getCollection();
               
        $this->setCollection($collection);
        return parent::_prepareCollection();
       
    }

    /**
     * prepare columns for this grid
     *
     * @return Magestore_Timer_Block_Adminhtml_Timer_Grid
     */
    protected function _prepareColumns() {
       
        $this->addColumn('timer_id', array(
            'header' => Mage::helper('timer')->__('ID'),
            'align' => 'center',
            'width' => '50px',
            'index' => 'timer_id',
        ));
        $this->addColumn('timername', array(
            'header' => Mage::helper('timer')->__('Timer Name'),
            'align' => 'center',
            'index' => 'timername',
   
        ));
        $this->addColumn('timerheading', array(
            'header' => Mage::helper('timer')->__('Countdown Timer Heading'),
            'align' => 'center',
            'index' => 'timerheading',
     
        ));

        $this->addColumn('start_time', array(
            'header' => Mage::helper('timer')->__('Start Time'),
            'align' => 'center',
            'width' => '200px',
            'index' => 'start_time',
            'type' => 'datetime',
  
         
        ));

        $this->addColumn('end_time', array(
            'header' => Mage::helper('timer')->__('End Time'),
            'align' => 'center',
            'width' => '200px',
            'index' => 'end_time',
            'type' => 'datetime',

           
        ));


        $this->addColumn('catalogpage', array(
            'header_css_class' => 'a-center',
            'header' => Mage::helper('timer')->__('Show in catalog'),
            'index' => 'catalogpage',
            'type' => 'checkbox',
            'width' => '100px',
            'align' => 'center',
            'values' => array('1'),
            'filter' => false,   
            'renderer'=>'Magestore_Timer_Block_Adminhtml_Timer_Renderer_Checkbox',
        ));
        $this->addColumn('productpage', array(
            'header_css_class' => 'a-center',
            'header' => Mage::helper('timer')->__('Show in product pages'),
            'index' => 'productpage',
            'width' => '100px',
            'type' => 'checkbox',
            'align' => 'center',
            'values' => array('1'),
            'filter' => false,
            'renderer'=>'Magestore_Timer_Block_Adminhtml_Timer_Renderer_Checkbox',
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('timer')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                2 => 'Disabled',
                1 => 'Enabled',
            ),
        ));
        $this->addColumn('action', array(
            'header' => Mage::helper('timer')->__('Action'),
            'width' => '100',
            'align' => 'center',
            'type' => 'action',
            'getter' => 'getTimerId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('timer')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));


        return parent::_prepareColumns();
    }

    /**
     * prepare mass action for this grid
     *
     * @return Magestore_Timer_Block_Adminhtml_Timer_Grid
     */
    protected function _prepareMassaction() {
        $this->setMassactionIdField('timer_id');
        $this->getMassactionBlock()->setFormFieldName('timer');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('timer')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('timer')->__('Are you sure?')
        ));

        
        $statuses = Mage::getSingleton('timer/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('timer')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('timer')->__('Status'),
                    'values' => $statuses
                ))
        ));
        $products = array(
            1 => Mage::helper('timer')->__('Show'),
            2 => Mage::helper('timer')->__('Hide')
        );
        array_unshift($products, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('product', array(
            'label' => Mage::helper('timer')->__('Show/hide in product pages'),
            'url' => $this->getUrl('*/*/massProduct', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'product',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('timer')->__('Product'),
                    'values' => $products
                ))
        ));

        $catalog = array(
            1 => Mage::helper('timer')->__('Show'),
            2 => Mage::helper('timer')->__('Hide')
        );
        array_unshift($catalog, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('catalog', array(
            'label' => Mage::helper('timer')->__('Show/hide in catalog'),
            'url' => $this->getUrl('*/*/massCatalog', array('_current' => true, 'show' => '1')),
            'additional' => array(
                'visibility' => array(
                    'name' => 'catalog',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('timer')->__('Catalog list'),
                    'values' => $catalog
                ))
        ));

        return $this;
    }

    /**
     * get url for each row in grid
     *
     * @return string
     */
    

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getTimerId()));
    }

}
