<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Block_Adminhtml_Timer_Edit_Tab_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('list_product_grid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        
        $this->setSaveParametersInSession(TRUE);
        $this->setUseAjax(true);
        if (($this->getRequest()->getParam('id'))) {
            $this->setDefaultFilter(array('in_products' => 1));
        }    
    }
    protected function _getStore() {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
    
    
    //load product with prameter id
    protected function _addColumnFilterToCollection($column) {

        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds))
                $productIds = 0;
            if ($column->getFilter()->getValue())
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            elseif ($productIds)
                $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
            return $this;
        }
        return parent::_addColumnFilterToCollection($column);
    }

    protected function _prepareCollection() {
            $collection = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect('sku')
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('special_price')
                   ->addAttributeToFilter('visibility', 4);
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
    protected function _prepareColumns() {
        $this->addColumn('in_products', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'index' => 'entity_id',
            'name' => 'in_products',
            'align' => 'center',
            'values' => $this->_getSelectedProducts(),
        ));


        $this->addColumn('entity_id', array(
            'header' => Mage::helper('timer')->__('ID'),
            'align' => 'center',
            'width' => '50px',
            'index' => 'entity_id',
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('timer')->__('Name'),
            'width' => '400px',
            'type' => 'text',
            'index' => 'name',
            'align' => 'center',
        ));

        $this->addColumn('sku', array(
            'header' => Mage::helper('timer')->__('SKU'),
            'type' => 'text',
            'index' => 'sku',
            'align' => 'center',
        ));

            $this->addColumn('special_price', array(
                'header' => Mage::helper('timer')->__(''),
                'name' => 'special_price',
                'index' => 'special_price',
                'align' => 'center',
                'width' => '0px',
                'editable' => true,
                'edit_only' => true,
                'column_css_class'=>'no-display',
                'header_css_class'=>'no-display',

            ));
        



        return parent::_prepareColumns();
        
    }
    protected function _getSelectedProducts() {
        $productArrays = $this->getProducts();
         
        $products = '';
        $adjustProducts = array();
        if ($productArrays) {
            $products = array();
            foreach ($productArrays as $productArray) {
                parse_str(urldecode($productArray), $adjustProducts);
                if (count($adjustProducts)) {
                    foreach ($adjustProducts as $pId => $enCoded) {
                        $products[] = $pId;
                    }
                }
            }
        }
        if ((!is_array($products))) {
            $products = array_keys($this->getProductSelect());
        }
        return $products;
    }

    public function getProductSelect() {
        $product = array();
        $tm_id = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('timer/timerproduct')->getCollection();
        $collection->addFieldToFilter('timer_id', $tm_id);

        // zend_debug::dump($collection->getData());die;
        
        foreach ($collection as $obj) {
            if ($obj->getId())
            $product[$obj->getProductId()] = array('product_id' => $obj->getID());
        }
        
        return $product;
        
        
    }
    public function getGridUrl() {
        return $this->getUrl('*/*/productsGrid', array(
                    '_current' => true,
                    'id' => $this->getRequest()->getParam('id')
        ));
    }

    public function getRowUrl($row) {
        
    }
    /**
     * get url for each row in grid
     *
     * @return string
     */
}
