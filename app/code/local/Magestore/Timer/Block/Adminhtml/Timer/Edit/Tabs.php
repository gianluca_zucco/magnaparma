<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Block_Adminhtml_Timer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('timer_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('timer')->__('Timer Information'));
    }

    /**
     * prepare before render block to html
     *
     * @return Magestore_Timer_Block_Adminhtml_Timer_Edit_Tabs
     */
    protected function _beforeToHtml() {

 
            $this->addTab('form_section', array(
                'label' => Mage::helper('timer')->__('General Information'),
                'title' => Mage::helper('timer')->__('General Information'),
                'content' => $this->getLayout()->createBlock('timer/adminhtml_timer_edit_tab_form')->toHtml(),
            ));
            $this->addTab('form_listproduct', array(
                'label' => $this->__('Select Product'),
                'title' => $this->__('Select Product'),
                'url' => $this->getUrl('*/*/timertad', array('_current' => true,'id' => $this->getRequest()->getParam('id'))),
                'class' => 'ajax'
            ));


        return parent::_beforeToHtml();
    }

}
