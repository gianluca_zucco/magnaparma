<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Inventory
 * @copyright   Copyright (c) 2009 - 2015 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * 
 * 
 * @category    Magestore
 * @package     Magestore_Timer
 * @author      Magestore Developer
 */
class Magestore_Timer_Block_Adminhtml_Timer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'timer';
        $this->_controller = 'adminhtml_timer';

        $this->_updateButton('save', 'label', Mage::helper('timer')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('timer')->__('Delete Item'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), -100);

        $this->_formScripts[] = "
			function toggleEditor() {
				if (tinyMCE.getInstanceById('timer_content') == null)
					tinyMCE.execCommand('mceAddControl', false, 'timer_content');
				else
					tinyMCE.execCommand('mceRemoveControl', false, 'timer_content');
			}

			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

    /**
     * get text to show in header when edit an item
     *
     * @return string
     */
    public function getHeaderText()
    {
        if( Mage::registry('timer_data') && Mage::registry('timer_data')->getId() ) {
            if(Mage::registry('timer_data')->getStatus() == 5)
			{
				return Mage::helper('timer')->__("Timer Infomation");			
			} else {
				return Mage::helper('timer')->__("Edit Timer for '%s'", $this->htmlEscape(Mage::registry('timer_data')->getTimername()));
			}
		} else {
            return Mage::helper('timer')->__('Add Timer');
        }
    }

}
