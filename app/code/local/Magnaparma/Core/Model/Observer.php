<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */
class Magnaparma_Core_Model_Observer
{
    public function resolve404LoginAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Adminhtml_IndexController $action */
        $action = $observer->getControllerAction();
        $action->getResponse()->setRedirect($action->getUrl('*/dashboard'), 302);
        $action->setFlag('', $action::FLAG_NO_DISPATCH, true);
    }
}
