<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 *
 * @var $this Mage_Sales_Model_Resource_Setup
 */

$installer = $this;

$data = array(
    'label' => 'Extra fee',
    'type' => 'decimal',
    'grid' => 'true'
);

$installer->startSetup();
$installer->addAttribute('order', 'extra_fee_amount', $data);
$installer->addAttribute('order', 'base_extra_fee_amount', $data);
$installer->endSetup();
