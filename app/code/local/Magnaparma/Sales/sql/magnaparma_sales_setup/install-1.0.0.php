<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 *
 * @var $this Mage_Sales_Model_Resource_Setup
 */

$installer = $this;

$data = array(
    'label' => 'Extra fee',
    'type' => 'decimal',
    'grid' => 'true'
);

$installer->startSetup();
$installer->addAttribute('quote_address', 'extra_fee_amount', $data);
$installer->addAttribute('quote_address', 'base_extra_fee_amount', $data);
$installer->endSetup();
