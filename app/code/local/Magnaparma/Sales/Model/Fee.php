<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */
class Magnaparma_Sales_Model_Fee extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $_code = 'extra_fee';

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     * @return $this
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);
        $address->setData('extra_fee_amount', 0);
        $address->setData('base_extra_fee_amount', 0);
        $subtotalIncludingTaxAndDiscount = $address->getSubtotalInclTax() - abs($address->getDiscountAmount());
        if ($address->getAddressType() == Mage_Sales_Model_Quote_Address::TYPE_SHIPPING) {
            if ($subtotalIncludingTaxAndDiscount <= $this->getMinimumSubtotalAmount()) {
                $address->setData('extra_fee_amount', $this->getExtraFeeAmount());
                $address->setData('base_extra_fee_amount', $this->getExtraFeeAmount());
                $address->setGrandTotal($address->getGrandTotal() + $address->getData('extra_fee_amount'));
                $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getData('base_extra_fee_amount'));
            }
        }
        return $this;
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     * @return $this
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getData('extra_fee_amount');
        if ($amount) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('magnaparma_sales')->__('Extra fee'),
                'value' => $amount
            ));
        }
        return $this;
    }

    /**
     * @return float
     */
    private function getMinimumSubtotalAmount()
    {
        return (float) Mage::getStoreConfig('magnaparma_sales/extra_fee/minimum_subtotal_amount');
    }

    private function getExtraFeeAmount()
    {
        return (float) Mage::getStoreConfig('magnaparma_sales/extra_fee/amount');
    }


}
