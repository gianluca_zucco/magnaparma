<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */
class Magnaparma_Sales_Model_Observer
{

    /**
     * Add his own product to order item
     *
     * @param Varien_Event_Observer $observer
     */
    public function bindProductSelection(Varien_Event_Observer $observer)
    {
        if (Mage::getStoreConfigFlag('magnaparma_sales/email_settings/add_product_image')) {
            /** @var Mage_Sales_Model_Resource_Order_Item_Collection $collection */
            $collection = $observer->getOrderItemCollection();
            $productIds = array_map(function(Mage_Sales_Model_Order_Item $item) {
                return $item->getProductId();
            }, $collection->getItems());
            /** @var Mage_Catalog_Model_Resource_Product_Collection $products */
            $attributes = array('thumbnail', 'thumbnail_label', 'small_image', 'small_image_label', 'url_key');
            $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($attributes)
                ->addFieldToFilter('entity_id', array('in' => $productIds))
                ->load();
            $collection->walk(function(Mage_Sales_Model_Order_Item $item) use ($products) {
                $item->setProduct($products->getItemById($item->getProductId()));
            });
        }
    }
}
