<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Magnaparma_Export_Adminhtml_Magnaparma_Export_DhlController extends Mage_Adminhtml_Controller_Action
{
    public function exportAction()
    {
        $export = Mage::getModel('magnaparma_export/order_dhl');
        try {
            /** @var Magnaparma_Export_Model_Order_Dhl $export */
            $ids = array_map('trim', $this->getRequest()->getPost('order_ids', array()));
            $export->setOrderIds($ids)->run();
            $this->_prepareDownloadResponse(
                $export->getFilename(),
                file_get_contents($export->getFilepath()),
                'text/plain'
            );
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }
    }

    protected function _isAllowed()
    {
        return true;
    }
}
