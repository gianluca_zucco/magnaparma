<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 *
 * @var $installer Mage_Sales_Model_Entity_Setup
 */

$installer = new Mage_Sales_Model_Entity_Setup('core_setup');

$installer->startSetup();
$code = 'dhl_exported';
$installer->addAttribute(Mage_Sales_Model_Order::ENTITY, $code, array(
    'label' => 'DHL exported',
    'input' => 'select',
    'type' => 'int',
    'visible' => true,
    'required' => false,
    'source' => 'eav/entity_attribute_source_boolean',
    'default' => 0,
    'note' => 'Automatically flagged after order export'
));
$installer->getConnection()->addColumn($installer->getTable('sales/order'), $code, array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'length' => null,
    'comment' => 'Flag DHL export'
));
$installer->endSetup();
