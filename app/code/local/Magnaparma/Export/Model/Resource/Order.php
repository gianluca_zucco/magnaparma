<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Magnaparma_Export_Model_Resource_Order extends Mage_Sales_Model_Resource_Order
{
    /**
     * Flag all exported orders
     *
     * @param array $orderIds
     * @param string $column
     * @param int $flag_value
     */
    public function flag($orderIds, $column, $flag_value = 1)
    {
        $bind = array($column => $flag_value);
        $where = $this->_getWriteAdapter()->quoteInto('entity_id IN (?)', $orderIds);
        $this->_getWriteAdapter()->update($this->getMainTable(), $bind, $where);
    }
}
