<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Magnaparma_Export_Model_Order_Stef_Item extends Varien_Object
{
    const RECORD_TYPE = 'T';

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function toOrderArray(Mage_Sales_Model_Order $order)
    {
        $data = $this->_toOrderArray($order);
        $rules = array (
            'record_type' => array(
                'type' => 'A',
                'length' => 1
            ),
            'ddt' => array(
                'type' => 'A',
                'length' => 16
            ),
            'data' => array(
                'type' => 'N',
                'length' => 8
            ),
            'affidamento_merce' => array(
                'type' => 'N',
                'length' => 8
            ),
            'order_number' => array(
                'type' => 'A',
                'length' => 16
            ),
            'merchant_code' => array(
                'type' => 'A',
                'length' => 16
            ),
            'company_name' => array(
                'type' => 'A',
                'length' => 36
            ),
            'company_address' => array(
                'type' => 'A',
                'length' => 36
            ),
            'company_city' => array(
                'type' => 'A',
                'length' => 36
            ),
            'company_zip' => array(
                'type' => 'A',
                'length' => 9
            ),
            'company_state' => array(
                'type' => 'A',
                'length' => 2
            ),
            'company_country' => array(
                'type' => 'A',
                'length' => 2
            ),
            'load_code' => array(
                'type' => 'A',
                'length' => 16
            ),
            'load_name' => array(
                'type' => 'A',
                'length' => 36
            ),
            'load_address' => array(
                'type' => 'A',
                'length' => 36
            ),
            'load_city' => array(
                'type' => 'A',
                'length' => 36
            ),
            'load_zip' => array(
                'type' => 'A',
                'length' => 9
            ),
            'load_state' => array(
                'type' => 'A',
                'length' => 2
            ),
            'load_country' => array(
                'type' => 'A',
                'length' => 2
            ),
            'customer_code' => array(
                'type' => 'A',
                'length' => 16
            ),
            'ship_to_name' => array(
                'type' => 'A',
                'length' => 36
            ),
            'ship_to_address' => array(
                'type' => 'A',
                'length' => 36
            ),
            'ship_to_city' => array(
                'type' => 'A',
                'length' => 36
            ),
            'ship_to_zip' => array(
                'type' => 'A',
                'length' => 9
            ),
            'ship_to_state' => array(
                'type' => 'A',
                'length' => 2
            ),
            'ship_to_country' => array(
                'type' => 'A',
                'length' => 2
            ),
            'offload_code' => array(
                'type' => 'A',
                'length' => 16
            ),
            'offload_name' => array(
                'type' => 'A',
                'length' => 36
            ),
            'offload_address' => array(
                'type' => 'A',
                'length' => 36
            ),
            'offload_city' => array(
                'type' => 'A',
                'length' => 36
            ),
            'offload_zip' => array(
                'type' => 'A',
                'length' => 9
            ),
            'offload_state' => array(
                'type' => 'A',
                'length' => 2
            ),
            'offload_country' => array(
                'type' => 'A',
                'length' => 2
            ),
            'colli' => array(
                'type' => 'N',
                'length' => 6
            ),
            'weight' => array(
                'type' => 'N',
                'length' => 6
            ),
            'pieces' => array(
                'type' => 'N',
                'length' => 6
            ),
            'bancali' => array(
                'type' => 'N',
                'length' => 6
            ),
            'qty_complementare' => array(
                'type' => 'N',
                'length' => 6
            ),
            'um_qty_complementare' => array(
                'type' => 'A',
                'length' => 3
            ),
            'importo_contrassegno' => array(
                'type' => 'N',
                'length' => 10
            ),
            'valuta_contrassegno' => array(
                'type' => 'A',
                'length' => 3
            ),
            'modo_rimborso_contrassegno' => array(
                'type' => 'A',
                'length' => 2
            ),
            'consegna_richiesta' => array(
                'type' => 'N',
                'length' => 8
            ),
            'ora_richiesta' => array(
                'type' => 'N',
                'length' => 4
            ),
            'commenti' => array(
                'type' => 'A',
                'length' => 128
            ),
            'giro_camion' => array(
                'type' => 'A',
                'length' => 5
            ),
            'categoria_temperatura' => array(
                'type' => 'A',
                'length' => 2
            ),
            'categoria_tariffaria' => array(
                'type' => 'A',
                'length' => 4
            ),
            'natura_merce' => array(
                'type' => 'A',
                'length' => 4
            ),
            'condizioni_vendita' => array(
                'type' => 'A',
                'length' => 2
            ),
            'flag_grouped' => array(
                'type' => 'A',
                'length' => 1
            ),
            'flag_ritiro' => array(
                'type' => 'A',
                'length' => 1
            ),
            'flag_fermo' => array(
                'type' => 'A',
                'length' => 1
            ),
            'flag_annuncio' => array(
                'type' => 'A',
                'length' => 1
            ),
            'chiave_annuncio' => array(
                'type' => 'A',
                'length' => 17
            ),
            'numero_posizione' => array(
                'type' => 'A',
                'length' => 12
            ),
            'unitá_spedizione' => array(
                'type' => 'N',
                'length' => 4
            ),
            'remettant' => array(
                'type' => 'A',
                'length' => 16
            ),
            'posizione_origine' => array(
                'type' => 'A',
                'length' => 12
            ),
            'F0' => array(
                'type' => 'N',
                'length' => 4
            ),
            'F1' => array(
                'type' => 'A',
                'length' => 4
            ),
            'F2' => array(
                'type' => 'A',
                'length' => 16
            ),
            'F3' => array(
                'type' => 'A',
                'length' => 3
            ),
            'F4' => array(
                'type' => 'N',
                'length' => 6
            ),
            'F5' => array(
                'type' => 'N',
                'length' => 6
            ),
            'F6' => array(
                'type' => 'N',
                'length' => 6
            ),
            'F7' => array(
                'type' => 'N',
                'length' => 6
            ),
            'F8' => array(
                'type' => 'N',
                'length' => 6
            ),
            'F9' => array(
                'type' => 'A',
                'length' => 36
            ),
            'F10' => array(
                'type' => 'A',
                'length' => 36
            ),
            'F11' => array(
                'type' => 'A',
                'length' => 36
            ),
            'F12' => array(
                'type' => 'A',
                'length' => 9
            ),
            'F13' => array(
                'type' => 'A',
                'length' => 2
            ),
            'F14' => array(
                'type' => 'A',
                'length' => 2
            ),
            'F15' => array(
                'type' => 'A',
                'length' => 20
            ),
            'F16' => array(
                'type' => 'A',
                'length' => 15
            ),
            'F17' => array(
                'type' => 'N',
                'length' => 8
            ),
            'F18' => array(
                'type' => 'A',
                'length' => 35
            ),
            'F19' => array(
                'type' => 'N',
                'length' => 8
            ),
            'F20' => array(
                'type' => 'A',
                'length' => 35
            ),
            'F21' => array(
                'type' => 'A',
                'length' => 35
            ),
            'F22' => array(
                'type' => 'A',
                'length' => 36
            ),
            'F23' => array(
                'type' => 'A',
                'length' => 36
            ),
            'F24' => array(
                'type' => 'A',
                'length' => 36
            ),
            'F25' => array(
                'type' => 'A',
                'length' => 9
            ),
            'F26' => array(
                'type' => 'A',
                'length' => 2
            ),
            'F27' => array(
                'type' => 'A',
                'length' => 2
            ),
            'F28' => array(
                'type' => 'N',
                'length' => 4
            ),
            'F29' => array(
                'type' => 'N',
                'length' => 4
            ),
            'F30' => array(
                'type' => 'A',
                'length' => 128
            )
        );
        array_walk($data, function(&$value, $key) use ($rules) {
            $rule = $rules[$key];
            $value = substr($value,0, $rule['length']);
            $value = str_pad($value, $rule['length'], $rule['type'] == 'N' ? '0' : " ", $rule['type'] == 'N' ? STR_PAD_LEFT : STR_PAD_RIGHT);
        });
        return array_values($data);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return float|mixed
     */
    protected function _toOrderArray(Mage_Sales_Model_Order $order)
    {
        $data = $this->_getGeneralInformation($order);
        $data += $this->_getMerchantInformation();
        $data += $this->_getLoadInformation();
        $data += $this->_getShippingAddressInformation($order);
        $data += $this->_getOffloadInformation();
        $data += $this->_getAdditionalInformation($order);
        return $data;
    }

    /**
     * Return order shipment
     *
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Order_Shipment
     * @throws Mage_Core_Exception
     */
    protected function _getShipment(Mage_Sales_Model_Order $order)
    {
        /** @var Mage_Sales_Model_Resource_Order_Shipment_Collection $shipments */
        $shipments = $order->getShipmentsCollection();
        if (!$shipments || !$shipments->count()) {
            $msg = "Oder {$order->getIncrementId()} cannot be exported because of missing shipment";
            Mage::throwException($msg);
        }
        return $shipments->getFirstItem();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _getGeneralInformation(Mage_Sales_Model_Order $order)
    {
        return array(
            'record_type' => self::RECORD_TYPE,
            'ddt' => $this->_getShipment($order)->getIncrementId(),
            'data' => Mage::getModel('core/date')->date('Ymd'),
            'affidamento_merce' => '',
            'order_number' => $order->getIncrementId()
        );
    }

    /**
     * Retrieve merchant information
     * @return array
     */
    protected function _getMerchantInformation()
    {
        return array(
            'merchant_code' => Mage::getStoreConfig('magnaparma_export/order_stef/merchant_code'),
            'company_name' => Mage::getStoreConfig('general/store_information/company_name'),
            'company_address' => Mage::getStoreConfig('general/store_information/company_address'),
            'company_city' => Mage::getStoreConfig('general/store_information/company_city'),
            'company_zip' => Mage::getStoreConfig('general/store_information/company_zip'),
            'company_state' => Mage::getStoreConfig('general/store_information/company_state'),
            'company_country' => Mage::getStoreConfig('general/store_information/company_country'),
        );
    }

    /**
     * Retrieve load information
     * @return array
     */
    protected function _getLoadInformation()
    {
        return array(
            'load_code' => null,
            'load_name' => null,
            'load_address' => null,
            'load_city' => null,
            'load_zip' => null,
            'load_state' => null,
            'load_country' => null,
        );
    }

    /**
     * Retrieve load information
     * @return array
     */
    protected function _getOffloadInformation()
    {
        return array(
            'offload_code' => null,
            'offload_name' => null,
            'offload_address' => null,
            'offload_city' => null,
            'offload_zip' => null,
            'offload_state' => null,
            'offload_country' => null,
        );
    }

    /**
     * Add shipping information
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _getShippingAddressInformation(Mage_Sales_Model_Order $order)
    {
        /** @var Mage_Sales_Model_Order_Address $address */
        $address = $order->getShippingAddress();
        return array(
            'customer_code' => null,
            'ship_to_name' => $address->getCompany() ? $address->getCompany() : $address->getName(),
            'ship_to_address' => implode(', ', $address->getStreet()),
            'ship_to_city' => $address->getCity(),
            'ship_to_zip' => $address->getPostcode(),
            'ship_to_state' => $address->getRegionCode(),
            'ship_to_country' => $address->getCountryId(),
        );
    }

    protected function _getAdditionalInformation(Mage_Sales_Model_Order $order)
    {
        $isCashOnDelivery = $order->getPayment()->getMethod() == 'cashondelivery';
        $data = array(
            'colli' => 1,
            'weight' => str_replace(array(',', '.'), '', round($order->getWeight(),1)),
            'pieces' => round($order->getTotalQtyOrdered(), 0),
            'bancali' => null,
            'qty_complementare' => null,
            'um_qty_complementare' => null,
            'importo_contrassegno' => $isCashOnDelivery ? str_replace(array('.', ','), '', number_format($order->getGrandTotal(),2)) : null,
            'valuta_contrassegno' => $isCashOnDelivery ? $order->getStore()->getConfig('currency/options/default') : null,
            'modo_rimborso_contrassegno' => $isCashOnDelivery ? 'CE' : null,
            'consegna_richiesta' => null,
            'ora_richiesta' => null,
            'commenti' => $this->getShipmentComments($order),
            'giro_camion' => null,
            'categoria_temperatura' => 'FR',
            'categoria_tariffaria' => null,
            'natura_merce' => 'HD',
            'condizioni_vendita' => 'PP',
            'flag_grouped' => null,
            'flag_ritiro' => null,
            'flag_fermo' => null,
            'flag_annuncio' => null,
            'chiave_annuncio' => null,
            'numero_posizione' => null,
            'unitá_spedizione' => 0,
            'remettant' => null,
            'posizione_origine' => null
        );

        //Add facultative 30 fields with absolutely no meaning to exists
        for ($i = 0; $i<=30; $i++) {
            $data["F$i"] = null;
        }
        return $data;
    }

    protected function getShipmentComments(Mage_Sales_Model_Order $order)
    {
        /** @var Mage_Sales_Model_Order_Shipment $shipment */
        $comments = array();
        $comments[] = $order->getCustomerEmail();
        $comments[] = $order->getShippingAddress()->getTelephone();
        $comments[] = $order->getCustomerNote();
        return implode(';', $comments);
    }
}
