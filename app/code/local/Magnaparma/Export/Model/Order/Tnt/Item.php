<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Magnaparma_Export_Model_Order_Tnt_Item extends Varien_Object
{
    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function toOrderArray(Mage_Sales_Model_Order $order)
    {
        $data = array_merge($this->_getShipToInfo($order), $this->_getShipmentInfo($order), $this->_getCodInfo($order));
        return array_map(function($value) {
            return "\"$value\"";
        }, $data);
    }

    /**
     * @param Mage_Sales_Model_Order_Address $address
     * @return string
     */
    protected function _getSendToName(Mage_Sales_Model_Order_Address $address)
    {
        return $address->getCompany() ? $address->getCompany() : $address->getName();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _getCodInfo(Mage_Sales_Model_Order $order)
    {
        $isCashOnDelivery = $order->getPayment()->getMethod() == 'cashondelivery';
        return array(
            'importo_contrassegno' => $isCashOnDelivery ? number_format($order->getGrandTotal(),2) : '',
            'commissione_contrassegno' => (int) $isCashOnDelivery
        );
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _getShipToInfo(Mage_Sales_Model_Order $order)
    {
        $address = $order->getShippingAddress();
        return array(
            'codice_cliente' => Mage::getStoreConfig('magnaparma_export/order_tnt/client_id'),
            'tipo' => 'I',
            'company_name' => $this->_getSendToName($address),
            'company_address' => implode(',', (array) $address->getStreetFull()),
            'company_zip' => $address->getPostcode(),
            'company_city' => $address->getCity(),
            'company_state' => $address->getRegion(),
            'company_country' => $address->getCountryId(),
            'contact' => $address->getName(),
            'phone' => $address->getTelephone(),
            'dest_email' => $order->getCustomerEmail()
        );
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _getShipmentInfo(Mage_Sales_Model_Order $order)
    {
        return array(
            'weight' => number_format($order->getWeight(), 2),
            'colli' => '1',
            'service' => 'NC',
            'merce' => 'C',
            'fermo_deposito' => 0,
            'ref_mitt' => 'SPOT',
            'istruzioni' => $order->getCustomerNote(),
            'descrizione' => 'MATERIALE',
            'ref_colli' => '',
            'cod_ragg' => $order->getIncrementId(),
            'volume' => '',
            'lunghezza' => '',
            'altezza' => '',
            'larghezza' => ''
        );
    }
}
