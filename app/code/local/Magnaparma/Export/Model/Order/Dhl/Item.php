<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Magnaparma_Export_Model_Order_Dhl_Item extends Varien_Object
{
    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function toOrderArray(Mage_Sales_Model_Order $order)
    {
        $dateHelper = Mage::getModel('core/date');
        $shippingAddress = $this->getShippingAddress($order);

        return [
            'code' => $this->getShipmentCode(),
            'shipment_date' => date('Ymd', $dateHelper->timestamp(now())),
            'shipment_type' => 'G',
            'dhl_customer_code' => Mage::getStoreConfig('magnaparma_export/order_dhl/client_id'),
            'shipment_value' => Mage::helper('core')->formatCurrency($order->getGrandTotal(), false),
            'shipment_currency' => Mage::getStoreConfig('currency/options/default', $order->getStoreId()),
            'shipment_insurance_value' => '',
            'shipment_insurance_currency' => '',
            'content_description' => implode("; ", $order->getItemsCollection()->getColumnValues('name')),
            'sender' => Mage::getStoreConfig('general/store_information/company_name'),
            'sender_name' => Mage::getStoreConfig('general/store_information/company_name'),
            'sender_address_0' => Mage::getStoreConfig('general/store_information/company_address'),
            'sender_address_1' => '',
            'sender_address_2' => '',
            'sender_zip' => Mage::getStoreConfig('general/store_information/company_zip'),
            'sender_city' => Mage::getStoreConfig('general/store_information/company_city'),
            'sender_country' => Mage::getStoreConfig('general/store_information/company_country'),
            'sender_country_name' => '',
            'sender_email' => Mage::getStoreConfig('contacts/email/recipient_email'),
            'sender_phone' => Mage::getStoreConfig('general/store_information/phone'),
            'receiver' => $shippingAddress->getCompany() ? $shippingAddress->getCompany() : $shippingAddress->getName(),
            'receiver_name' => $shippingAddress->getName(),
            'receiver_address_1' => $shippingAddress->getStreet1(),
            'receiver_address_2' => $shippingAddress->getStreet2(),
            'receiver_address_3' => $shippingAddress->getStreet3(),
            'receiver_zip' => $shippingAddress->getPostcode(),
            'receiver_city' => $shippingAddress->getCity(),
            'receiver_country' => $shippingAddress->getCountryId(),
            'receiver_country_name' => '',
            'receiver_email' => $shippingAddress->getEmail(),
            'receiver_phone' => $shippingAddress->getTelephone(),
            'shipment_weight' => $order->getWeight(),
            'shipment_pieces' => 1,
            'unrequired_1' => '',
            'unrequired_2' => '',
            'unrequired_3' => '',
            'unrequired_4' => '',
            'unrequired_5' => '',
            'unrequired_6' => '',
            'unrequired_7' => '',
            'unrequired_8' => '',
            'unrequired_9' => '',
            'unrequired_10' => '',
            'unrequired_11' => '',
        ];
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Order_Shipment
     */
    private function getShipment(Mage_Sales_Model_Order $order)
    {
        return $order->getShipmentsCollection()->getFirstItem();
    }

    /**
     * DHL code
     * Possible values are
     *  DOM	    Spedizione Espressa in Italia
     *  ECX     Spedizione Espressa in Europa CEE
     *  WPX	    Spedizione Espressa di Merci Extra CEE
     *  DOX     Spedizione Espressa di Documenti Extra CEE
     *  ESU     Spedizione Camionistica in Europa CEE
     *  ESI     Spedizione Camionistica in Extra CEE
     *
     * @return string
     */
    private function getShipmentCode()
    {
        return 'DOM';
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Order_Address
     */
    private function getShippingAddress(Mage_Sales_Model_Order $order)
    {
        return $order->getShippingAddress();
    }
}
