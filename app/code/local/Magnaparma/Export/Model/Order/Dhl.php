<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 *
 * @method array getOrderIds()
 * @method setOrderIds(array $ids)
 * @method setMessages(array $messages)
 */

class Magnaparma_Export_Model_Order_Dhl extends Varien_Object implements Magnaparma_Export_Model_Export_Interface
{
    const ROW_PER_EXPORT = 1;
    protected $_fetchedOrderIds = array();

    /**
     * Export collected orders via crontab
     */
    public function run()
    {
        $filename = $this->_prepareFile();
        $this->setFilename($filename);
        $this->setFilepath($this->_getFilePath() . $filename);
        Mage::getResourceModel('magnaparma_export/order')->flag($this->_fetchedOrderIds, 'dhl_exported', 1);
    }

    /**
     * @param int $page
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    public function getCollection($page)
    {
        $collection = Mage::getResourceModel('sales/order_collection');

        if ($this->getOrderIds()) {
            $collection->addFieldToFilter('entity_id', array('in' => $this->getOrderIds()));
        } else {
            $collection->addFieldToFilter('tnt_exported', array('null' => true));
            $collection->addFieldToFilter('status', array('in' => $this->_getAllowedStatuses()));
        }

        $collection->getSelect()->limitPage($page, self::ROW_PER_EXPORT);
        return $collection;
    }

    /**
     * @return mixed
     */
    protected function _getAllowedStatuses()
    {
        return Mage::getStoreConfig('magnaparma_export/order_dhl/order_status');
    }

    /**
     * @return string
     */
    protected function _getFilePath()
    {
        return Mage::getConfig()->getVarDir('export') . '/';
    }

    /**
     * @return string
     */
    protected function _getFilename()
    {
        $timestamp = Mage::getModel('core/date')->date('ymdHis');
        return "magnaparma_dhl_$timestamp.csv";
    }

    /**
     * Returns path to generated file
     *
     * @return string
     */
    protected function _prepareFile()
    {
        $io = new Varien_Io_File();
        $filename = $this->_getFilename();
        $currentPage = 1;
        $itemClass = 'magnaparma_export/order_dhl_item';
        $_globalData = array();
        while ($this->getCollection($currentPage)->count() > 0) {
            foreach ($this->getCollection($currentPage) as $order) {
                try {
                    $data = Mage::getModel($itemClass)->toOrderArray($order);
                    $_globalData[] = implode(',', $data);
                    $this->_fetchedOrderIds[] = $order->getId();
                } catch (Exception $e) {
                    if (php_sapi_name() == 'cli') {
                        fwrite(STDOUT, $e->getMessage() . PHP_EOL);
                    }
                    $this->addMessage($e->getMessage());
                }
            }
            $currentPage++;
        }
        $io->write($this->_getFilePath() . $filename, implode(PHP_EOL, $_globalData), 'a');
        return $filename;
    }

    /**
     * @param $message
     * @return $this
     */
    public function addMessage($message)
    {
        if (!is_array($this->getMessages())) {
            $this->setMessages(array());
        }
        $messages = $this->getMessages();
        $messages[]= $message;
        $this->setMessages($messages);
        return $this;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return (array) $this->getData('messages');
    }
}

