<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class Magnaparma_Export_Model_System_Config_Order_Status
{
    protected $options;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        if (!isset($this->options)) {
            $this->options = array_map(function(Mage_Sales_Model_Order_Status $status) {
                return array(
                    'value' => $status->getId(),
                    'label' => $status->getStoreLabel(Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID)->getId())
                );
            }, Mage::getResourceModel('sales/order_status_collection')->getItems());
        }
        return $this->options;
    }
}
