<?php

/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */
class Magnaparma_Export_Model_Observer
{
    /**
     * Listens to
     * - adminhtml_block_html_before
     * @param Varien_Event_Observer $observer
     */
    public function addMassaction(Varien_Event_Observer $observer)
    {
        if ($observer->getBlock() instanceof Mage_Adminhtml_Block_Sales_Order_Grid) {
            foreach (Mage::getStoreConfig('magnaparma_export') as $key => $data) {
                $observer->getBlock()->getMassactionBlock()->addItem($key, array(
                    'label'=> Mage::helper('sales')->__(Mage::getStoreConfig("magnaparma_export/$key/label")),
                    'url'  => $observer->getBlock()->getUrl(Mage::getStoreConfig("magnaparma_export/$key/url"))
                ));
            }
        }
    }
}
