<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

interface Magnaparma_Export_Model_Export_Interface
{
    public function run();
}
