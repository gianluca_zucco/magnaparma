<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @var $this Mage_Catalog_Model_Resource_Setup
 */

$this->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'available_on_request', [
    'label' => 'Disponibile su richiesta',
    'group' => 'General',
    'input' => 'select',
    'type'  => 'int',
    'source' => 'eav/entity_attribute_source_boolean',
    'configurable' => false,
    'required' => false,
    'user_defined' => true,
    'is_searchable' => false,
    'is_filterable' => false,
    'is_comparable' => false,
    'is_visible_on_front' => false,
    'is_used_for_price_rules' => true,
    'is_configurable' => false,
    'used_in_product_listing' => true,
]);
