<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

class Magnaparma_Catalog_Model_AvailabilityRequest_Mailer
{
    /**
     * @param array $variables
     * @throws Exception
     */
    public function send(array $variables)
    {
        $emailTemplate = Mage::getModel('core/email_template')->loadDefault('request_availability');
        $emailTemplate->send(
            Mage::getStoreConfig('trans_email/ident_general/email'),
            Mage::getStoreConfig('trans_email/ident_general/name'),
            $variables
        );
    }
}
