<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

use Varien_Event_Observer as Event;

class Magnaparma_Catalog_Model_Observer
{

    public function redirectFromSearch(Event $observer)
    {
        $request = Mage::app()->getRequest();
        $keyword = $request->getParam('q', '');
        /** @var Mage_Core_Controller_Front_Action $action */
        $action = $observer->getControllerAction();
        $category = Mage::getStoreConfig('magnaparma_catalog/forced_search/colli_di_parma');
        if (strripos($keyword, 'colli') !== false && strripos($keyword, 'parma') && $request->getParam('cat', null) != $category) {
            $routeParams = array_merge(
                $request->getParams(),
                array('cat' => $category)
            );
            $action->getResponse()->setRedirect(Mage::getSingleton('core/url')->getUrl('*/*/*/', array(
                '_query' => $routeParams
            )));
            $action->setFlag('', $action::FLAG_NO_DISPATCH, true);
        }
    }

    public function isSalableAfter(Event $event)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $event->getData('product');
        /** @var Varien_Object $transport */
        $transport = $event->getData('salable');
        $isSalable = $transport->getData('is_salable') && !$product->getData('available_on_request');
        $transport->setData('is_salable', $isSalable);
    }

    public function addLayoutHandles(Event $event)
    {
        /** @var Mage_Core_Model_Layout_Update $update */
        $update = $event->getData('layout')->getUpdate();
        if (in_array('catalog_product_view', $update->getHandles())) {
            /** @var  $product */
            $product = Mage::registry('current_product');
            if ($product->getData('available_on_request')) {
                $update->addHandle('catalog_product_view_available_on_request');
            }
        }
    }
}
