<?php

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . '/CartController.php';

class Magnaparma_Catalog_CartController extends Mage_Checkout_CartController
{
    public function addAction()
    {
        if (!$this->getRequest()->isAjax()) {
            parent::addAction();
        } else {
            $this->ajaxAddToCart();
        }
    }

    private function ajaxAddToCart()
    {
        $this->getResponse()->setHeader('Content-type', 'application/json', true);
        $response = new Varien_Object([
            'message' => ''
        ]);
        try {
            $cart   = $this->_getCart();
            $params = $this->getRequest()->getParams();
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                throw new \Exception($this->__('The product does not exists'));
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            Mage::dispatchEvent('checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );
            $total = array_reduce($cart->getQuote()->getAllVisibleItems(), function($total, Mage_Sales_Model_Quote_Item $item) {
                return $total + $item->getRowTotalInclTax();
            }, 0);
            $response->addData([
                'product' => $product->toArray(),
                'message' => $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName())),
                'cart_total' => Mage::helper('core')->formatPrice($total, false)
            ]);
        } catch (Exception $e) {
            $response->setData('message', $e->getMessage());
            $this->getResponse()->setHttpResponseCode(400);
        }
        $this->getResponse()->setBody($response->toJson());
    }

    public function reloadMinicartAction()
    {
        $this->loadLayout(false);
        $this->renderLayout();
    }
}
