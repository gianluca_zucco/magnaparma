<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

class Magnaparma_Catalog_AvailabilityController extends Mage_Core_Controller_Front_Action
{
    public function requestAction()
    {
        try {
            $this->validateAvailabilityRequest();
            /** @var Magnaparma_Catalog_Model_AvailabilityRequest_Mailer $mailer */
            $mailer = Mage::getModel('magnaparma_catalog/availabilityRequest_mailer');
            $mailer->send($this->getRequest()->getParams());
            $this->getSession()->addSuccess($this->__('Message has been sent successfully'));
        } catch (Exception $e) {
            $this->getSession()->addError($this->__($e->getMessage()));
        }
        $this->_redirectReferer();
    }

    /**
     * @throws Exception
     */
    private function validateAvailabilityRequest()
    {
        if (!$this->getRequest()->isPost() || !$this->_validateFormKey() || !$this->validateCaptcha()) {
            throw new Exception("Wrong request");
        }
    }

    /**
     * @throws Exception
     */
    private function validateCaptcha()
    {
        $client = new Zend_Http_Client('https://www.google.com/recaptcha/api/siteverify');
        $client
            ->setMethod(Zend_Http_Client::POST)
            ->setParameterPost([
                'secret' => Mage::getStoreConfig('plugincompany_contactforms/form/recaptcha_private_key'),
                'response' => $this->getRequest()->getParam('g-recaptcha-response'),
                'remoteip' => Mage::helper('core/http')->getRemoteAddr(),
            ]);
        $response = Mage::helper('core')->jsonDecode($client->request()->getBody());
        return !!$response['success'];
    }

    private function getSession()
    {
        return Mage::getSingleton('core/session');
    }
}
