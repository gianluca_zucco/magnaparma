<?php

class Webgriffe_QuiPago_PaymentController extends Mage_Core_Controller_Front_Action
{

    /**
     * Default Helper
     */
    protected $_helper;

    /**
     * Costruttore
     * 
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs 
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        parent::__construct($request, $response, $invokeArgs);
        $this->_helper = Mage::helper('wgquipago');
    }

    /**
     * Funzione di logging
     * 
     * @param string $msg 
     */
    protected function _log($msg)
    {
        $this->_helper->log($msg);
    }

    /*
     * Action per la funzionalità di Ripeti Pagamento
     */
    public function repayAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        if ($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId()) {
                $this->_helper->getCheckoutSession()->setLastRealOrderId($order->getIncrementId());

                $paymentMethod = $order->getPayment()->getMethodInstance();
                $paymentMethod
                        ->getInfoInstance()
                        ->setAdditionalInformation('isRepay', true)
                        ->save();

                return $this->_redirectUrl($paymentMethod->getOrderPlaceRedirectUrl());
            }
        }
        return false;
    }

    /*
     * Action per la funzionalità di Pagamento
     */
    public function requestAction()
    {
        $checkoutSession = $this->_helper->getCheckoutSession();

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($checkoutSession->getLastRealOrderId());
        if (!$order->getId()) {
            Mage::throwException('No order found with Increment Id ' . $checkoutSession->getLastRealOrderId());
        }

        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $isRepay = $method->getInfoInstance()->getAdditionalInformation('isRepay');

        try {
            if ($quote = $checkoutSession->getQuote()) {
                $quote->setIsActive(false)->save();
            }

            $paymentRequestMsg = Mage::getModel('wgquipago/paymentMessage_request');
            $paymentRequestMsg->initialize($order, $isRepay);

            #$checkoutSession->clear();
            $checkoutSession->setQuoteId(null);

            $htmlForm = $paymentRequestMsg->getHtmlForm();
            Mage::log($htmlForm, null, 'Webgriffe_QuiPago-' . date('Ymd-His-u') . '.html');
            $this->getResponse()->setBody($htmlForm);
        } catch (Exception $e) {
            $this->_log($e->getMessage());
            if ($isRepay) {
                return $this->_redirect('sales/order/view/order_id/' . $order->getId() . '/');
            } else {
                return $this->_redirect('checkout/cart');
            }
        }
    }

    /**
     * Gestisce il ritorno al Checkout dopo un tentativo di pagamento sul 
     * portale Key Client.
     */
    public function returnCheckoutAction()
    {
        $this->_log('Called returnCheckoutAction()');
        /** @var Webgriffe_QuiPago_Model_PaymentMessage_Response $paymentResponseMsg */
        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');
        if ($paymentResponseMsg->handleRequest($this->getRequest())) {
            return $this->_redirect('checkout/onepage/success');
        }
        
        $this->_helper->getCheckoutSession()->setErrorMessage($this->__('The following error occurred during Payment process: %s', $paymentResponseMsg->getMessaggio()));
        return $this->_redirect('checkout/onepage/failure');
    }

    /**
     * Gestisce il ritorno al Checkout dopo la cancellazione del pagamento sul
     * portale Key Client.
     */
    public function cancelCheckoutAction()
    {
        $this->_log('Called cancelCheckoutAction()');
        $this->_helper->getCheckoutSession()->setErrorMessage($this->__('Payment canceled by the User'));
        return $this->_redirect('checkout/onepage/failure');
    }

    /**
     * Gestisce il ritorno all'Ordine dopo un tentativo di pagamento sul 
     * portale Key Client.
     */
    public function returnRepayAction()
    {
        $this->_log('Called returnRepayAction()');
        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');
        if ($paymentResponseMsg->handleRequest($this->getRequest())) {
            $this->_helper->getCoreSession()->addSuccess($this->__('Payment of order %s was successful.', $paymentResponseMsg->getOrderIncrementId()));
        } else {
            $this->_helper->getCoreSession()->addError($this->__('The following error occurred during Payment process: %s', $paymentResponseMsg->getMessaggio()));
            
        }
        $this->_redirect('sales/order/view/order_id/' . $paymentResponseMsg->getOrderId());
    }

    /**
     * Gestisce il ritorno all'Ordine dopo la cancellazione del pagamento sul 
     * portale Key Client.
     */
    public function cancelRepayAction()
    {
        $this->_log('Called cancelRepayAction()');
        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');
        $paymentResponseMsg->handleRequest($this->getRequest());
        $this->_helper->getCoreSession()->addError($this->__('Payment canceled by the User'));
        $this->_redirect('sales/order/view/order_id/' . $paymentResponseMsg->getOrderId());
    }

    /**
     * Gestisce la notifica Server To Server da Key Client a Magento 
     * relativamente ad un pagamento effettuato.
     */
    public function serverToServerAction()
    {
        $this->_log('Called serverToServerAction()');
        /** @var Webgriffe_QuiPago_Model_PaymentMessage_Response $paymentResponseMsg */
        $paymentResponseMsg = Mage::getModel('wgquipago/paymentMessage_response');
        $paymentResponseMsg->handleRequest($this->getRequest(), true);
        $this->_log(print_r($_GET, true));
    }

}
