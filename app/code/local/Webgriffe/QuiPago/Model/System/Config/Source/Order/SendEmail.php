<?php

/**
 * Order Statuses source model for KeyClient
 */
class Webgriffe_QuiPago_Model_System_Config_Source_Order_SendEmail {

    const EMAIL_SEND_ONORDER    = 'onorderplacement';
    const EMAIL_SEND_ONPAYMENT  = 'onsuccessfulpayment';
    
    // set null to enable all possible
    protected $_options = array(
      self::EMAIL_SEND_ONORDER      => 'when the Order is placed',
      self::EMAIL_SEND_ONPAYMENT    => 'when the Payment is successful',
    );

    public function toOptionArray() {
        $options = array();
        $options[] = array(
            'value' => '',
            'label' => Mage::helper('adminhtml')->__('-- Please Select --')
        );
        foreach ($this->_options as $value => $label) {
            $options[] = array(
                'value' => $value,
                'label' => Mage::helper('wgquipago')->__($label)
            );
        }
        return $options;
    }

}
