<?php
/**
 * Encryption algorithm source model for KeyClient
 */
class Webgriffe_QuiPago_Model_System_Config_Source_Encryption
{
    public function toOptionArray()
    {
        $options = array(
			array(
               'value' => 'sha1',
               'label' => Mage::helper('adminhtml')->__('SHA-1')
			),		
			array(
               'value' => 'md5',
               'label' => Mage::helper('adminhtml')->__('MD5')
            ),
		);
        return $options;
    }
}