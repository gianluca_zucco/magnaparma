<?php

/**
 * @method Webgriffe_QuiPago_Model_PaymentMessage setDataTrans(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setOrarioTrans(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setEsito(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setCodAut(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setBrand(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setNome(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setCognome(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setEmail(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setNazionalita(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setPan(string $value)
 * @method Webgriffe_QuiPago_Model_PaymentMessage setScadenzaPan(string $value)
 */
class Webgriffe_QuiPago_Model_PaymentMessage_Response extends Webgriffe_QuiPago_Model_PaymentMessage_Abstract
{

    const RESULT_OK = 'OK';
    const RESULT_KO = 'KO';

    public function getParams()
    {
        if (is_null($this->_params) || $this->hasDataChanges()) {
            $this->_params = array();
            $this->_params['session_id'] = $this->getSessionId();
            $this->_params['codAut'] = $this->getCodAut();
            $this->_params['alias'] = $this->getMerchantAlias();
            $this->_params['orario'] = $this->getOrarioTrans();
            $this->_params['data'] = $this->getDataTrans();
            $this->_params['mac'] = $this->getMac();
            $this->_params['importo'] = $this->getImportoFormatted();
            $this->_params['$BRAND'] = $this->getBrand();
            $this->_params['cognome'] = $this->getCognome();
            $this->_params['nazionalita'] = $this->getNazionalita();
            $this->_params['pan'] = $this->getPan();
            $this->_params['divisa'] = $this->getDivisa();
            $this->_params['email'] = $this->getCustomerEmail();
            $this->_params['scadenza_pan'] = $this->getScadenzaPan();
            $this->_params['esito'] = $this->getEsito();
            $this->_params['codTrans'] = $this->getCodTrans();
            $this->_params['nome'] = $this->getNome();
            $this->_params['messaggio'] = $this->getMessaggio();

            Mage::helper('wgquipago')->log('The following PaymentMessage_Response parameters were initialized:');
            Mage::helper('wgquipago')->log(print_r($this->_params, true));
        }
        return $this->_params;
    }

    public function toHtmlString()
    {
        $html = '';
        foreach ($this->getParams() as $key => $value) {
            $html .= $key . ': <strong>' . $value . '</strong><br/>';
        }
        return $html;
    }

    public function toTransactionArray()
    {
        $array = array();
        $helper = Mage::helper('wgquipago');
        foreach ($this->getParams() as $key => $value) {
            $array[$helper->__($key)] = $value;
        }
        return $array;
    }

    public function getStringToEncode()
    {
        #return 'codTrans=' . $this->getCodTransFormatted()
        return 'codTrans=' . $this->getCodTrans()
                . 'esito=' . $this->getEsito()
                . 'importo=' . $this->getImportoFormatted()
                . 'divisa=' . $this->getDivisa()
                . 'data=' . $this->getDataTrans()
                . 'orario=' . $this->getOrarioTrans()
                . 'codAut=' . $this->getCodAut()
                . $this->getMacKey();
    }

    public function initialize($request)
    {
        Mage::helper('wgquipago')->log('PaymentMessage_Response initialized with the following parameters:');
        Mage::helper('wgquipago')->log(print_r($request->getParams(), true));

        // First of all, get the Order which generated the Response; this way
        // we can recover proper configuration.
        #$this->setCodTrans($this->_decodeCodTrans($request->getParam('codTrans')));
        $this->setCodTrans($request->getParam('codTrans'));
        $incrementId = $this->getDecodedCodTrans();

        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
        if ($order->getId()) {
            $storeId = $order->getStoreId();
        } else {
            throw new Exception('Unrecognized Increment Id: ' . $incrementId);
        }

        //
        // Set configuration based values
        //
        $configData = Mage::getStoreConfig('payment/' . Webgriffe_QuiPago_Model_PaymentMethod::PAYMENT_METHOD_CODE, $storeId);
        $this->setIsTest($configData['test_mode']);
        $this->setEncryptionMethod($configData['mac_encryption']);
        $this->setMacKey($configData['mac_key']);

        //
        // Set other Request based values
        //
        $this->setSessionId($request->getParam('session_id'));
        $this->setCodAut($request->getParam('codAut'), 'NO-COD-AUT');
        $this->setMerchantAlias($request->getParam('alias'));
        $this->setOrarioTrans($request->getParam('orario'));
        $this->setDataTrans($request->getParam('data'));
        $this->setMac($request->getParam('mac'));
        $this->setImporto((float) $request->getParam('importo', 0) / 100);
        $this->setBrand($request->getParam('$BRAND'));
        $this->setCognome($request->getParam('cognome'));
        $this->setNazionalita($request->getParam('nazionalita'));
        $this->setPan($request->getParam('pan'));
        $this->setDivisa($request->getParam('divisa'));
        $this->setCustomerEmail($request->getParam('email'));
        $this->setScadenzaPan($request->getParam('scadenza_pan'));
        $this->setEsito($request->getParam('esito'));
        $this->setNome($request->getParam('nome'));
        $this->setMessaggio($request->getParam('messaggio'));

        return true;
    }

    protected function _getTransactionId()
    {
        return $this->getCodAut() . '-' . date('YmdHis');
    }

    public function getTransaction(Mage_Sales_Model_Order $order)
    {
        $transaction = null;
        $helper = Mage::helper('wgquipago');
        $payment = $order->getPayment();
        if ($payment->getId()) {
            $transaction = Mage::getModel('sales/order_payment_transaction')
                    ->setOrderId($order->getId())
                    ->setPaymentId($payment->getId())
                    ->setTxnId($this->_getTransactionId())
                    ->setOrderPaymentObject($payment)
                    ->setAdditionalInformation(
                            Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $this->toTransactionArray())
                    ->setCreatedAt($helper->decodePayDate($this->getDataTrans()) . ' ' . $helper->decodePayTime($this->getOrarioTrans(), ':'));
        }
        return $transaction;
    }

    public function isSuccessful()
    {
        return $this->getEsito() == self::RESULT_OK;
    }

    public function isNotValidAfterInitialization()
    {
        Mage::helper('wgquipago')->log('Incoming MAC: ' . $this->getMac());
        Mage::helper('wgquipago')->log('Calculated MAC: ' . $this->getCalculatedMac());
        return ($this->getMac() != $this->getCalculatedMac())
                || (!$this->getOrder());
    }
    
    public function isNotAlreadyHandled($isServerToServer)
    {
        #if (!$isServerToServer) return true;

        /** @var Webgriffe_QuiPago_Model_PaymentMethod $paymentMethod */
        $paymentMethod = $this->getOrder()->getPayment()->getMethodInstance();
        $newOrderState = $paymentMethod->getStateForNewOrder();
        
        return ($this->getOrder()->getState() == $newOrderState);
    }

    public function getOrderIncrementId()
    {
        #return $this->getCodTrans();
        return $this->getDecodedCodTrans();
    }

    public function getOrder()
    {
        if ($this->_order == null) {
            $this->_order = Mage::getModel('sales/order')
                    ->loadByIncrementId($this->getDecodedCodTrans());
            if ($this->_order->getId()) {
                // Arricchisce l'entità con tutti i dati
                $this->_order = $this->_order->load($this->_order->getId());
            }
        }
        return $this->_order;
    }

    public function getOrderId()
    {
        if ($this->getOrder())
            return $this->getOrder()->getId();
        return '';
    }

    public function updateOrderState()
    {
        $paymentMethod = $this->getOrder()->getPayment()->getMethodInstance();

        if ($this->isSuccessful()) {
            $state = $paymentMethod->getStateForSuccessfulPayment();
            
            if ($paymentMethod->isSendEmailOnPayment()) {
                $this->getOrder()->sendNewOrderEmail();
            }
        } else {
            $state = $paymentMethod->getStateForUnsuccessfulPayment();
        }
        try {
            if (!strcmp(Mage_Sales_Model_Order::STATE_CANCELED, $state)) {
                $this->getOrder()->cancel()->save();
                Mage::helper('wgquipago')->log('Order ' . $this->getOrder()->getIncrementId() . ' canceled');
            } else {
                    $this
                        ->getOrder()
                        ->setState($state, $state, $this->toHtmlString())
                        ->save();
                    Mage::helper('wgquipago')->log('State set to ' . $state . ' for Order ' . $this->getOrder()->getIncrementId());
            }
        } catch (Exception $orderSaveException) {
            Mage::helper('wgquipago')->log($orderSaveException->getMessage());
        }
    }

    public function createOrderTransaction()
    {
        Mage::helper('wgquipago')->log('Called createOrderTransaction()');
        
        if (Mage::helper('wgquipago')->isOldVersion()) {
            Mage::helper('wgquipago')->log('Transaction can\'t be created on current Magento version: '. Mage::getVersion());
            return;
        }
        
        $paymentMethod = $this->getOrder()->getPayment()->getMethodInstance();
        $transaction = $this->getTransaction($this->getOrder());
        try {
            $transaction = $transaction
                    ->setIsClosed($this->isSuccessful())
                    ->setTxnType(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER)
                    ->save();
            Mage::helper('wgquipago')->log('Transaction was created with id: ' . $transaction->getId());
        } catch (Exception $transactionSaveException) {
            Mage::helper('wgquipago')->log($transactionSaveException->getMessage());
        }
    }
    
    public function handleRequest($request, $isServerToServer = false) {
        $isSuccessful = false;
        
        $this->initialize($request);
        if ($this->isNotValidAfterInitialization()) {
            Mage::helper('wgquipago')->log('Invalid Payment Response.');
        } elseif ($this->isNotAlreadyHandled($isServerToServer)) {
            $this->updateOrderState();
            $this->createOrderTransaction();

            if ($this->isSuccessful()) {
                Mage::helper('wgquipago')->log('Successful Payment for Order ' . $this->getOrderIncrementId());
                $isSuccessful = true;
            } else {
                Mage::helper('wgquipago')->log('Unsuccessful Payment for Order ' . $this->getOrderIncrementId());
            }
        } else {
            Mage::helper('wgquipago')->log('Payment for Order ' . $this->getOrderIncrementId() . ' already handled.');
            $paymentMethod = $this->getOrder()->getPayment()->getMethodInstance();
            $successfulState = $paymentMethod->getStateForSuccessfulPayment();
            $isSuccessful = !strcmp($successfulState, $this->getOrder()->getState());
        }
        return $isSuccessful;
    }

}
