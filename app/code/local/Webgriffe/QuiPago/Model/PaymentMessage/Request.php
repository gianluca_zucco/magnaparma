<?php

class Webgriffe_QuiPago_Model_PaymentMessage_Request extends Webgriffe_QuiPago_Model_PaymentMessage_Abstract
{

    public function getParams()
    {
        if (is_null($this->_params) || $this->hasDataChanges()) {
            $this->_params = array();
            $this->_params['alias'] = $this->getMerchantAlias();
            $this->_params['importo'] = $this->getImportoFormatted();
            $this->_params['divisa'] = $this->getDivisa();
            $this->_params['codTrans'] = $this->getCodTransFormatted();
            $this->_params['mail'] = $this->getCustomerEmail();
            $this->_params['url'] = $this->getBrowserReturnUrl();
            $this->_params['session_id'] = $this->getSessionId();
            $this->_params['url_back'] = $this->getBrowserCancelUrl();
            $this->_params['languageId'] = $this->getLanguageId();
            $this->_params['urlpost'] = $this->getServerReturnUrl();
            $this->_params['mac'] = $this->getMac();
            
            Mage::helper('wgquipago')->log('The following PaymentMessage_Request parameters were initialized:');
            Mage::helper('wgquipago')->log(print_r($this->_params, true));
        }
        return $this->_params;
    }

    public function getStringToEncode()
    {
        return 'codTrans=' . $this->getCodTransFormatted()
                . 'divisa=' . $this->getDivisa()
                . 'importo=' . $this->getImportoFormatted()
                . $this->getMacKey();
    }

    public function initialize(Mage_Sales_Model_Order $order, $isFromRepay = false)
    {
        //
        // Set configuration based values
        //
        $configData = Mage::getStoreConfig('payment/' . Webgriffe_QuiPago_Model_PaymentMethod::PAYMENT_METHOD_CODE, $order->getStoreId());
        Mage::helper('wgquipago')->log('The following Config Parameters were initialized:');
        Mage::helper('wgquipago')->log(print_r($configData, true));
        
        $this->setIsTest($configData['test_mode']);
        $this->setMerchantAlias($configData['merchant_alias']);
        $this->setEncryptionMethod($configData['mac_encryption']);
        $this->setFormAction($configData['payment_url']);
        $this->setMacKey($configData['mac_key']);

        //
        // Set Context based values
        //
        if ($isFromRepay) {
            $this->setBrowserReturnUrl(Mage::getUrl('wgquipago/payment/returnRepay'));
            $this->setBrowserCancelUrl(Mage::getUrl('wgquipago/payment/cancelRepay'));
        } else {
            $this->setBrowserReturnUrl(Mage::getUrl('wgquipago/payment/returnCheckout'));
            $this->setBrowserCancelUrl(Mage::getUrl('wgquipago/payment/cancelCheckout'));
        }
        $this->setServerReturnUrl(Mage::getUrl('wgquipago/payment/serverToServer'));
        $this->setSessionId(Mage::helper('wgquipago')->getCurrentSessionId());

        //
        // Set Order based values
        //
        $this->setImporto($this->_getOrderEuroAmount($order));
        $this->setCodTrans($order->getIncrementId());
        $this->setCustomerEmail($order->getCustomerEmail());
        $this->setLanguageId($this->_getOrderLanguage($order));
        $this->setMac($this->getCalculatedMac());

        return true;
    }

    public function getHtmlForm($formNameValue = 'paymentform', $formMethodValue = 'POST')
    {
        $helper = Mage::helper('wgquipago');

        $doc = new DOMDocument('1.0', 'UTF-8');

        $html = $doc->createElement('html');
        $doc->appendChild($html);

        $head = $doc->createElement('head');
        $html->appendChild($head);

        $title = $doc->createElement('title', $helper->__('Key Client Payment Gateway'));
        $head->appendChild($title);

        $script = $doc->createElement('script', 'function redirect() { document.' . $formNameValue . '.submit(); }');
        $head->appendChild($script);

        $scriptType = $doc->createAttribute('type');
        $scriptType->value = 'text/javascript';
        $script->appendChild($scriptType);

        $body = $doc->createElement('body');
        $html->appendChild($body);

        $bodyStyle = $doc->createAttribute('style');
        $bodyStyle->value = 'text-align:center; font-family:Arial; font-size:14px; font-weight:bold;';
        $body->appendChild($bodyStyle);

        $bodyOnload = $doc->createAttribute('onload');
        $bodyOnload->value = 'javascript:setTimeout(\'redirect()\', 5000);';
        $body->appendChild($bodyOnload);

        $form = $doc->createElement('form');
        $body->appendChild($form);

        $formId = $doc->createAttribute('id');
        $formId->value = $formNameValue;
        $form->appendChild($formId);

        $formName = $doc->createAttribute('name');
        $formName->value = $formNameValue;
        $form->appendChild($formName);

        $formMethod = $doc->createAttribute('method');
        $formMethod->value = $formMethodValue;
        $form->appendChild($formMethod);

        $formAction = $doc->createAttribute('action');
        $formAction->value = $this->getformAction();
        $form->appendChild($formAction);

        foreach ($this->getParams() as $paramName => $paramValue) {
            #'<input type="hidden" name="' . $name . '" value="' . $value . '" />';
            $input = $doc->createElement('input');
            $form->appendChild($input);

            $inputType = $doc->createAttribute('type');
            $inputType->value = 'hidden';
            $input->appendChild($inputType);

            $inputId = $doc->createAttribute('id');
            $inputId->value = $paramName;
            $input->appendChild($inputId);

            $inputName = $doc->createAttribute('name');
            $inputName->value = $paramName;
            $input->appendChild($inputName);

            $inputValue = $doc->createAttribute('value');
            $inputValue->value = $paramValue;
            $input->appendChild($inputValue);
        }

        $form->appendChild($doc->createElement('p', $helper->__('You will be redirected to the Key Client Payment Page in a few seconds')));

        $p = $doc->createElement('p', $helper->__('If you are not redirected within five seconds:'));
        $form->appendChild($p);

        $input = $doc->createElement('input');
        $p->appendChild($input);

        $inputType = $doc->createAttribute('type');
        $inputType->value = 'submit';
        $input->appendChild($inputType);

        $inputStyle = $doc->createAttribute('style');
        $inputStyle->value = 'background:none; text-decoration:underline; border:none; font-weight:bold; color:#2200CC; cursor:pointer;';
        $input->appendChild($inputStyle);

        $inputValue = $doc->createAttribute('value');
        $inputValue->value = $helper->__('Click here');
        $input->appendChild($inputValue);

        $doc->formatOutput = true;
        return $doc->saveHTML();
    }

    protected function _getOrderEuroAmount(Mage_Sales_Model_Order $order)
    {
        $amount = $order->getBaseGrandTotal();
        if (empty($amount)) {
            Mage::helper('wgquipago')->log('Warning: empty amount for order ' . $order->getIncrementId());
        }
        $storeCurrency = Mage::getSingleton('directory/currency')->load($order->getBaseCurrencyCode());
        $amount = $storeCurrency->convert($amount, 'EUR');
        return $amount;
    }

    protected function _getOrderLanguage(Mage_Sales_Model_Order $order)
    {
        $locale = Mage::helper('wgquipago')->getStoreLocaleCode($order->getStoreId());
        $lang = substr($locale, 0, 2);
        if (array_key_exists($lang, $this->_languages)) {
            return $this->_languages[$lang];
        }
        return self::LANG_DEFAULT;
    }

}
