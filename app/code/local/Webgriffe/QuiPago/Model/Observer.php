<?php

require_once Mage::getModuleDir('model', 'Webgriffe_QuiPago') . DS . 'lib' . DS . 'phpQuery' . DS . 'phpQuery-onefile.php';

class Webgriffe_QuiPago_Model_Observer {

    public function addRepayBlock($event) {
        if (Mage::helper('wgquipago')->isOldVersion()
                || !Mage::getStoreConfig('payment/wgquipago/active')
                || !Mage::getStoreConfig('payment/wgquipago/enable_repay')) return;
        
        $helper = Mage::helper('wgquipago');
        
        $block = $event->getBlock();

        if ($block instanceof Mage_Sales_Block_Order_Recent
                || $block instanceof Mage_Sales_Block_Order_History) {
            $orders = $block->getOrders();

            $html = $event->getTransport()->getHtml();

            $doc = phpQuery::newDocument($html);
            phpQuery::selectDocument($doc);

            foreach (pq('#my-orders-table tbody tr') as $tr) {
                if (false !== $orderId = $this->__extractOrderId(pq($tr)->html())) {
                    $order = $orders->getItemById($orderId);
                    if ($helper->isOrderRepayable($order)) {
                        pq($tr)
                                ->find('td:last-child span a:last-child')
                                ->after('<span class="separator">|</span><a class="'.Mage::getStoreConfig('payment/wgquipago/repay_css_class').'" href="' . $helper->getRepayUrl($order) . '">'.$helper->__('Repay').'</a>');
                    }
                }
            }
            $event->getTransport()->setHtml($doc->htmlOuter());
        } else if ($block instanceof Mage_Sales_Block_Order_Info_Buttons) {
            $order = $block->getOrder();
            $orderId = $order->getId();
            if ($helper->isOrderRepayable($order)) {
                $html = $event->getTransport()->getHtml();
                $doc = phpQuery::newDocument($html);
                pq('a:last-child')
                    ->after('<span class="separator">|</span><a class="'.Mage::getStoreConfig('payment/wgquipago/repay_css_class').'" href="' . $helper->getRepayUrl($order) . '">'.$helper->__('Repay').'</a>');
                $event->getTransport()->setHtml($doc->htmlOuter());
            }
        }
    }
    
    private function __extractOrderId($htmlstr) {
        $matches = null;
        $pattern = '/\/order_id\/(\d+)/';
        preg_match($pattern, $htmlstr, $matches);
        if (count($matches) > 1)
            return $matches[1];
        return false;
    }

}