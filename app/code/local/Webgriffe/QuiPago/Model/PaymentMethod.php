<?php

/*
 * Reminder: the customers payment data is set on the info instance through the 
 * payment method model using assignData().
 * You can get the customer payment data from the info instance using 
 * $this->getInfoInstance()->getData() after assignData() was called.
 */

class Webgriffe_QuiPago_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract {

    const PAYMENT_METHOD_CODE = 'wgquipago';
    
    protected $_code = self::PAYMENT_METHOD_CODE;

    /*
     * KeyClient Qui Pago doesn't support authorize or authorize_capture
     * @var bool
     */
    protected $_canOrder = true;
    /*
     * Force the call to the initialize() method
     * @var bool
     */
    protected $_isInitializeNeeded = true;
    /*
     * Multiaddress checkout doesn’t support redirects
     * @var bool
     */
    protected $_canUseForMultishipping = false;
    /*
     * Disable payment method in Admin because of redirection
     * @var bool
     */
    protected $_canUseInternal = false;

    /*
     * Reminder: You also have to call $order->sendNewOrderEmail() yourself.
     * Get the orderId after the customer returns via 
     * checkoutSession->getLastOrderId()    
     */
    public function getOrderPlaceRedirectUrl() {
        $url = Mage::getUrl('quipago/payment/request');
        Mage::helper('wgquipago')->log('Called getOrderPlaceRedirectUrl(); redirecting to ' . $url);
        return $url;
    }

    public function initialize($paymentAction, $stateObject) {
        Mage::helper('wgquipago')->log('Payment initialization with Payment Action: ' . $paymentAction);
        
        // Se passo da qui significa che arrivo dal Checkout, e non da un
        // Ripeti Pagamento.
        $this->getInfoInstance()->setAdditionalInformation('isRepay', false);
        
        //
        // Imposta lo stato del Nuovo Ordine
        //
        $stateObject->setState($this->getConfigData('order_new_state'));
        $stateObject->setStatus($this->getConfigData('order_new_state'));
        $stateObject->setIsNotified(false);        

        //
        // Controlla se deve essere mandata l'e-mail di conferma Ordine
        //
        if($this->isSendEmailOnOrder()) {
            Mage::helper('wgquipago')->log('Order Confirmation e-mail configured to be sent on Order placement.');
            $stateObject->setIsNotified(true);        
            $this->getInfoInstance()->getOrder()->sendNewOrderEmail();
        }
        
        return $this;
    }

    public function validate() {
        return parent::validate();
    }
    
    public function getStateForSuccessfulPayment() {
        return $this->getConfigData('order_success_state');
    }

    public function getStateForUnsuccessfulPayment() {
        return $this->getConfigData('order_fault_state');
    }
    
    public function isSendEmailOnOrder() {
        return $this->getConfigData('order_send_email') == Webgriffe_QuiPago_Model_System_Config_Source_Order_SendEmail::EMAIL_SEND_ONORDER;
    }

    public function isSendEmailOnPayment() {
        return $this->getConfigData('order_send_email') == Webgriffe_QuiPago_Model_System_Config_Source_Order_SendEmail::EMAIL_SEND_ONPAYMENT;
    }

    public function getStateForNewOrder()
    {
        return $this->getConfigData('order_new_state');
    }

}
