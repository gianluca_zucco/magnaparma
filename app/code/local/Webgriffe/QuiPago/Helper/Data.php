<?php

class Webgriffe_QuiPago_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Metodo utilizzato per l'encryption del MAC.
     */
    const ENCTYPE_MD5   = 'md5';
    const ENCTYPE_SHA1  = 'sha1';

    /**
     * LOG
     * 
     * @param string $msg
     * @param integer $level 
     */
    public function log($msg, $level = null)
    {
        Mage::log($msg, $level, 'Webgriffe_QuiPago.log');
    }
    
    public function calculateMac($stringToEncode, $encodingMethod) {
        $mac = '';
        switch ($encodingMethod) {
            case self::ENCTYPE_MD5:
                $mac = urlencode(base64_encode(md5($stringToEncode)));
                break;

            case self::ENCTYPE_SHA1:
                $mac = sha1($stringToEncode);
                break;

            default:
                throw new Exception('Unsupported Encryption Method');
        }
        return $mac;
    }

    /**
     * Restituisce il singleton della sessione del Checkout
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Restituisce il singleton della sessione del Core
     *
     * @return Mage_Core_Model_Session
     */
    public function getCoreSession()
    {
        return Mage::getSingleton('core/session');
    }

    /**
     * Torna il codice del Locale relativo allo store passato.
     * 
     * @param string|int $storeId
     * @return string 
     */
    public function getStoreLocaleCode($storeId)
    {
        return Mage::getStoreConfig('general/locale/code', $storeId);
    }

    /**
     * Torna l'ID crittato della sessione corrente 
     * @return string
     */
    public function getCurrentSessionId()
    {
        return Mage::getModel("core/session")->getEncryptedSessionId();
    }

    /**
     * Verifica se l'ordine passato è ripagabile in funzione del metodo di 
     * pagamento utilizzato e dello stato dell'ordine.
     * @param Mage_Sales_Model_Order $order
     * @return boolean
     */
    public function isOrderRepayable(Mage_Sales_Model_Order $order)
    {
        $paymentMethod = $order->getPayment()->getMethodInstance();
        return ($paymentMethod->getCode() == Webgriffe_QuiPago_Model_PaymentMethod::PAYMENT_METHOD_CODE
                && $order->getState() == Mage_Sales_Model_Order::STATE_PENDING_PAYMENT);
    }

    /**
     * Torna l'URL per avviare l'azione di Ripeti Pagamento
     *  
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    public function getRepayUrl(Mage_Sales_Model_Order $order)
    {
        return Mage::getUrl('quipago/payment/repay', array('order_id' => $order->getId()));
    }

    /**
     * Torna il valore della chiave 'Location' se presente negli header, null
     * altrimenti.
     * 
     * @param Zend_Controller_Response_Abstract $response
     * @return string
     */
    public function getLocationFromHttpResponse(Zend_Controller_Response_Abstract $response)
    {
        $location = null;
        $headers = $response->getHeaders();
        foreach ($headers as $header) {
            if ($header['name'] == 'Location') {
                $location = $header['value'];
                break;
            }
        }
        return $location;
    }

    /**
     * Su versioni più vecchie della 1.4.0.1 compresa, alcune funzionalità 
     * non sono utilizzabili:
     * - Ripeti Pagamento
     * - Salvataggio Transazioni 
     */
    public function isOldVersion()
    {
        return version_compare(Mage::getVersion(), '1.4.0.1', '<=');
    }
    
    public function decodePayDate($payDate, $delim = '-')
    {
        $yyyy = substr($payDate, 0, 4);
        $mm = substr($payDate, 4, 2);
        $dd = substr($payDate, 6, 2);
        return $yyyy . $delim . $mm . $delim . $dd;
    }

    public function decodePayTime($payTime, $delim = '.')
    {
        $hh = substr($payTime, 0, 2);
        $mm = substr($payTime, 2, 2);
        $ss = substr($payTime, 4, 2);
        return $hh . $delim . $mm . $delim . $ss;
    }    

}
