<?php

class Webgriffe_QuiPago_Helper_Test extends Mage_Core_Helper_Data
{

    /**
     * Torna un prodotto test; se non esiste lo crea.
     * Utilizzata per generare fixtures.
     * 
     * @param string $sku
     * @param string $name
     * @param int $storeId 
     */
    public function getTestProduct($sku, $name, $storeId = null)
    {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getDefaultStoreView()->getId();
        }

        #$product = new Mage_Catalog_Model_Product();
        #$product->loadByAttribute('sku', $sku);
        $product = Mage::getModel('catalog/product');
        $product->load($product->getIdBySku($sku));

        if (!$product->getId()) {

            // Build the product
            $product->setSku($sku);
            $product->setAttributeSetId($this->_getProductAttributeSetIdByName('Default'));
            $product->setTypeId('simple');
            $product->setName($name);
            $product->setCategoryIds(array());
            $product->setWebsiteIDs(array($this->_getWebsiteIdFromStoreId($storeId)));
            $product->setDescription('Product ' . $name . ' has been created programmatically only for testing purposes. You actually should\'t see this product in your catalog. If so, there must be something wrong in tests!');
            $product->setShortDescription('Product created for testing.');
            $product->setPrice(0.01);

            //Default Magento attribute
            $product->setWeight(0.0000);
            $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
            $product->setStatus(1);
            $product->setTaxClassId(0);
            $product->setStockData(array(
                'is_in_stock' => 1,
                'qty' => 99999,
            ));

            $product->setCreatedAt(strtotime('now'));

            try {
                $product->save();
            } catch (Exception $e) {
                $this->log($e);
            }
        }

        return $product;
    }

    public function getTestCustomer($email)
    {
        $customer = Mage::getModel('customer/customer');

        $password = 'magento123';
        #$email = 'mail@webgriffe.com';

        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($email);

        if (!$customer->getId()) {
            $customer->setEmail($email);
            $customer->setFirstname('Mario');
            $customer->setLastname('Rossi');
            $customer->setPassword($password);
        }

        try {
            $customer->save();
            $customer->setConfirmation(null);
            $customer->save();

            //Make a "login" of new customer
            #Mage::getSingleton('customer/session')->loginById($customer->getId());

            $_custom_address = array(
                'firstname' => 'Mario',
                'lastname' => 'Rossi',
                'street' => array(
                    '0' => 'Indirizzo - riga 1',
                    '1' => 'Indirizzo - riga 2',
                ),
                'city' => 'Casalgrande',
                'region_id' => '',
                'region' => '',
                'postcode' => '42013',
                'country_id' => 'IT',
                'telephone' => '0039123456789',
            );

            $customAddress = Mage::getModel('customer/address');
            $customAddress->setData($_custom_address)
                    ->setCustomerId($customer->getId())
                    ->setIsDefaultBilling('1')
                    ->setIsDefaultShipping('1')
                    ->setSaveInAddressBook('1');

            try {
                $customAddress->save();
            } catch (Exception $addressSaveException) {
                $this->log($addressSaveException->getMessage());
            }

            Mage::getSingleton('checkout/session')->getQuote()->setBillingAddress(Mage::getSingleton('sales/quote_address')->importCustomerAddress($customAddress));
        } catch (Exception $customerSaveException) {
            $this->log($customerSaveException->getMessage());
        }

        return $customer;
    }

    public function createCustomerTestOrder(
    Mage_Customer_Model_Customer $customer, Mage_Catalog_Model_Product $product)
    {
        if (!$customer->getId() && !$product->getId())
            return null;

        $quote = Mage::getModel('sales/quote')
                ->setStoreId(Mage::app()->getStore('default')->getId());

        $quote->assignCustomer($customer);

        $buyInfo = array(
            'qty' => 1,
                // custom option id => value id
                // or
                // configurable attribute id => value id
        );
        $quote->addProduct($product, new Varien_Object($buyInfo));

        $billingAddressData = $customer->getDefaultBillingAddress()->getData();
        /* $billingAddress = */$quote->getBillingAddress()->addData($billingAddressData);

        $shippingAddressData = $customer->getDefaultShippingAddress()->getData();
        $shippingAddress = $quote->getShippingAddress()->addData($shippingAddressData);

        $shippingAddress->setCollectShippingRates(true)->collectShippingRates()
                ->setShippingMethod('flatrate_flatrate')
                ->setPaymentMethod('wgquipago');

        $quote->getPayment()->importData(array('method' => 'wgquipago'));

        $quote->collectTotals()->save();

        $service = Mage::getModel('sales/service_quote', $quote);
        if (Mage::helper('wgquipago')->isOldVersion()) {
            $order = $service->submit();
        } else {
            $service->submitAll();
            $order = $service->getOrder();
        }

        return $order;
    }

    protected function _getWebsiteIdFromStoreId($storeId)
    {
        return Mage::getModel('core/store')->load($storeId)->getWebsiteId();
    }

    protected function _getProductAttributeSetIdByName($name)
    {
        $entityTypeId = Mage::getModel('eav/entity')->setType(Mage_Catalog_Model_Product::ENTITY)->getTypeId();
        return Mage::getModel('catalog/config')->getAttributeSetId($entityTypeId, $name);
    }

}
