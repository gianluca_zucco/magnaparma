<?php
/**
 *
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 *
 */

/**
 * Contactforms module install script
 *
 * @category    Plugincompany
 * @package     Plugincompany_Contactforms
 * @author      Milan Simek
 */
$this->startSetup();

$this->getConnection()
    ->dropForeignKey($this->getTable('plugincompany_contactforms/entry_comment'),$this->getFkName('plugincompany_contactforms/entry_comment', 'entry_id', 'plugincompany_contactforms/entry', 'entity_id'))
    ->addForeignKey($this->getFkName('plugincompany_contactforms/entry_comment', 'entry_id', 'plugincompany_contactforms/entry', 'entity_id'),  $this->getTable('plugincompany_contactforms/entry_comment'),'entry_id', $this->getTable('plugincompany_contactforms/entry'), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
;

$this->endSetup();
