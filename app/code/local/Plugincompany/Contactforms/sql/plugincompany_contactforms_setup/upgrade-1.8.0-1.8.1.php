<?php
/**
 *
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 *
 */

/**
 * Contactforms module install script
 *
 * @category    Plugincompany
 * @package     Plugincompany_Contactforms
 * @author      Milan Simek
 */
$this->startSetup();

//fix for 1.8.0 bug
$forms = Mage::getModel('plugincompany_contactforms/form')->getCollection();
foreach($forms as $form) {
    $form = $form->load($form->getId());
    if(!$form->getStoreId()){
        $form->setStoreId(array(0));
        $form->save();
    }
}

$this->endSetup();
