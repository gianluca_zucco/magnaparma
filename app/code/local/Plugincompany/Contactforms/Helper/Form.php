<?php
/**
 *
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 *
 */
 ?>
<?php 

/**
 * Form helper
 *
 * @category    Plugincompany
 * @package     Plugincompany_Contactforms
 * @author      Milan Simek
 */
class Plugincompany_Contactforms_Helper_Form
    extends Mage_Core_Helper_Abstract {
    /**
     * check if breadcrumbs can be used
     * @access public
     * @return bool
     * @author Milan Simek
     */
    public function getUseBreadcrumbs(){
        return Mage::getStoreConfigFlag('plugincompany_contactforms/form/breadcrumbs');
    }
}
