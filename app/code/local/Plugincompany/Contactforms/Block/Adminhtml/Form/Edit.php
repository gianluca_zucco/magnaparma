<?php
/**
 *
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 *
 */
 ?>
<?php

/**
 * Form admin edit form
 *
 * @category    Plugincompany
 * @package     Plugincompany_Contactforms
 * @author      Milan Simek
 */
class Plugincompany_Contactforms_Block_Adminhtml_Form_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container {
    /**
     * constructor
     * @access public
     * @return void
     * @author Milan Simek
     */
    public function __construct(){
        parent::__construct();
        $this->_blockGroup = 'plugincompany_contactforms';
        $this->_controller = 'adminhtml_form';
        $this->_updateButton('save', 'label', Mage::helper('plugincompany_contactforms')->__('Save Form'));
        $this->_updateButton('save', 'onclick', 'submitForm()');
        $this->_updateButton('delete', 'label', Mage::helper('plugincompany_contactforms')->__('Delete Form'));
        $this->_addButton('saveandcontinue', array(
            'label'        => Mage::helper('plugincompany_contactforms')->__('Save And Continue Edit'),
            'onclick'    => 'saveAndContinueEdit()',
            'class'        => 'save',
        ), -100);
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                prepareFormSave();
                editForm.submit($('edit_form').action+'back/edit/');
            }
            function submitForm(){
                prepareFormSave();
                 editForm.submit();
            }
        ";
    }
    /**
     * get the edit form header
     * @access public
     * @return string
     * @author Milan Simek
     */
    public function getHeaderText(){
        if( Mage::registry('current_form') && Mage::registry('current_form')->getId() ) {
            return Mage::helper('plugincompany_contactforms')->__("Edit Form '%s'", $this->escapeHtml(Mage::registry('current_form')->getTitle()));
        }
        else {
            return Mage::helper('plugincompany_contactforms')->__('Add Form');
        }
    }

}
