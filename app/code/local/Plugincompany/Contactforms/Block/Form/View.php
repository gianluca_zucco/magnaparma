<?php
/**
 *
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 *
 */
 ?>
<?php

/**
 * Form view block
 *
 * @category    Plugincompany
 * @package     Plugincompany_Contactforms
 * @author      Milan Simek
 */

class Plugincompany_Contactforms_Block_Form_View
    extends Mage_Core_Block_Template {
    /**
     * get the current form
     * @access public
     * @return mixed (Plugincompany_Contactforms_Model_Form|null)
     * @author Milan Simek
     */

    public function getCurrentForm(){
        return Mage::registry('current_form');
    }

    protected function _isSecure(){
        return Mage::app()->getStore()->isCurrentlySecure();
    }

    /**
     * returns form submit URL
     * @return string
     */
    public function getSubmitUrl()
    {
        $formId = $this->getCurrentForm()->getId();
        $url = $this->getUrl('*/*/submitform', array('id' => $formId,'_secure' => $this->_isSecure()));
        return $url;
    }

    /**
     * returns visual captcha start URL
     * @return string
     */
    public function getVisualCaptchaUrl()
    {
        $url = $this->getUrl('plugincompany_contactforms/form/',array('_secure' => $this->_isSecure()));
        return $url;
    }

    /**
     * Returns General File Upload URL
     * @return string
     */
    public function getUploadUrl()
    {
        $url = $this->getUrl('plugincompany_contactforms/form/upload',array('_secure' => $this->_isSecure()));
        return $url;
    }
}
