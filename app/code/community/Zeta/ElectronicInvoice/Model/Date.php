<?php

class Zeta_ElectronicInvoice_Model_Date
{
    /** @var string */
    private $date;

    public function __construct($date)
    {
        $this->date = $date;
    }

    public function __toString()
    {
        try {
            $date = new Zend_Date(strtotime($this->date));
            return $date->toString('Y-MM-dd');
        } catch (Zend_Date_Exception $e) {
            return $this->date;
        }
    }
}
