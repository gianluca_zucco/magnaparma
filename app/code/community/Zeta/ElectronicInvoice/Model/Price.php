<?php
/**
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 */

class Zeta_ElectronicInvoice_Model_Price
{
    public static function toPrice($price)
    {
        return number_format($price, 2, '.', '');
    }
}
