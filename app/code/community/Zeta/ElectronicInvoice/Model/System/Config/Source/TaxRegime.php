<?php

class Zeta_ElectronicInvoice_Model_System_Config_Source_TaxRegime
{
    private $options = [
        'RF01' => 'Ordinario',
        'RF02' => 'Contribuenti minimi',
        'RF03' => 'Nuove iniziative produttive (art.13, L.388/2000)',
        'RF04' => 'Agricoltura e attività connesse e pesca (artt. 34 e 34-bis,D.P.R. 633/1972)',
        'RF05' => 'Vendita sali e tabacchi (art. 74, c.1, D.P.R. 633/1972)',
        'RF06' => 'Commercio dei fiammiferi (art. 74, c.1, D.P.R. 633/1972)',
        'RF07' => 'Editoria (art. 74, c.1, D.P.R. 633/1972)',
        'RF08' => 'Gestione di  servizi  di  telefonia  pubblica  (art.  74,  c.1,D.P.R.  633/1972)',
        'RF09' => 'Rivendita di  documenti di trasporto pubblico e di sosta (art. 74, c.1,D.P.R. 633/1972)',
        'RF10' => 'Intrattenimenti D.P.R. n. 640/72 (art. 74, c.6,D.P.R. 633/1972)',
    ];

    public function toOptionArray()
    {
        return $this->options;
    }
}
