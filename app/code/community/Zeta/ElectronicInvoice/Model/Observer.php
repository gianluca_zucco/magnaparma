<?php

use Varien_Event_Observer as Event;

class Zeta_ElectronicInvoice_Model_Observer
{
    public function addPec(Event $event)
    {
        /** @var Varien_Object $dataset */
        $dataset = $event->getData('dataset');
        /** @var Zeta_ElectronicInvoice_Model_Invoice_Xml_InvoiceData $invoiceData */
        $invoiceData = $event->getData('invoice_data');

        if ($invoiceData->getSDI() === $invoiceData::DEFAULT_SDI) {
            $dataset->setData(array_merge_recursive($dataset->getData(), [
                'DatiTrasmissione' => [
                    'PECDestinatario' => $invoiceData->getPEC(),
                ]
            ]));
        }
    }

    public function addStoreOfficeInformation(Event $event)
    {
        /** @var Varien_Object $dataset */
        $dataset = $event->getData('dataset');

        /** @var Zeta_ElectronicInvoice_Model_Invoice_Xml_Interface $office */
        $address = Mage::getModel('customer/address')
            ->setStreet(Mage::getStoreConfig('zeta_electronicinvoice/registry/address'))
            ->setCountryId(Mage::getStoreConfig('zeta_electronicinvoice/registry/country'))
            ->setRegion(Mage::getStoreConfig('zeta_electronicinvoice/registry/region'))
            ->setPostcode(Mage::getStoreConfig('zeta_electronicinvoice/registry/postcode'));

        $office = Mage::helper('zeta_electronicinvoice')->getXmlRendererInstance('office', $address);

        $dataset->setData(array_merge_recursive($dataset->getData(), [
            'CedentePrestatore' => [
                $office->getNodeName() => $office->toArray(),
            ]
        ]));
    }

    public function addCustomerOfficeInformation(Event $event)
    {
        /** @var Varien_Object $dataset */
        $dataset = $event->getData('dataset');

        /** @var Zeta_ElectronicInvoice_Model_Invoice_Xml_Interface $office */
        $office = Mage::helper('zeta_electronicinvoice')
            ->getXmlRendererInstance('office', $event->getData('billing_address'));

        $dataset->setData(array_merge_recursive($dataset->getData(), [
            'CessionarioCommittente' => [
                $office->getNodeName() => $office->toArray(),
            ]
        ]));
    }

    public function addButtonToInvoiceGrid(Event $event)
    {
        $block = $event->getData('block');
        if ($block instanceof Mage_Adminhtml_Block_Sales_Invoice_Grid || $block instanceof Mage_Adminhtml_Block_Sales_Order_View_Tab_Invoices) {
            $block->addColumn('electronic_invoice_download', [
                'header' => Mage::helper('sales')->__('Action'),
                'width' => '50px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => 'Scarica Fattura Elettronica',
                        'url' => [
                            'base' => '*/electronicInvoice/download'
                        ],
                        'field' => 'invoice_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'is_system' => true
            ]);
        }
    }
}
