<?php

class Zeta_ElectronicInvoice_Model_TransmissionType
{
    const INVOICE_TO_COMPANY = 'FPA12';
    const INVOICE_TO_PRIVATE = 'FPR12';

    /** @var Mage_Sales_Model_Order_Address */
    private $address;

    public function __construct(Mage_Sales_Model_Order_Address $address)
    {
        $this->address = $address;
    }

    public function __toString()
    {
        return $this->address->getCompany() ? self::INVOICE_TO_COMPANY : self::INVOICE_TO_PRIVATE;
    }
}
