<?php

use Mage_Eav_Model_Entity_Attribute_Source_Abstract as AbstractSource;

class Zeta_ElectronicInvoice_Model_Attribute_Source_PreferredInvoiceType extends AbstractSource
{
    protected $_options = ['pec', 'sdi'];

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        return array_map([$this, 'toOption'], $this->_options);
    }

    /**
     * @param string $code
     * @return array
     */
    private function toOption($code)
    {
        return [
            'label' => Mage::helper('zeta_electronicinvoice')->__("label_$code"),
            'value' => $code
        ];
    }
}
