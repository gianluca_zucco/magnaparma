<?php

class Zeta_ElectronicInvoice_Model_Attribute_Data_InvoiceType extends Zeta_ElectronicInvoice_Model_Attribute_Data_Abstract
{
    protected function getErrorMessage()
    {
        return Mage::helper('zeta_electronicinvoice')->__('Please choose an Electronic invoice type');
    }
}
