<?php

use Mage_Core_Controller_Request_Http as Request;
use Mage_Eav_Model_Attribute_Data_Text as AttributeData;

abstract class Zeta_ElectronicInvoice_Model_Attribute_Data_Abstract extends AttributeData
{
    public function validateValue($value)
    {
        $request = Mage::app()->getRequest();
        return $this->isRequired($request) ? $this->validateRequired($value) : true;
    }

    protected function isRequired(Request $request)
    {
        $isCheckout = $request->getParam('billing', []);
        $vatIdAttribute = $isCheckout ? 'vat_id' : 'taxvat';
        $params = $request->getParam('billing', $request->getParams());
        return !!(@$params[$vatIdAttribute]);
    }

    /**
     * @param $value
     * @return array|bool
     */
    protected function validateRequired($value)
    {
        return !$value ? [Mage::helper('zeta_electronicinvoice')->__('Please choose an Electronic invoice type')] : true;
    }

    /**
     * @return string
     */
    abstract protected function getErrorMessage();
}
