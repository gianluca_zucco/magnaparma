<?php

class Zeta_ElectronicInvoice_Model_Config
{
    /**
     * @return string
     */
    public function getCountry()
    {
        return Mage::getStoreConfig('general/store_information/merchant_country');
    }

    /**
     * @return string
     */
    public function getVatNumber()
    {
        return Mage::getStoreConfig('zeta_electronicinvoice/settings/vat_number') ?:
            Mage::getStoreConfig('general/store_information/merchant_vat_number');
    }

    /**
     * @return string
     */
    public function getVatId()
    {
        return Mage::getStoreConfig('zeta_electronicinvoice/settings/vat_id') ?:
            $this->getVatNumber();
    }

    /**
     * @return string
     */
    public function getTaxRegime()
    {
        return Mage::getStoreConfig('zeta_electronicinvoice/settings/tax_regime');
    }

    /**
     * @return string
     */
    public function getDenomination()
    {
        return Mage::getStoreConfig('zeta_electronicinvoice/registry/denomination');
    }


    public function getStoreTelephone()
    {
        return Mage::getStoreConfig('general/store_information/phone');
    }

    public function getStoreEmail()
    {
        return Mage::getStoreConfig('trans_email/ident_general/email');
    }

    public function getIdCode()
    {
        return Mage::getStoreConfig('zeta_electronicinvoice/settings/id_code');
    }

    public function getTaxVatField()
    {
        return Mage::getStoreConfig('zeta_electronicinvoice/settings/address_taxvat_field');
    }

    public function getVatIdField()
    {
        return Mage::getStoreConfig('zeta_electronicinvoice/settings/address_vat_id_field');
    }
}
