<?php

class Zeta_ElectronicInvoice_Model_Address
{
    /** @var Zeta_ElectronicInvoice_Model_Config */
    protected $config;

    /** @var Mage_Sales_Model_Order_Address */
    private $address;

    public function __construct(Mage_Sales_Model_Order_Address $address)
    {
        $this->address = $address;
        $this->config = Mage::getModel('zeta_electronicinvoice/config');
    }

    public function getTaxVat()
    {
        $field = $this->config->getTaxVatField();
        return $field ? $this->address->getData($field): '';
    }

    public function getVatId()
    {
        $field = $this->config->getVatIdField();
        return $field ? $this->address->getData($field): '';
    }

    public function getDenomination()
    {
        return $this->address->getCompany() ?: $this->address->getName();
    }
}
