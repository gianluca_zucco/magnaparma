<?php

use Mage_Sales_Model_Order_Invoice as Invoice;
use Mage_Xml_Generator as XmlGenerator;

class Zeta_ElectronicInvoice_Model_Invoice_Xml
{
    /** @var XmlGenerator */
    private $generator;

    /** @var Invoice */
    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
        $this->generator = new XmlGenerator();
    }

    public function toXml()
    {
        $xml = $this->generator;
        /** @var DOMDocument $dom */
        $dom = $xml->getDom();
        $dom->encoding = "UTF-8";
        $info = Mage::getModel('core/app_emulation')->startEnvironmentEmulation($this->invoice->getStoreId());
        $xml = $this->toInvoiceXml($this->invoice);
        Mage::getModel('core/app_emulation')->stopEnvironmentEmulation($info);
        return $xml;
    }

    /**
     * @param Invoice $invoices
     * @return string
     */
    private function toInvoiceXml(Invoice $invoice)
    {
        $renderer = Mage::helper('zeta_electronicinvoice')->getXmlRendererInstance('FatturaElettronica', $invoice);
        return (string) $this->generator->arrayToXml($renderer->toArray());
    }
}
