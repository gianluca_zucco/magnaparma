<?php

use Mage_Catalog_Model_Product as Product;
use Mage_Sales_Model_Order_Invoice_Item as InvoiceItem;

class Zeta_ElectronicInvoice_Model_Order_Xml_Invoice_Item implements Zeta_ElectronicInvoice_Model_Order_Xml_Interface
{
    /** @var InvoiceItem */
    private $item;

    /** @var int */
    private $index;

    /** @var Product */
    private $product;

    public function __construct(InvoiceItem $item, $index)
    {
        $this->item = $item;
        $this->index = $index;
        $this->product = Mage::getModel('catalog/product')->load($item->getProductId());
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            $this->getNodeName() => array_merge([
                'NumeroLinea' => (string) $this->index,
                'Descrizione' => $this->item->getName(),
                'Quantita' => (float) $this->item->getQty(),
                'PrezzoUnitario' => (float) $this->product->getFinalPrice($this->item->getQty()),
                'PrezzoTotale' => (float) $this->item->getPriceInclTax(),
                'AliquotaIVA' => (float) $this->item->getOrderItem()->getTaxPercent(),
            ], $this->getDiscount())
        ];
    }

    /**
     * @return string
     */
    public function getNodeName()
    {
        return 'DettaglioLinee';
    }

    /**
     * @return array
     */
    private function getDiscount()
    {
        if ($this->item->getDiscountAmount()) {
            $fullPrice = $this->product->getFinalPrice($this->item->getQty());
            $itemPrice = $this->item->getPriceInclTax();
            return [
                'ScontoMaggiorazione' => [
                    'Tipo' => 'SC',
                    'Percentuale' => (string) ($itemPrice * 100) / $fullPrice,
                    'Importo' => (string) round($this->item->getDiscountAmount(), 2),
                ]
            ];
        }
        return [];
    }
}
