<?php

use Mage_Sales_Model_Order_Address as Address;

class Zeta_ElectronicInvoice_Model_Order_Xml_InvoiceData
{
    /** @var Address */
    private $address;

    const DEFAULT_SDI = '0000000';

    public function __construct(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->address->getData('invoice_code');
    }

    public function getSDI()
    {
        return !$this->isEmail() ? $this->getValue() : self::DEFAULT_SDI;
    }

    public function getPEC()
    {
        return $this->isEmail() ? $this->getValue() : '';
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->address->getData('invoice_type');
    }

    /**
     * @return bool
     */
    private function isEmail()
    {
        return strpos($this->getValue(), '@') !== false;
    }
}
