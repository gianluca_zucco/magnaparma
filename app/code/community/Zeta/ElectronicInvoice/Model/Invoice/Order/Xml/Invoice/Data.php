<?php

use Mage_Sales_Model_Order_Invoice as Invoice;
use Mage_Sales_Model_Order_Invoice_Item as InvoiceItem;
use Zeta_ElectronicInvoice_Model_Order_Xml_Invoice_Item as ItemRenderer;

class Zeta_ElectronicInvoice_Model_Order_Xml_Invoice_Data implements Zeta_ElectronicInvoice_Model_Order_Xml_Interface
{
    /** @var Invoice */
    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return iterator_to_array($this->getItems());
    }

    /**
     * @return string
     */
    public function getNodeName()
    {
        return 'DatiBeniServizi';
    }

    public function toLineDetail(InvoiceItem $item, $count)
    {
        $renderer = new ItemRenderer($item, $count);
        return $renderer->toArray();
    }

    /**
     * @return Generator|array[]
     */
    private function getItems()
    {
        $items = array_values($this->invoice->getAllItems());
        foreach ($items as $i => $item) {
            yield $this->toLineDetail($item, $i+1);
        }

        foreach ($this->prepareSummary($items) as $taxPercent => $data) {
            yield ['DatiRiepilogo' => $data];
        }
    }

    /**
     * @param array $items
     * @return mixed
     */
    private function prepareSummary(array $items)
    {
        return array_reduce($items, function (array $summary, InvoiceItem $item) {
            $taxPercent = (int) $item->getOrderItem()->getTaxPercent();
            if (!array_key_exists($taxPercent, $summary)) {
                $summary[$taxPercent] = [
                    'AliquotaIVA' => (float) $taxPercent,
                    'ImponibileImporto' => (float) $item->getRowTotal(),
                    'Imposta' => (float) $item->getTaxAmount(),
                ];
            } else {
                $summary[$taxPercent]['ImponibileImporto'] += (float) $item->getRowTotal();
                $summary[$taxPercent]['Imposta'] += (float) $item->getTaxAmount();
            }
            return $summary;
        }, []);
    }
}
