<?php

use Mage_Customer_Model_Address_Abstract as Address;
use Mage_Sales_Model_Order_Address as OrderAddress;

class Zeta_ElectronicInvoice_Model_Order_Xml_Office implements Zeta_ElectronicInvoice_Model_Order_Xml_Interface
{
    /** @var Address|OrderAddress */
    private $address;

    public function __construct(Address $address)
    {
        $this->address = $address;
    }


    /** @inheritdoc */
    public function toArray()
    {
        return [
            'Indirizzo' => $this->address->getStreetFull(),
            'CAP' => $this->address->getPostcode(),
            'Comune' => 'NP',
            'Provincia' => $this->address->getRegionCode(),
            'Nazione' => $this->address->getCountryId(),
        ];
    }

    /**
     * @return string
     */
    public function getNodeName()
    {
        return 'Sede';
    }
}
