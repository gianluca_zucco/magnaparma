<?php

use Mage_Sales_Model_Order_Invoice as Invoice;
use Zeta_ElectronicInvoice_Model_Invoice_Xml_Interface as XmlInterface;

class Zeta_ElectronicInvoice_Model_Invoice_Xml_Body implements XmlInterface
{
    /** @var Invoice */
    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    public function toArray()
    {
        return [
            'DatiGenerali' => [
                'DatiGeneraliDocumento' => [
                    'TipoDocumento' => 'TD01',
                    'Divisa' => $this->invoice->getOrder()->getOrderCurrencyCode(),
                    'Data' => (string) new Zeta_ElectronicInvoice_Model_Date($this->invoice->getCreatedAt()),
                    'Numero' => $this->invoice->getIncrementId(),
                    'ImportoTotaleDocumento' => round($this->invoice->getGrandTotal(), 2),
                    'Arrotondamento' => (float) 0,
                ],
            ],
            'DatiBeniServizi' => $this->helper()->getXmlRendererInstance('DatiBeniServizi', $this->invoice)->toArray(),
        ];
    }

    /** @inheritdoc */
    public function getNodeName()
    {
        return 'FatturaElettronicaBody';
    }

    /**
     * @return Zeta_ElectronicInvoice_Helper_Data
     */
    private function helper()
    {
        return Mage::helper('zeta_electronicinvoice');
    }
}
