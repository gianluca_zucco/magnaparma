<?php

interface Zeta_ElectronicInvoice_Model_Invoice_Xml_Interface
{
    /**
     * @return array
     */
    public function toArray();

    /**
     * @return string
     */
    public function getNodeName();
}
