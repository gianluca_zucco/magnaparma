<?php

use Mage_Sales_Model_Order as Order;
use Mage_Sales_Model_Order_Address as Address;
use Mage_Sales_Model_Order_Invoice as Invoice;
use Zeta_ElectronicInvoice_Model_Config as Config;
use Zeta_ElectronicInvoice_Model_Invoice_Xml_Interface as XmlInterface;
use Zeta_ElectronicInvoice_Model_Invoice_Xml_InvoiceData as InvoiceData;

class Zeta_ElectronicInvoice_Model_Invoice_Xml_Header implements XmlInterface
{
    /** @var Config */
    private $config;

    /** @var InvoiceData */
    private $invoiceData;

    /** @var Invoice */
    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
        $this->invoiceData = new InvoiceData($this->getBillingAddress());
        $this->config = new Config();
    }

    /** @inheritdoc */
    public function toArray()
    {
        $dataset = new Varien_Object($this->getNodeBody());
        Mage::dispatchEvent('zeta_electronicinvoice_generate_header_before', [
            'dataset' => $dataset,
            'config' => $this->config,
            'invoice' => $this->invoice,
            'invoice_data' => $this->invoiceData,
            'billing_address' => $this->getBillingAddress(),
        ]);
        return $dataset->getData();
    }

    /** @inheritdoc */
    public function getNodeName()
    {
        return 'FatturaElettronicaHeader';
    }

    /**
     * @return Address
     */
    private function getBillingAddress()
    {
        return $this->invoice->getBillingAddress();
    }

    /**
     * @return Order
     */
    private function getOrder()
    {
        return $this->invoice->getOrder();
    }

    /**
     * @return array
     */
    private function getNodeBody()
    {
        $address = new Zeta_ElectronicInvoice_Model_Address($this->getBillingAddress());
        $trasmissione = new Zeta_ElectronicInvoice_Model_TransmissionType($this->getBillingAddress());
        return [
            'DatiTrasmissione' => [
                'IdTrasmittente' => [
                    'IdPaese' => $this->config->getCountry(),
                    'IdCodice' => $this->config->getVatNumber(),
                ],
                'ProgressivoInvio' => $this->getOrder()->getIncrementId(),
                'FormatoTrasmissione' => (string) $trasmissione,
                'CodiceDestinatario' => $this->invoiceData->getSDI(),
            ],
            'CedentePrestatore' => [
                'DatiAnagrafici' => [
                    'IdFiscaleIVA' => [
                        'IdPaese' => $this->config->getCountry(),
                        'IdCodice' => $this->config->getVatNumber(),
                    ],
                    'CodiceFiscale' => $this->config->getVatId(),
                    'Anagrafica' => [
                        'Denominazione' => $this->config->getDenomination(),
                    ],
                    'RegimeFiscale' => $this->config->getTaxRegime(),
                ],
            ],
            'CessionarioCommittente' => [
                'DatiAnagrafici' => [
                    'IdFiscaleIVA' => [
                        'IdPaese' => $this->getBillingAddress()->getCountry(),
                        'IdCodice' => $address->getTaxVat() ?: $address->getVatId(),
                    ],
                    'Anagrafica' => [
                        'Denominazione' => $address->getDenomination(),
                    ]
                ],
            ]
        ];
    }
}
