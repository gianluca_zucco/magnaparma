<?php

use Mage_Sales_Model_Order_Invoice as Invoice;
use Zeta_ElectronicInvoice_Model_Invoice_Xml_Interface as XmlInterface;

class Zeta_ElectronicInvoice_Model_Invoice_Xml_Root implements XmlInterface
{
    /** @var Invoice */
    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /** @inheritdoc */
    public function toArray()
    {
        $components = iterator_to_array($this->getElements());
        $data = [
            $this->getNodeName() => [
                '_attribute' => $this->prepareRootAttributes(),
                '_value' => $components,
            ],
        ];

        array_walk_recursive($data, function(&$item) {
            if (is_float($item)) {
                $item = Zeta_ElectronicInvoice_Model_Price::toPrice($item);
            } else {
                $item = (string) $item;
            }
        });

        return $data;
    }

    public function getElements()
    {
        $header = $this->helper()->getXmlRendererInstance('FatturaElettronicaHeader', $this->invoice);
        $body = $this->helper()->getXmlRendererInstance('FatturaElettronicaBody', $this->invoice);
        yield [
            $header->getNodeName() => [
                '_attribute' => [
                    'xmlns' => ''
                ],
                '_value' => $header->toArray(),
            ]
        ];

        yield [
            $body->getNodeName() => [
                '_attribute' => [
                    'xmlns' => ''
                ],
                '_value' => $body->toArray(),
            ]
        ];
    }

    /** @inheritdoc */
    public function getNodeName()
    {
        return 'FatturaElettronica';
    }

    /**
     * @return Zeta_ElectronicInvoice_Helper_Data
     */
    private function helper()
    {
        return Mage::helper('zeta_electronicinvoice');
    }

    private function prepareRootAttributes()
    {
        return array_merge(Mage::getStoreConfig('zeta_electronicinvoice/root_attributes'), [
            'xmlns:xsd' => 'http://www.w3.org/2001/XMLSchema',
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
        ]);
    }
}
