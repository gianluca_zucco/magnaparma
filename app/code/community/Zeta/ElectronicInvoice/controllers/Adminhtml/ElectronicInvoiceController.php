<?php

use Zeta_ElectronicInvoice_Model_Invoice_Xml as InvoiceXml;

class Zeta_ElectronicInvoice_Adminhtml_ElectronicInvoiceController extends Mage_Adminhtml_Controller_Action
{
    public function downloadAction()
    {
        /** @var Mage_Sales_Model_Order_Invoice $invoice */
        $invoice = Mage::getModel('sales/order_invoice')->load($this->getRequest()->getParam('invoice_id'));
        $generator = new InvoiceXml($invoice);
        $xml = $generator->toXml();
        $this->_prepareDownloadResponse(
            "{$invoice->getOrderIncrementId()}_{$invoice->getIncrementId()}.xml",
            $xml,
            strlen($xml),
            'application/xml'
        );
    }

    protected function _isAllowed()
    {
        return true;
    }
}
