<?php

use Mage_Sales_Model_Resource_Setup as Setup;

$installer = new Setup('core_setup');

$attributeCodes = ['invoice_type', 'invoice_code'];

foreach (['quote_address', 'order_address'] as $entityType) {
    $installer->addAttribute($entityType, $attributeCodes[0], [
        'label' => 'Electronic Invoice Type',
        'type' => 'varchar',
    ]);
}

foreach (['quote_address', 'order_address'] as $entityType) {
    $installer->addAttribute($entityType, $attributeCodes[1], [
        'label' => 'Electronic Invoice Code',
        'type' => 'varchar',
    ]);
}


