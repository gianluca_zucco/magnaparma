<?php

use Mage_Customer_Model_Entity_Setup as Setup;

$installer = new Setup('core_setup');

$entityTypeId = $installer->getEntityTypeId('customer_address');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$attributeCodes = ['invoice_type', 'invoice_code'];

$installer->addAttribute('customer_address', $attributeCodes[0], [
    'label' => 'Electronic Invoice Type',
    'type' => 'varchar',
    'required' => true,
    'input' => 'select',
    'visible' => true,
    'source' => 'zeta_electronicinvoice/attribute_source_preferredInvoiceType',
    'data' => 'zeta_electronicinvoice/attribute_data_invoiceType'
]);

$installer->addAttribute('customer_address', $attributeCodes[1], [
    'label' => 'Electronic Invoice Code',
    'type' => 'varchar',
    'required' => true,
    'input' => 'text',
    'visible' => true,
    'data' => 'zeta_electronicinvoice/attribute_data_invoiceCode'
]);

foreach ($attributeCodes as $i => $attributeCode) {

    $attribute = Mage::getSingleton("eav/config")->getAttribute('customer_address', $attributeCode);

    $attribute->addData([
        'used_in_forms' => ['customer_register_address','customer_address_edit','adminhtml_customer_address'],
        'is_used_for_customer_segment' => false,
        'is_system' => 0,
        'is_user_defined' => 1,
        'is_visible' => 1,
        'sort_order' => 100+$i,
    ])->save();
}

