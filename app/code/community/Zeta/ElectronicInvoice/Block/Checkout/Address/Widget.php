<?php

use Mage_Customer_Model_Customer as Customer;
use Mage_Sales_Model_Quote_Address as Address;

class Zeta_ElectronicInvoice_Block_Checkout_Address_Widget extends Mage_Core_Block_Template
{
    /**
     * @param string $attributeCode
     * @return string
     */
    public function getSelectOptionsHtml($attributeCode)
    {
        return implode(PHP_EOL, array_map(function(array $option) use ($attributeCode)    {
            $selected = $this->getValue($attributeCode) == $option['value'] ? 'selected="selected"' : '';
            return "<option value=\"{$option['value']}\" $selected>{$option['label']}</option>";
        }, $this->helper('zeta_electronicinvoice')->getAttributeOptions($attributeCode)));
    }

    /**
     * @param string $string
     * @return string
     */
    public function getValue($string)
    {
        return (string) ($this->getBillingAddress()->getData($string) ?: $this->getCustomer()->getData($string));
    }

    /**
     * @return Address
     */
    private function getBillingAddress()
    {
        return Mage::getSingleton('checkout/cart')->getQuote()->getBillingAddress();
    }

    /**
     * @return Customer
     */
    private function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
}
