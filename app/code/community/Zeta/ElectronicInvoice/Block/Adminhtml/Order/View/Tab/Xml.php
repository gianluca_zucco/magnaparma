<?php

use Mage_Adminhtml_Block_Widget_Tab_Interface as TabInterface;
use Mage_Core_Block_Abstract as Block;
use Mage_Sales_Model_Order as Order;
use Mage_Sales_Model_Order_Invoice as Invoice;
use Mage_Adminhtml_Block_Sales_Order_View as OrderView;use Mage_Sales_Model_Order_Invoice;

class Zeta_ElectronicInvoice_Block_Adminhtml_Order_View_Tab_Xml extends Block implements TabInterface
{
    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Electronic Invoice');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return $this->getOrder()->getInvoiceCollection()->count() > 0;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return !$this->canShowTab();
    }

    protected function _toHtml()
    {
        $invoices = $this->getOrder()->getInvoiceCollection()->getItems();
        return implode(PHP_EOL, array_reduce($invoices, function(array $values, Invoice $invoice) {
            $model = new Zeta_ElectronicInvoice_Model_Invoice_Xml($invoice);
            $values[] = "<pre>{$this->escapeHtml($model->toXml())}</pre>";
            return $values;
        }, []));
    }

    /**
     * @return Order
     */
    private function getOrder()
    {
        return Mage::registry('current_order');
    }
}
