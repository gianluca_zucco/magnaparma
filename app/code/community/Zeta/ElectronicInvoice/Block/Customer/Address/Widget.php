<?php

use Mage_Customer_Model_Address as Address;

class Zeta_ElectronicInvoice_Block_Customer_Address_Widget extends Mage_Customer_Block_Widget_Abstract
{
    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->getParentBlock()->getAddress() ?: new Address();
    }

    /**
     * @param string $string
     * @return string
     */
    public function getValue($string)
    {
        return (string) $this->escapeHtml($this->getAddress()->getData($string));
    }

    /**
     * @param string $attributeCode
     * @return string
     */
    public function getSelectOptionsHtml($attributeCode)
    {
        return implode(PHP_EOL, array_map(function(array $option) use ($attributeCode)    {
            $selected = $this->getValue($attributeCode) == $option['value'] ? 'selected="selected"' : '';
            return "<option value=\"{$option['value']}\" $selected>{$option['label']}</option>";
        }, $this->helper('zeta_electronicinvoice')->getAttributeOptions($attributeCode)));
    }
}
