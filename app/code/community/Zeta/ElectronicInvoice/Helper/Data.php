<?php

use Zeta_ElectronicInvoice_Model_Invoice_Xml_Interface as XmlInterface;

class Zeta_ElectronicInvoice_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param $attributeCode
     * @return array
     */
    public function getAttributeOptions($attributeCode)
    {
        try {
            return Mage::getResourceSingleton('customer/address')
                ->getAttribute($attributeCode)
                ->getSource()->getAllOptions();
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
        }
        return [];
    }

    /**
     * @param string $type
     * @param array|Mage_Core_Model_Abstract $arguments
     * @return XmlInterface
     */
    public function getXmlRendererInstance($type, $arguments = [])
    {
        $className = Mage::getStoreConfig("zeta_electronicinvoice/xml_renderer/$type");
        return Mage::getConfig()->getModelInstance($className, $arguments);
    }
}
