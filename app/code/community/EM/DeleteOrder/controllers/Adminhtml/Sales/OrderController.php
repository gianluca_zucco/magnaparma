<?php

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . '/Sales/OrderController.php';

class EM_DeleteOrder_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{
    public function deleteorderAction()
    {
        $orderIds = array_map('trim', $this->getRequest()->getPost('order_ids', array()));
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        /** @var Varien_Db_Adapter_Pdo_Mysql $write */
        $write = $resource->getConnection('core_write');
        if ($orderIds) {
            $write->delete($resource->getTableName('sales/order_grid'),
                $write->quoteInto('entity_id IN (?)', $orderIds));
            $this->_getSession()->addSuccess(Mage::helper('sales')->__('Order(s) deleted.'));
        }
        $this->_redirect('*/*/');
    }
}
