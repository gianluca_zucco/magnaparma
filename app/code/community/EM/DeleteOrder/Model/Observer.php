<?php
/**
 *
 * @author Gianluca Zucco <g.zucco@mzentrale.de>
 * @project magnaparma
 */

class EM_DeleteOrder_Model_Observer
{
    /**
     * Listens to
     * - adminhtml_block_html_before
     * @param Varien_Event_Observer $observer
     */
    public function addMassaction(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_Grid) {
            $block->getMassactionBlock()->addItem('delete_order', array(
                'label'=> Mage::helper('sales')->__('Delete order'),
                'url'  => $block->getUrl('*/sales_order/deleteorder'),
            ));
        }
    }
}
