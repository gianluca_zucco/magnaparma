<?php

class VeInteractive_VePlatform_Block_Template extends Mage_Core_Block_Template {
   
    public function renderLocalTemplate( $template_name, $params = array() ) 
    {        
        $template_path = Mage::getModuleDir( '', $this->getModuleName() ). DS. 'templates'. DS. $template_name. '.phtml';        
        $inums = extract( $params );
        
        ob_start();
            require $template_path;
            $html = ob_get_contents();
        ob_end_clean();
        
        return $html;
    }
}
