<?php

class VeInteractive_VePlatform_Block_ConfigFieldset
  extends Mage_Adminhtml_Block_System_Config_Form_Fieldset {

    const MY_SECTION = "veplatform_options";
    const MY_PACKAGE = "veplatform";
    const MY_THEME = "default";
    const PUBLIC_DESIGN_AREA = "skin";
    const IMAGES_DIR = "images";
    const FONTS_DIR = "fonts";
    const PRODUCTS_MODEL_KEY = "VeInteractive_VePlatform/Products";
    const PRODUCTS_SETTINGS_KEY = 'veplatform_options/settings/products';
    const THANKS_DISPLAYED_KEY_PATTERN = "veplatform/adminhtml/%s/thanks_displayed";
    const HEADER_TEMPLATE_PATH = "systemconfig/%s/formfieldset/header.phtml";
    const FOOTER_TEMPLATE_PATH = "systemconfig/%s/formfieldset/footer.phtml";

    private $on_my_section;
    private $found_table;
    private $model;

    public function getMyImagesUrl( $image )
    {
        return $this->getSkinUrl(). self::IMAGES_DIR. DS. $image;
    }

    public function getMyFontsUrl( $font )
    {
        return $this->getSkinUrl(). self::FONTS_DIR. DS. $font;
    }

    public function isVeAssistCtrlEnabled()
    {
        return $this->isProductCtrlEnabled( $this->getModel()->getVeAssistID() );
    }

    public function isVeAdsCtrlEnabled()
    {
        return $this->isProductCtrlEnabled( $this->getModel()->getVeAdsID() );
    }

    public function isVeChatCtrlEnabled()
    {
        return $this->isProductCtrlEnabled( $this->getModel()->getVeChatID() );
    }

    public function isVeContactCtrlEnabled()
    {
        return $this->isProductCtrlEnabled( $this->getModel()->getVeContactID() );
    }

    public function isAnyProductCtrlEnabled()
    {
        if( $this->isVeChatCtrlEnabled() ) return true;
        if( $this->isVeContactCtrlEnabled() ) return true;
        if( $this->isVeAssistCtrlEnabled() ) return true;
        if( $this->isVeAdsCtrlEnabled() ) return true;
        // Add products.
        return false;
    }

    public function triggerThanksTime() {
        $isThanksDisplayed = true;
        $products = $this->getModel()->getAlls();
        if( $products ) {
            for( $idx = 0; $idx < count( $products ); $idx++ ) {
                if( $this->isProductCtrlEnabled( $products[ $idx ] ) ) {
                    $isProductThanksDisplayed = $this->triggerThanksDisplayed( $products[ $idx ] );
                    if( !$isProductThanksDisplayed ) $isThanksDisplayed = false;
                }
            }
        }
        return !$isThanksDisplayed;
    }

    private function triggerThanksDisplayed( $product ) {
        $config_path = sprintf( self::THANKS_DISPLAYED_KEY_PATTERN, $product );
        $thanks_displayed = Mage::getStoreConfig( $config_path );
        if( !$thanks_displayed ) {
            Mage::getConfig()->saveConfig( $config_path, true );
            Mage::app()->getCacheInstance()->cleanType( "config" );
        }
        return $thanks_displayed;
    }

    private function isProductCtrlEnabled( $product ) {
        $products = self::getProductsEnabled();
        return ( $products && in_array( $product, $products ) );
    }

    public static function setProductsEnabled( $products ) {
        if( empty( $products ) ) $products = array();
        $products_list = implode( ',', $products );
        $products_list = ','. $products_list;
        // Example: $products_list = ',vecontact,vechat'.
        // VeInteractive_VePlatform_Model_Products - save it as list.
        Mage::getConfig()->saveConfig( self::PRODUCTS_SETTINGS_KEY, $products_list );
        Mage::app()->getCacheInstance()->cleanType( "config" );
    }
    
    public static function getProductsEnabled() {
        // Example: $products_list = ',vecontact,vechat'.
        // VeInteractive_VePlatform_Model_Products - save it as list.
        $products = null;
        $products_list = Mage::getStoreConfig( self::PRODUCTS_SETTINGS_KEY );
        if( $products_list ) $products =  explode( ',', trim( $products_list, ',' ) );
        return $products;
    }
        
    private function getModel() {
        if( !$this->model ) $this->model = Mage::getModel( self::PRODUCTS_MODEL_KEY );
        return $this->model;
    }

    protected function _construct(){
        parent::_construct();
        $section = $this->getAction()->getRequest()->getParam( 'section', false );
        $this->on_my_section = ( $section === self::MY_SECTION );
    }

    /* To use this function remove fieldset from template.
    protected function _getHeaderHtml( $element ) {

        $header = parent::_getHeaderHtml( $element );

        if( $this->on_my_section ) {

            $header_without_table = $this->removeTable( $header, true );
            if( $header_without_table !== false ) {
                $this->found_table = true;
                $this->setData( "parent_html_id", $element->getHtmlId() );
                $template_path = sprintf( self::HEADER_TEMPLATE_PATH, $element->getHtmlId() );
                $header = $header_without_table. $this->generateHtml( $template_path );
            }

        }

        return $header;

    }
    */

    protected function _getHeaderHtml( $element ) {
        if( !$this->on_my_section ) {
            $header = parent::_getHeaderHtml( $element );
        }
        else {
            $this->setData( "parent_html_id", $element->getHtmlId() );
            $template_path = sprintf( self::HEADER_TEMPLATE_PATH, $element->getHtmlId() );
            $header = $this->generateHtml( $template_path );
        }
        return $header;
    }

    /* To use this function remove fieldset from template.
    protected function _getFooterHtml( $element ) {

        $footer = parent::_getFooterHtml( $element );

        if( $this->on_my_section && $this->found_table ) {

            $footer_without_table = $this->removeTable( $footer, false );
            if( $footer_without_table !== false ) {
                $template_path = sprintf( self::FOOTER_TEMPLATE_PATH, $element->getHtmlId() );
                $footer = $footer_without_table. $this->generateHtml( $template_path );
            }

        }

        return $footer;
    }
    */

    protected function _getFooterHtml($element) {
        if( !$this->on_my_section ) {
            $footer = parent::_getFooterHtml( $element );
        }
        else {
            $template_path = sprintf( self::FOOTER_TEMPLATE_PATH, $element->getHtmlId() );
            $footer = $this->generateHtml( $template_path );
        }
        return $footer;
    }

    private function generateHtml( $template_path ) {

        $this->setTemplate( $template_path );

        // Change Theme only for this Block.
        $design = Mage::getDesign();
        $package_name = $design->getPackageName();
        $theme_name = $design->getTheme( self::PUBLIC_DESIGN_AREA );
        $design->setPackageName( self::MY_PACKAGE )->setTheme( self::MY_THEME );

            $html = $this->toHtml();

        // Restore Theme.
        $design->setPackageName( $package_name )->setTheme( $theme_name );

        return $html;

    }

    /*
    private function removeTable( $html, $toend ) {

        if( $toend ) {
            // Remove from <table> to end.
            $ipos = stripos( $html, "<table " );
            if( $ipos !== false )
                $html_without_table = substr( $html, 0, $ipos );
        } else {
            // Remove until </table> included.
            $ipos = stripos( $html, "</table>" );
            if( $ipos !== false )
                $html_without_table = substr( $html, $ipos + strlen( "</table>" ), strlen( $html ) - strlen( "</table>" ) - $ipos );
        }

        return $html_without_table;

    }
    */
}