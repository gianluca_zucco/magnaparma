<?php

class VeInteractive_VePlatform_Observer_AdminUserLoadAfter {

    /**
     * Time in seconds to recheck
     */
    const REQUEST_THRESHOLD = 3600;

    /**
     * Checks if the client has been created in VePlatform, will be called when the user access the admin panel
     *
     * @param  [type] $observer
     * @return null
     */
    public function checkInstallation($observer)
    {
        $isAjax = Mage::app()->getRequest()->isXmlHttpRequest();
        if(!$isAjax) {
            $this->checkModuleInstallation();
            $this->checkJourneyInstallation();
        }
    }

    private function checkJourneyInstallation()
    {
        // Ensure journey code exists
        $journeyTagUrl = Mage::getStoreConfig(VeInteractive_VePlatform_Helper_Data::TAG_URL);

        // If not install
        if(!$journeyTagUrl) {
            $helper = Mage::helper('VeInteractive_VePlatform');

            // get last request
            $lastRequest = (int) Mage::getStoreConfig(VeInteractive_VePlatform_Helper_Data::CONFIG_LAST_REQUEST);

            // Is the user reting
            $veretry = (int) Mage::app()->getRequest()->getParam('veretry', 0);

            // If it has spent an hour since last request or if it is the first time we do the journey request (install)
            $delta = time() - $lastRequest;

            if($veretry || self::REQUEST_THRESHOLD < $delta) {

                // get data to send
                $data = array(
                    'domain' => $helper->getBaseUrl(),
                    'language' => $helper->getLang(),
                    'email' =>  Mage::getSingleton('admin/session')->getUser()->getEmail(),
                    'phone' => Mage::getStoreConfig('general/store_information/phone'),
                    'merchant' => Mage::app()->getWebsite()->getName(),
                    'country' => Mage::getStoreConfig('general/country/default'),
                    'currency'=> Mage::app()->getBaseCurrencyCode()
                );

                $jsonString = $helper->install($data);
                
                $response = false;
                if($jsonString) {
                    
                    $response = json_decode($jsonString);

                    if( $response && isset($response->URLTag) && isset($response->URLPixel) && isset($response->Token)) {
                        $journeyTagUrl = $response->URLTag;
                        $journeyPixelUrl = $response->URLPixel;
                        $token = $response->Token;
                        if( strpos($journeyTagUrl, 'https://') === 0) {
                            $journeyTagUrl = substr($journeyTagUrl, 6);
                        }
                        else if( strpos($journeyTagUrl, 'http://') === 0) {
                            $journeyTagUrl = substr($journeyTagUrl, 5);
                        }
                        if( strpos($journeyPixelUrl, 'https://') === 0) {
                            $journeyPixelUrl = substr($journeyPixelUrl, 6);
                        }
                        else if( strpos($journeyPixelUrl, 'http://') === 0) {
                            $journeyPixelUrl = substr($journeyPixelUrl, 5);
                        }
                    }
                }
                
                // If a Journey code is returned we save it
                $config = new Mage_Core_Model_Config();
                if( isset($journeyTagUrl) && $journeyTagUrl && isset($journeyPixelUrl) && $journeyPixelUrl && isset($response->Token) && $token) {

                    $config ->saveConfig(VeInteractive_VePlatform_Helper_Data::TAG_URL, $journeyTagUrl, 'default', 0);
                    $config ->saveConfig(VeInteractive_VePlatform_Helper_Data::PIXEL_URL, $journeyPixelUrl, 'default', 0);
                    $config ->saveConfig(VeInteractive_VePlatform_Helper_Data::TOKEN, $token, 'default', 0);
                }
                else {
                    $config ->saveConfig(VeInteractive_VePlatform_Helper_Data::CONFIG_LAST_REQUEST, time(), 'default', 0);
                }
                // Clear config cache
                Mage::app()->getCacheInstance()->cleanType( "config" );
            }
        }

        // Check again, maybe we have it now
        if(!$journeyTagUrl) {
            if(!$this->isMessageSended()) {
                $optionsUrl = Mage::helper("adminhtml")->getUrl("adminhtml/system_config/edit/section/veplatform_options", array('veretry' => 1));
                $message = Mage::helper('VeInteractive_VePlatform')->__('There was a problem connecting to Ve Interactive Server, <a href="%s">try again please</a>', $optionsUrl);
                Mage::getSingleton('adminhtml/session')->addWarning($message);
            }
        }
        // If we have it we will check if it has select product
        else {
            // Check if has select any product, If not we show him a notice.
            if(!VeInteractive_VePlatform_Block_ConfigFieldset::getProductsEnabled()) {
                if( !$this->isMessageSended() && !$this->isOptionsPage() ) {
                    $optionsUrl = Mage::helper("adminhtml")->getUrl("adminhtml/system_config/edit/section/veplatform_options");
                    $message = Mage::helper('VeInteractive_VePlatform')->__('You have installed VePlatform successfully, please select which products want you to use: <a href="%s">Select Ve Products</a>', $optionsUrl);
                    Mage::getSingleton('adminhtml/session')->addNotice($message);
                }
            }
        }
    }

    /**
     * Check if is the Ve options page
     *
     * @return boolean [description]
     */
    private function isOptionsPage()
    {
        $params = Mage::app()->getRequest()->getParams();
        if(isset($params['section']) && 'veplatform_options' == $params['section']) {
            return true;
        }

        return false;
    }

    /**
     * Check if we have already sended the message
     *
     * @return boolean [description]
     */
    private function isMessageSended()
    {
        $toSearch = array('VePlatform', 'VeInteractive');
        $smessages = Mage::getSingleton('adminhtml/session')->getMessages()->getItems();
        foreach ($smessages as $smessage) {
            foreach ($toSearch as $toSearchItem) {
                if(stripos($smessage->getText(), $toSearchItem) !== false) {
                    return true;
                }
            }
        }

        return false;
    }

    private function checkModuleInstallation()
    {
        // Update ACL is needed after module installation to avoid 404 Error while accessing System > Configuration > VeInteractive > VePlatform.
        $module_installed = Mage::getStoreConfig( VeInteractive_VePlatform_Helper_Data::MODULE_INSTALLED );
        if( !$module_installed ) {
            Mage::getConfig()->saveConfig( VeInteractive_VePlatform_Helper_Data::MODULE_INSTALLED, true );
            Mage::app()->getCacheInstance()->cleanType( "config" );
            Mage::getSingleton( 'admin/session' )->setAcl( Mage::getResourceModel( 'admin/acl' )->loadAcl() );
        }
    }
}