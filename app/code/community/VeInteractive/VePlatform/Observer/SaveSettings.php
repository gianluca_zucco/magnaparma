<?php

class VeInteractive_VePlatform_Observer_SaveSettings
{
    const ACTIVATED_PRODUCTS_KEY = 'veplatform/adminhtml/activatedproducts';
    
    public function sendData()
    {
        $recentSaveSettings = VeInteractive_VePlatform_Block_ConfigFieldset::getProductsEnabled();
        if( $recentSaveSettings ) {                        
            $alreadyActivatedProducts = @unserialize( Mage::getStoreConfig( self::ACTIVATED_PRODUCTS_KEY ) );
            if( is_array( $alreadyActivatedProducts ) )
                $currentActivatedProducts = array_diff( $recentSaveSettings, $alreadyActivatedProducts );
            else 
                $currentActivatedProducts = $recentSaveSettings;        
            if( $currentActivatedProducts ) {

                $helper = Mage::helper('VeInteractive_VePlatform');

                $token = Mage::getStoreConfig(VeInteractive_VePlatform_Helper_Data::TOKEN);

                $data = array(
                    'token' => $token,
                    'language' => $helper->getLang(),
                    'domain' => $helper->getBaseUrl(),
                    'email' =>  Mage::getSingleton('admin/session')->getUser()->getEmail(),
                    'country' => Mage::getStoreConfig('general/country/default'),
                    'currency'=>Mage::app()->getBaseCurrencyCode(),
                    'taskId' => 1,
                    'appCodes' => self::getAppCodes( $currentActivatedProducts ),
                );

                $jsonString = $helper->sendProducts($data);

                if( $jsonString ) {
                    Mage::getConfig()->saveConfig( self::ACTIVATED_PRODUCTS_KEY, serialize( $recentSaveSettings ) );
                    Mage::app()->getCacheInstance()->cleanType( "config" );
                } else {
                    // Rollback changes already saved, before to call this method.
                    VeInteractive_VePlatform_Block_ConfigFieldset::setProductsEnabled( $alreadyActivatedProducts );
                    $message = Mage::helper('VeInteractive_VePlatform')->__('There was a problem connecting to Ve Interactive Server, try again please.');
                    throw new Mage_Core_Exception( $message );
                }
            }
        }
   }

    static private function getAppCodes( $productSettings ) 
    {  
        $appCodes = array();
        if(in_array('vecontact', $productSettings)) {
            $appCodes[] = 1;
        }
        if(in_array('vechat', $productSettings)) {
            $appCodes[] = 2;
        }
        if(in_array('veassist', $productSettings)) {
            $appCodes[] = 3;
        }
        if(in_array('veads', $productSettings)) {
            $appCodes[] = 5;
        }
        $appCodes = implode('|', $appCodes);        
        return $appCodes;
    }
}
