<?php

class VeInteractive_VePlatform_Model_Products
{
    public function toOptionArray()
    {
        return array(
            array( 'value' => '', 'label'=> '' ), // BUG-FIX for Magento < 1.7.
            array( 'value' => $this->getVeContactID(), 'label' => Mage::helper('VeInteractive_VePlatform')->__('VeContact') ),
            array( 'value' => $this->getVeChatID(), 'label' => Mage::helper('VeInteractive_VePlatform')->__('VeChat') ),
            array( 'value' => $this->getVeAssistID(), 'label' => Mage::helper('VeInteractive_VePlatform')->__('VeAssist') ),
            array( 'value' => $this->getVeAdsID(), 'label' => Mage::helper('VeInteractive_VePlatform')->__('VeAds') ),
        );
    }

    public function getAlls()
    {
        return array(
            $this->getVeContactID(),
            $this->getVeChatID(),
            $this->getVeAssistID(),
            $this->getVeAdsID(),
        );
    }

    public function getVeChatID()
    {
        return "vechat";
    }

    public function getVeContactID()
    {
        return "vecontact";
    }

    public function getVeAssistID()
    {
        return "veassist";
    }

    public function getVeAdsID()
    {
        return "veads";
    }
}
