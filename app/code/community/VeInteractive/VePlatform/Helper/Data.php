<?php

class VeInteractive_VePlatform_Helper_Data extends Mage_Core_Helper_Abstract
{
    const TAG_URL = 'veplatform/journey/tag';
    const PIXEL_URL = 'veplatform/journey/pixel';
    const TOKEN = 'veplatform/journey/token';
    const CONFIG_LAST_REQUEST = 'veplatform/journey/lastrequest';
    const MODULE_INSTALLED = 'veplatform/adminhtml/module_installed';

    private $baseUrl;

    public function __construct()
    {
        $this->baseUrl = Mage::getConfig()->getNode('default/veplatform/service/url');
    }


    private function httpPost($url, $parameters)
    {
        Mage::log( "BEGIN[httpPost] - Send(". $url. ") = ". var_export( $parameters, true ) ); 
        
        $journey = false;

        
        $client = new Varien_Http_Client($url);
        $client->setMethod(Varien_Http_Client::POST);
        $client->setConfig(array(
            'timeout'=>25,
        ));

        $client->setRawData(json_encode($parameters), "application/json;charset=UTF-8");

        //more parameters
        try{
            $response = $client->request();

            if ($response->isSuccessful()) {
                $journey = $response->getBody();
            }
        } catch (Exception $e) {
        }

        Mage::log( "END[httpPost] - Receive = ". var_export( $journey, true ) ); 
        return $journey;
    }

    public function install($data)
    {
        
        return $this->httpPost( $this->baseUrl . '/API/Magento/Install', $data);
    }

    public function sendProducts($data)
    {
        return $this->httpPost( $this->baseUrl . '/API/Magento/ActivateProducts', $data);
    }

    public function getLang()
    {
        $lang = Mage::getStoreConfig('general/locale/code');
        $subDashPositionInLang = strpos($lang, '_');
        if($subDashPositionInLang > 0) {
            $lang = substr($lang, 0, $subDashPositionInLang);
        }
        return $lang;
    }
    
    public function getBaseUrl()
    {
        return preg_replace("(^https?://)", "", Mage::getBaseUrl( Mage_Core_Model_Store::URL_TYPE_WEB, false ) );
    }
}
