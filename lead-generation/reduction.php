<?php require("varie/mail-fr.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width" />
<meta name="robots" content="noindex, nofollow" />
<title>Inscrivez-vous pour recevoir tout de suite 5% de r�duction sur MagnaParma</title>
<link rel="stylesheet" type="text/css" href="css/landing.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800' rel='stylesheet' type='text/css'>
<script>
	function controllaModulo() {
		var re=/ /g;
		var espressione = /^[_a-z0-9+-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$/;
		var nome = document.newsletter_magna.FNAME.value.replace(re, "");
		var email = document.newsletter_magna.EMAIL.value.replace(re, "");
				
		if (email == "") { document.getElementById("err_mail").style.display = "block"; document.newsletter_magna.EMAIL.focus(); document.getElementById("mce-EMAIL").style.borderColor = "#bd001e"; return false; }
		else { document.getElementById("err_mail").style.display = "none"; document.getElementById("mce-EMAIL").style.borderColor = "#333";}
		
		if (email != "" && !espressione.test(email)) { document.getElementById("err_val_mail").style.display = "block"; document.newsletter_magna.EMAIL.focus(); document.getElementById("mce-EMAIL").style.borderColor = "#bd001e"; return false; }
		else { document.getElementById("err_val_mail").style.display = "none"; document.getElementById("mce-EMAIL").style.borderColor = "#333"; } 
		
		if (nome == "") {document.getElementById("err_nome").style.display = "block"; document.newsletter_magna.FNAME.focus(); document.getElementById("mce-FNAME").style.borderColor = "#bd001e"; return false; }
		else {document.getElementById("err_nome").style.display = "none"; document.getElementById("mce-FNAME").style.borderColor = "#333";}
		
		if(document.newsletter_magna.privacy.checked) {  document.getElementById("err_pri").style.display = "none"; }
		else {  document.getElementById("err_pri").style.display = "block"; return false; }
		
		document.newsletter_magna.submit();
	}
</script>
</head>
<body>
<!-- BEGIN GOOGLE ANALYTICS CODE -->
<script type="text/javascript">
//<![CDATA[
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
    })();

    var _gaq = _gaq || [];

_gaq.push(['_setAccount', 'UA-17768269-1']);
_gaq.push(['_trackPageview']);


//]]>
</script>
<!-- END GOOGLE ANALYTICS CODE -->
	<div id="logo"><img src="immagini/logo.gif" alt="" border="0" /></div>
    <div id="wrapper">
        <div id="slogan"><h1>Recevez 5% de r�duction</h1></div>
        <div id="form">
            Inscrivez-vous � la newsletter pour obtenir une <strong>r�duction de 5%</strong> pour vos courses sur <strong>MagnaParma</strong>.<br />
Seulement les meilleurs produits typiques de Parme, choisis pour vous, � votre domicile en quelques heures.
			<?php if(isset($popup)) { echo "<br /><strong style='color:#bd001e;'>".$popup."</strong>"; } ?>
			<br /><br />
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
            <form action="reduction.php" method="post" id="newsletter_magna" name="newsletter_magna" class="validate" novalidate>
                <div id="mc_embed_signup_scroll">
                    <div class="mc-field-group">
                		<label for="mce-EMAIL" style="font-size:16px;"><strong>Votre adresse E-mail</strong> <span class="asterisk">*</span></label><br />
                		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" style="width:100%; height:25px; border:1px solid #333; margin:0px; padding:0px;">
                		<div id="err_mail">&uArr; &Eacute;crivez votre adresse E-mail</div><div id="err_val_mail">&uArr; &Eacute;crivez une adresse E-mail valide</div>
            		</div><br style="line-height:10px;" />
            		<div class="mc-field-group">
                		<label for="mce-FNAME" style="font-size:16px;"><strong>Votre Nom</strong> <span class="asterisk">*</span></label><br />
               			<input type="text" value="" name="FNAME" class="required" id="mce-FNAME" style="width:100%; height:25px; border:1px solid #333; margin:0px; padding:0px;">
               			<div id="err_nome">&uArr; &Eacute;crivez votre Nom</div>
            		</div>
                    <div class="mc-field-group" align="right" style="margin-top:5px; font-size:11px">				
                    	Je autorise l'utilisation des donn�es personnelles<br />(art.13 D.Lgs. 196/2003) - <u><strong><a href="privacy-fr.php" style="color:inherit; text-decoration:none;" target="_blank" title="Termini e Condizioni">Conditions g�n�rales de ventes</a></strong></u>*&nbsp;<input type="checkbox" name="privacy" id="privacy" />
                        <div id="err_pri" align="right">Vous devez accepter la notice de confidentialit� &uArr;</div>
                    </div>
            	    <div id="mce-responses" class="clear">
                	    <div class="response" id="mce-error-response" style="display:none"></div>
                    	<div class="response" id="mce-success-response" style="display:none"></div>
                	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                	<div style="position: absolute; left: -5000px;"><input type="hidden" name="azione" value="1"></div><br style="line-height:5px;" />
            		<div class="indicates-required" style="text-align:right; font-size:12px;color:#666"><em><strong>Les champs marqu�s (*) sont obligatoires</strong></em></div>    
               	 	<br /><br style="line-height:10px;" />
                	<div style="text-align:center"><a class="button" href="#" onclick="controllaModulo();" title="Je veux 5% de r�duction">Je veux 5% de r�duction</a></div>
                <!--<input type="submit" value="S�, voglio 10 euro di sconto" name="subscribe" id="mc-embedded-subscribe" class="button">-->
                <br /><br />
                </div>
            </form>
            </div>
            <!--End mc_embed_signup-->
        </div>
    </div>
    <div id="content">
    	<div id="icons">
        	<div class="box_icona perc">
            	<h2>LES MEILLEURS PRODUITS</h2>
                <p><br />Parmigiano Reggiano, Jambon de Parme et charcuteries de qualit�. De nombreux coffrets gourmands faits � la main.</p>
            </div>
            <div class="box_icona off">
            	<h2>OFFRES LIMIT�ES</h2>
                <p>Tous les mois, vous recevrez des promotions et des r�ductions pour �conomiser sur vos d�penses. Pas de SPAM.</p>
            </div>
            <div class="box_icona sped">
            	<h2>LIVRAISONS RAPIDES</h2>
                <p>Nous livrons vos courses dans toute la France avec UPS, dans quelques jours de votre commande.<br /><br /></p>
            </div>
            <div class="box_icona ok">
            	<h2>SATISFAIT OU REMBOURS�</h2>
                <p><br />Vous avez 10 jours pour retourner votre commande.<br /><br /></p>
            </div>
        </div>
        <br />
        <div id="testimonial">
        	<h2>Lire les commentaires de nos clients qui ont d�j� achet�...</h2>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                Produits de qualit�. Envoi rapide et emballage parfait.
                <div class="sign">06/05/2016</div>
            </div>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                D�j� trois fois que je commande et toujours aussi satisfait. Ne changez rien. Merci pour ce d�licieux jambon de Parme.... Bien � vous.
                <div class="sign">01/03/2016</div>
            </div>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
				tout est parfait tres bonne qualite et tres bonne conservation
                <div class="sign">01/03/2016</div>
            </div>
        </div>
    </div>
</body>
</html>