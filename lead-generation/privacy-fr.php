<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Notice de confidentialit�</title>
<link rel="stylesheet" type="text/css" href="css/landing.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="logo"><img src="immagini/logo.gif" alt="" border="0" /></div>
<div id="form" style="max-width:600px !important"><strong>NOTICE DE CONFIDENTIALIT�</strong><br />
Nous vous informons que, aux termes de l'article 13 du d�cret-loi n. 196 de 2003, les informations fournies lors de votre enregistrement sur ce site sont recueillies pour effectuer l'exp�dition et pour d�livrer la facture relative � la commande.<br />
En cas de non transmission des informations n�cessaires la commande ne peut pas �tre finalis�e.<br /><br />

<strong>MAGNAPARMA</strong> s'engage � traiter de mani�re confidentielle ces informations et � ne pas les divulguer aux personnes non autoris�es, ni � les utiliser � des fins autres que celles indiqu�es ici, sauf apr�s �ventuelle autorisation de la part du Client. Ces informations pourront �tre communiqu�es, apr�s la signature d'un engagement de confidentialit�, aux collaborateurs et employ�s responsables de leur traitement et pour effectuer les activit�s susmentionn�es, et seront diffus�es exclusivement dans le cadre du service rendu. Ces informations seront �galement transmises sur demande aux autorit�s comp�tentes selon les termes de la loi.<br /><br />

L�organisme charg� et responsable pour le traitement des informations personnelles est:<br />
<strong>Giovanna Mari pour A & M Service snc</strong><br />
<strong>Via Ferraris, 16/A - 43036 FIDENZA - PR (Italie)</strong><br />
<strong>tel. +39 349 807782 - +39 340 0672272</strong><br />
<strong>e-mail info@magnaparma.com</strong><br /><br />

En ce qui concerne le traitement des informations � caract�re personnel, le client peut directement et � tout moment exercer ses droits conform�ment � l'art. 7 du d�cret-loi n. 196 de 2003, y compris, en particulier obtenir des renseignements sur:<br />
<ul>
 	<li>l'origine des informations personnelles;</li>
	<li>les finalit�s et les m�thodes de traitement;</li>
	<li>la m�thode appliqu�e en cas de traitement effectu� � l'aide de moyens �lectroniques;</li>
	<li>les donn�es relatives � l'identit� du titulaire, des responsables et du repr�sentant d�sign� aux termes de l'article 5, alin�a 2;</li>
	<li>les personnes ou cat�gories de personnes auxquelles les informations � caract�re personnel peuvent �tre communiqu�es ou qui peuvent en avoir connaissance en qualit� de repr�sentant d�sign� sur le territoire de l'�tat, de responsables ou charg�s.</li>
</ul><br />


<strong>Le client aura �galement le droit d�obtenir</strong>: 
<ul>
	<li>la mise � jour, la rectification ou l'int�gration des informations;</li>
    <li>l�effacement, la transformation sous forme anonyme ou le blocage des informations trait�es ill�galement, y compris pour celles qui ne sont pas n�cessaires aux fins pour lesquelles les informations ont �t� collect�es ou trait�es.</li>
</ul>
<br />

<strong>Il aura �galement le droit de s'opposer, partiellement ou globalement: </strong>: 
<ul>
	<li>pour des raisons l�gitimes au traitement des donn�es � caract�re personnel qui le concernent, bien que pertinentes par rapport au but de la collecte;</li>
    <li>au traitement des donn�es personnelles le concernant aux fins de l'envoi de mat�riel publicitaire ou de vente directe ou pour la r�alisation d'�tudes de march� ou de communications commerciales.</li>
</ul>
 </div>
</body>
</html>