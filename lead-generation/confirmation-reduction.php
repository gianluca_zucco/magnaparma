<?php /* if (!isset($_GET["k"]) || $_GET["k"] != md5(date("Ymd"))) {
    header("Location: sconto-5-euro.php");
} */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width" />
<meta name="robots" content="noindex, nofollow" />
<title>Votre coupon pour obtenir la r�duction de 5%</title>
<link rel="stylesheet" type="text/css" href="css/landing.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800' rel='stylesheet' type='text/css'>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1626694720926358');
fbq('track', 'CompleteRegistration');</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1626694720926358&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- BEGIN GOOGLE ANALYTICS CODE -->
<script type="text/javascript">
//<![CDATA[
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
    })();

    var _gaq = _gaq || [];

_gaq.push(['_setAccount', 'UA-17768269-1']);
_gaq.push(['_trackPageview']);


//]]>
</script>
<!-- END GOOGLE ANALYTICS CODE -->

	<div id="logo"><img src="immagini/logo.gif" alt="" border="0" /></div>
    <div id="wrapper">
        <br /><br /><br /><br /><br /><br />
        <div id="contenuti">
            <h1>Merci d'avoir choisi de vous inscrire � la newsletter</h1><br />
        	Voici votre code de r�duction:<br />
			<br />
			<div class="coupon">5PER-NEWS2016</div><br /><br />

Avec ce coupon vous avez droit � une <strong>r�duction imm�diate de 5%</strong> pour votre premier achat sur <strong>MagnaParma</strong>.<br /><br /><strong style="color:#bd001e">Le coupon est valable pour 10 jours � partir d�aujourd�hui.</strong><br /><br />
La r�duction n�est pas applicable aux frais d�exp�dition, elle n�est pas cumulable avec les autres promotions en cours, elle ne peut �tre convertie en argent comptant et elle ne peut pas �tre utilis�e pour des commandes effectu�es pr�c�demment.<br />
V�rifiez votre adresse email pour d�couvrir comment utiliser la r�duction.<br /><br />		
<strong>BESOIN D�AIDE?</strong><br />
Ecrivez-nous � <a href="mailto:info@magnaparma.com" style="text-decoration:none;"><strong style="color:#bd001e">info@magnaparma.com</strong></a> ou appellez-nous au <strong style="color:#bd001e">+39 349 8077821</strong>.

        </div>
    </div>
    <div id="content">
    	<div id="icons">
        	<div class="box_icona perc">
            	<h2>SEULEMENT LES MEILLEURS PRODUITS</h2>
                <p>Parmigiano Reggiano, Jambon de Parme et charcuteries de qualit�. De nombreux coffrets gourmands faits � la main.</p>
            </div>
            <div class="box_icona off">
            	<h2>OFFRES LIMIT�ES</h2>
                <p>Tous les mois, vous recevrez des promotions et des r�ductions pour �conomiser sur vos d�penses. Pas de SPAM.</p>
            </div>
            <div class="box_icona sped">
            	<h2>LIVRAISONS RAPIDES</h2>
                <p>Nous livrons vos courses dans toute la France avec UPS, dans quelques jours de votre commande.<br /><br /></p>
            </div>
            <div class="box_icona ok">
            	<h2>SATISFAIT OU REMBOURS�</h2>
                <p>Vous avez 10 jours pour retourner votre commande.<br /><br /></p>
            </div>
        </div>
        <br />
        <div id="testimonial">
        	<h2>Lire les commentaires de nos clients qui ont d�j� achet�...</h2>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                Produits de qualit�. Envoi rapide et emballage parfait.
                <div class="sign">- 06/05/2016 -</div>
            </div>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                D�j� trois fois que je commande et toujours aussi satisfait. Ne changez rien. Merci pour ce d�licieux jambon de Parme.... Bien � vous.
                <div class="sign">- 01/03/2016 -</div>
            </div>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
				tout est parfait tres bonne qualite et tres bonne conservation
                <div class="sign">- 01/03/2016 -</div>
            </div>
        </div>
    </div>
</body>
</html>