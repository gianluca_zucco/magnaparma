<?php require("varie/mail.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width" />
<meta name="robots" content="noindex, nofollow" />
<title>Iscriviti per ricevere subito 5 euro di sconto su MagnaParma</title>
<link rel="stylesheet" type="text/css" href="css/landing.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800' rel='stylesheet' type='text/css'>
<script>
	function controllaModulo() {
		var re=/ /g;
		var espressione = /^[_a-z0-9+-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$/;
		var nome = document.newsletter_magna.FNAME.value.replace(re, "");
		var email = document.newsletter_magna.EMAIL.value.replace(re, "");
				
		if (email == "") { document.getElementById("err_mail").style.display = "block"; document.newsletter_magna.EMAIL.focus(); document.getElementById("mce-EMAIL").style.borderColor = "#bd001e"; return false; }
		else { document.getElementById("err_mail").style.display = "none"; document.getElementById("mce-EMAIL").style.borderColor = "#333";}
		
		if (email != "" && !espressione.test(email)) { document.getElementById("err_val_mail").style.display = "block"; document.newsletter_magna.EMAIL.focus(); document.getElementById("mce-EMAIL").style.borderColor = "#bd001e"; return false; }
		else { document.getElementById("err_val_mail").style.display = "none"; document.getElementById("mce-EMAIL").style.borderColor = "#333"; } 
		
		if (nome == "") {document.getElementById("err_nome").style.display = "block"; document.newsletter_magna.FNAME.focus(); document.getElementById("mce-FNAME").style.borderColor = "#bd001e"; return false; }
		else {document.getElementById("err_nome").style.display = "none"; document.getElementById("mce-FNAME").style.borderColor = "#333";}
		
		if(document.newsletter_magna.privacy.checked) {  document.getElementById("err_pri").style.display = "none"; }
		else {  document.getElementById("err_pri").style.display = "block"; return false; }
		
		document.newsletter_magna.submit();
	}
</script>
</head>
<body>
<!-- BEGIN GOOGLE ANALYTICS CODE -->
<script type="text/javascript">
//<![CDATA[
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
    })();

    var _gaq = _gaq || [];

_gaq.push(['_setAccount', 'UA-17768269-1']);
_gaq.push(['_trackPageview']);


//]]>
</script>
<!-- END GOOGLE ANALYTICS CODE -->
	<div id="logo"><img src="immagini/logo.gif" alt="" border="0" /></div>
    <div id="wrapper">
        <div id="slogan"><h1>Ricevi subito 5 euro di sconto</h1></div>
        <div id="form">
            Iscriviti alla newsletter per avere subito <strong>5 euro di sconto</strong> per la tua spesa su <strong>MagnaParma</strong>.<br />
Solo <strong>i buoni di Parma</strong>, scelti per te da chi se ne intende, <strong>a casa tua in poche ore</strong>.
			<?php if(isset($popup)) { echo "<br /><strong style='color:#bd001e;'>".$popup."</strong>"; } ?>
			<br /><br />
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
            <form action="sconto-5-euro.php" method="post" id="newsletter_magna" name="newsletter_magna" class="validate" novalidate>
                <div id="mc_embed_signup_scroll">
                    <div class="mc-field-group">
                		<label for="mce-EMAIL" style="font-size:16px;"><strong>Il tuo indirizzo Email</strong> <span class="asterisk">*</span></label><br />
                		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" style="width:100%; height:25px; border:1px solid #333; margin:0px; padding:0px;">
                		<div id="err_mail">&uArr; Compilare il campo Email</div><div id="err_val_mail">&uArr; Inserire un indirizzo Email valido</div>
            		</div><br style="line-height:10px;" />
            		<div class="mc-field-group">
                		<label for="mce-FNAME" style="font-size:16px;"><strong>Il tuo Nome</strong> <span class="asterisk">*</span></label><br />
               			<input type="text" value="" name="FNAME" class="required" id="mce-FNAME" style="width:100%; height:25px; border:1px solid #333; margin:0px; padding:0px;">
               			<div id="err_nome">&uArr; Compilare il campo Nome</div>
            		</div>
                    <div class="mc-field-group" align="right" style="margin-top:5px; font-size:11px">
                    	Autorizzo il trattamento dei dati personali<br />(art.13 D.Lgs. 196/2003) - <u><strong><a href="privacy.php" style="color:inherit; text-decoration:none;" target="_blank" title="Termini e Condizioni">Termini e Condizioni</a></strong></u>*&nbsp;<input type="checkbox" name="privacy" id="privacy" />
                        <div id="err_pri" align="right">Devi accettare la normativa sulla privacy &uArr;</div>
                    </div>
            	    <div id="mce-responses" class="clear">
                	    <div class="response" id="mce-error-response" style="display:none"></div>
                    	<div class="response" id="mce-success-response" style="display:none"></div>
                	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                	<div style="position: absolute; left: -5000px;"><input type="hidden" name="azione" value="1"></div><br style="line-height:5px;" />
            		<div class="indicates-required" style="text-align:right; font-size:12px;color:#666"><em><strong>I campi contrassegnati da (*) sono obbligatori</strong></em></div>    
               	 	<br /><br style="line-height:10px;" />
                	<div style="text-align:center"><a class="button" href="#" onclick="controllaModulo();" title="Voglio 5 euro di sconto">Voglio 5 euro di sconto</a></div>
                <!--<input type="submit" value="S�, voglio 10 euro di sconto" name="subscribe" id="mc-embedded-subscribe" class="button">-->
                <br /><br />
                </div>
            </form>
            </div>
            <!--End mc_embed_signup-->
        </div>
    </div>
    <div id="content">
    	<div id="icons">
        	<div class="box_icona perc">
            	<h2>SOLO COSE BUONE</h2>
                <p>Parmigiano Reggiano, Prosciutto di Parma, vini e salumi di qualit�. Tante confezioni regalo realizzate a mano.</p>
            </div>
            <div class="box_icona off">
            	<h2>OFFERTE LIMITATE</h2>
                <p>Ogni mese nella tua mail promozioni e sconti per risparmiare sulla tua spesa. Niente SPAM.</p>
            </div>
            <div class="box_icona sped">
            	<h2>SPEDIZIONE GRATIS</h2>
                <p>Ti regaliamo le spedizioni in tutta Italia se spendi pi� di 150 euro.<br /><br /></p>
            </div>
            <div class="box_icona ok">
            	<h2>A CASA TUA IN POCHE ORE</h2>
                <p>Ricevi la tua spesa entro poche ore dall�ordine con i nostri corrieri espressi.<br /><br /></p>
            </div>
        </div>
        <br />
        <div id="testimonial">
        	<h2>Leggi i commenti di chi ha gi� acquistato...</h2>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                I prodotti sono arrivati in un imballaggio da urlo con ghiaccio sintetico che ne garantisce la qualit�. Un plauso alla vostra seriet� e competenza. Grazie
                <div class="sign">- Rosa Maria -</div>
            </div>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                Indipendentemente dal fatto che abito a Londra da 50 anni quando assaggio I vostri prodotti mi sembra di essere in Italia. Odori e sapori della mia terra. La qualita' dei vostri prodotti, l'imballaggio,la spedizione sono second to none. E la vostra cortesia e' incredibile; vi ho chiesto se potevate esaudire la mia richiesta di ricevere un prodotto special e questo e' stato fatto con la massima gentilezza.
                <div class="sign">- Lauro -</div>
            </div>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                E' la prima volta che acquistiamo in questo negozio. La consegna � stata veloce e l'ordine preciso. Non abbiamo ancora assaggiato il culatello, ma l'aspetto � ottimo..... Sicuramente faremo altri acquisti! Consigliatissimo!
                <div class="sign">- Roberta -</div>
            </div>
        </div>
    </div>
</body>
</html>