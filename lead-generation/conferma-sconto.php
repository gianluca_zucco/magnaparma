<?php /* if (!isset($_GET["k"]) || $_GET["k"] != md5(date("Ymd"))) {
    header("Location: sconto-5-euro.php");
} */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width" />
<meta name="robots" content="noindex, nofollow" />
<title>Il tuo coupon per avere subito il 5% di sconto</title>
<link rel="stylesheet" type="text/css" href="css/landing.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800' rel='stylesheet' type='text/css'>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1626694720926358');
fbq('track', 'CompleteRegistration');</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1626694720926358&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- BEGIN GOOGLE ANALYTICS CODE -->
<script type="text/javascript">
//<![CDATA[
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
    })();

    var _gaq = _gaq || [];

_gaq.push(['_setAccount', 'UA-17768269-1']);
_gaq.push(['_trackPageview']);


//]]>
</script>
<!-- END GOOGLE ANALYTICS CODE -->

<!-- INIZIO Google Code for Lead Generation IT Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1007645329;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "69pOCMLrkmAQkeW94AM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1007645329/?label=69pOCMLrkmAQkeW94AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- FINE Google Code for Lead Generation IT Conversion Page -->

	<div id="logo"><img src="immagini/logo.gif" alt="" border="0" /></div>
    <div id="wrapper">
        <br /><br /><br /><br /><br /><br />
        <div id="contenuti">
            <h1>Grazie per aver scelto di iscriverti alla newsletter</h1><br />
        	Questo � il tuo codice sconto:<br />
			<br />
			<div class="coupon">5PER-NEWS2016</div><br /><br />

Con questo coupon hai diritto a uno <strong>sconto immediato del 5%</strong> per il tuo primo ordine concluso su <strong>MagnaParma</strong>.<br /><br /><strong style="color:#bd001e">Il coupon � valido per 10 giorni a partire da oggi.</strong><br /><br />
Lo sconto non si applica alle spese di spedizione, non � cumulabile con altre promozioni in corso, non � rimborsabile, non pu� essere convertito in denaro contante e non pu� essere usato per ordini conclusi in precedenza.<br />
Controlla subito la tua mail per scoprire come usare lo sconto.<br /><br />

<strong>HAI BISOGNO DI AIUTO?</strong><br />
Scrivici a <a href="mailto:info@magnaparma.com" style="text-decoration:none;"><strong style="color:#bd001e">info@magnaparma.com</strong></a> o chiamaci al <strong style="color:#bd001e">349 8077821</strong>.

        </div>
    </div>
    <div id="content">
    	<div id="icons">
        	<div class="box_icona perc">
            	<h2>SOLO COSE BUONE</h2>
                <p>Parmigiano Reggiano, Prosciutto di Parma, vini e salumi di qualit�. Tante confezioni regalo realizzate a mano.</p>
            </div>
            <div class="box_icona off">
            	<h2>OFFERTE LIMITATE</h2>
                <p>Ogni mese nella tua mail promozioni e sconti per risparmiare sulla tua spesa. Niente SPAM.</p>
            </div>
            <div class="box_icona sped">
            	<h2>SPEDIZIONE GRATIS</h2>
                <p>Ti regaliamo le spedizioni in tutta Italia se spendi pi� di 150 euro.<br /><br /></p>
            </div>
            <div class="box_icona ok">
            	<h2>A CASA TUA IN POCHE ORE</h2>
                <p>Ricevi la tua spesa entro poche ore dall�ordine con i nostri corrieri espressi.<br /><br /></p>
            </div>
        </div>
        <br />
        <div id="testimonial">
        	<h2>Leggi i commenti di chi ha gi� acquistato</h2>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                I prodotti sono arrivati in un imballaggio da urlo con ghiaccio sintetico che ne garantisce la qualit�. Un plauso alla vostra seriet� e competenza. Grazie
                <div class="sign">- Rosa Maria -</div>
            </div>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                Indipendentemente dal fatto che abito a Londra da 50 anni quando assaggio I vostri prodotti mi sembra di essere in Italia. Odori e sapori della mia terra. La qualita' dei vostri prodotti, l'imballaggio,la spedizione sono second to none. E la vostra cortesia e' incredibile; vi ho chiesto se potevate esaudire la mia richiesta di ricevere un prodotto special e questo e' stato fatto con la massima gentilezza.
                <div class="sign">- Lauro -</div>
            </div>
            <div class="test">
                <img src="immagini/img_quote.png" alt="" border="0" style="float:left; margin-right:5px;" />
                E' la prima volta che acquistiamo in questo negozio. La consegna � stata veloce e l'ordine preciso. Non abbiamo ancora assaggiato il culatello, ma l'aspetto � ottimo..... Sicuramente faremo altri acquisti! Consigliatissimo!
                <div class="sign">- Roberta -</div>
            </div>
        </div>
    </div>
</body>
</html>