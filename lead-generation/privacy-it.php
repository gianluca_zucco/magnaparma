<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Informativa sulla Privacy</title>
<link rel="stylesheet" type="text/css" href="css/landing.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="logo"><img src="immagini/logo.gif" alt="" border="0" /></div>
<div id="form" style="max-width:600px !important"><strong>NORMATIVA SULLA PRIVACY</strong><br />
Si informa che,  ai sensi dell�art. 13 del Decreto Legislativo n. 196 del 2003 , i dati  forniti in fase di registrazione al presente sito sono raccolti per la spedizione e l�eventuale fatturazione dell�ordine. 
In caso di mancato conferimento dei dati l�ordine non andr� a buon fine.<br /><br />

<strong>MAGNAPARMA</strong> si obbliga a trattare con riservatezza tali dati e a non rivelarli a persone non autorizzate, ne� ad usarli per scopi diversi da quelli qui indicati, salvo diversa esplicita autorizzazione in tal senso da parte del Cliente. Tali dati potranno essere comunicati, previa sottoscrizione di un impegno di riservatezza, a collaboratori e dipendenti delegati alla loro conservazione ed allo svolgimento delle attivit� di cui sopra, e saranno diffusi esclusivamente nell�ambito delle finalit� del servizio reso. Tali dati saranno inoltre esibiti su richiesta delle autorit� per legge autorizzate.<br /><br />

Titolare e responsabile del trattamento dei dati personali �:<br />
<strong>Giovanna Mari per conto di A & M Service snc</strong><br />
<strong>con sede in Via Ferraris 16/A - 43036 FIDENZA - PR</strong><br />
<strong>tel. 349 807782 - 340 0672272</strong><br />
<strong>e-mail info@magnaparma.com</strong><br /><br />

In relazione al trattamento dei dati personali, <strong>il cliente potr� direttamente ed in ogni momento, esercitare i diritti di cui all�art. 7 del Decreto Legislativo n. 196 del 2003</strong>, tra cui, in particolare ottenere l�indicazione:<br />
<ul>
 	<li>dell�origine dei dati personali;</li>
	<li>delle finalit� e modalit� del trattamento; </li>
	<li>della logica applicata in caso di trattamento effettuato con l�ausilio di strumenti elettronici;</li>
	<li>degli estremi identificativi del titolare, dei responsabili e del rappresentante designato ai sensi dell�articolo 5, comma 2; </li>
	<li>dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualit� di rappresentante designato nel territorio dello Stato, di responsabili o incaricati.</li>
</ul><br />


<strong>Il cliente avr� inoltre diritto di ottenere</strong>: 
<ul>
	<li>l�aggiornamento, la rettifica oppure l�integrazione dei dati; </li>
    <li>la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non � necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati.</li>
</ul>
<br />

<strong>Avr� inoltre il diritto di opporsi, in tutto o in parte</strong>: 
<ul>
	<li>per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorch� pertinenti allo scopo della raccolta;</li>
    <li>al trattamento di dati personali che lo riguardano a fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale.</li>
</ul>
 </div>
</body>
</html>